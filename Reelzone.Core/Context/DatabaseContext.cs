﻿using Microsoft.EntityFrameworkCore;
using Reelzone.Core.Entities;
using Reelzone.Core.Entities.Users;
using System.Configuration;

namespace Reelzone.Core.Contexts
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Coin> Coins { get; set; }
        public DbSet<UserExperience> UserExperiences { get; set; }
        public DbSet<UserOauthProvider> UserOauthProviders { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<UserAchievement> UserAchievements { get; set; }
        public DbSet<UserVerify> UserVerifications { get; set; }
        public DbSet<UserWheelSpin> UserWheelSpins { get; set; }
        public DbSet<WalletToken> WalletTokens { get; set; }

        public DbSet<Activity> Activities { get; set; }
        public DbSet<Achievement> Achievements { get; set; }

        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<TournamentAttende> TournamentAttendes { get; set; }
        public DbSet<TournamentPrizepool> TournamentPrizepools { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<GameProvider> GameProviders { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameTransaction> GameTransactions { get; set; }

        public DbSet<Refferal> Refferals { get; set; }
        public DbSet<UserRefferal> UserRefferals { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(b => b.UserCreated)
                .HasDefaultValueSql("getdate()");
            modelBuilder.Entity<GameTransaction>()
                .Property(b => b.Created)
                .HasDefaultValueSql("getdate()");
            modelBuilder.Entity<UserNotification>()
                .Property(b => b.CreatedDate)
                .HasDefaultValueSql("getdate()");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Server=tcp:reelzone-dev-db.database.windows.net,1433;Initial Catalog=reelzone-dev-db;Persist Security Info=False;User ID=dev;Password=He34buhe34bu;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            //var connectionString = @"Server=localhost\SQLEXPRESS;Database=reelzone-dev;Trusted_Connection=True;";
            //var connectionString = ConfigurationManager.AppSettings.Get("Microsoft.Sql.ConnectionString");
            optionsBuilder.UseSqlServer(connectionString);
            optionsBuilder.EnableSensitiveDataLogging(true);
        }

        ~DatabaseContext()
        {
            Dispose();
        }
    }
}