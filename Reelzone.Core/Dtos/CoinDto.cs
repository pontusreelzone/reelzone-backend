﻿using System;
using System.Collections.Generic;
using System.Text;
using Reelzone.Core.Dtos;

namespace Reelzone.Core.Dtos
{
    public class CoinDto
    {
        public Guid ID { get; set; }
        public UserDto User { get; set; }
        public long Amount { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}