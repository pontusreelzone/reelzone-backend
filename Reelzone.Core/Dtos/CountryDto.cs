﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class CountryDto
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string CountryIso { get; set; }
        public string CountryFlag { get; set; }
    }
}
