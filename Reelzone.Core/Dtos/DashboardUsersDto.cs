﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class DashboardDto
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public int? Day { get; set; }

        public int Total { get; set; }
    }
}
