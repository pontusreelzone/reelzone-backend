﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    
    public class FilterUserDto
    {
        public List<FilterUsersDto> Filters { get; set; }
        
    }
    public class FilterUsersDto
    {
        public string Name { get; set; }
        public string Filter { get; set; }
       
    }
   
}
