﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class GameDto
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HeaderImage { get; set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }
        public string CardBackgroundImage { get; set; }
        public string URL { get; set; }
        public int ExternalGameId { get; set; }
        public GameProviderDto GameProvider { get; set; }
        public bool Visible { get; set; }
    }
}
