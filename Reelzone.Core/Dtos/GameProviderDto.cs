﻿using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class GameProviderDto
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public string Description { get; set; }
        public List<GameDto> Games { get; set; }
        public string IntegrationUrl { get; set; }
        public string IntegrationConfig { get; set; }
        public string IntegrationInitScript { get; set; }
        public IntegrationType IntegrationType { get; set; }
    }
}
