﻿using System;
using Reelzone.Core.Dtos;

namespace Reelzone.Core.Entities
{
    public class GameTranscationDto
    {
        public Guid ID { get; set; }
        public Int64 Amount { get; set; }
        public string Description { get; set; }
        public Int64 ExternalTransactionId { get; set; }
        public UserDto User { get; set; }
        public TournamentDto Tournament { get; set; }
    }
}