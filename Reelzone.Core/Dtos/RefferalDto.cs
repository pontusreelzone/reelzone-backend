﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class RefferalDto
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long Amount { get; set; }
        public List<UserRefferalDto>UsersRefferals { get; set; }
    }
}
