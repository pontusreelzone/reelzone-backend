﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Dtos
{
    public class TournamentAttendeDto
    {
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public TournamentDto Tournament { get; set; }
        public Guid TournamentID { get; set; }
        public UserDto User { get; set; }
        public Int64 Score { get; set; }
        public int Placement { get; set; }
        public string UserName { get; set; }
        public int CurrentPrize { get; set; }
        public bool CoinPrize { get; set; }
        public Int64 Spins { get; set; }
        public Int64 Balance { get; set; }
        public Guid AttendeID { get; set; }
        public Game Game { get; set; }
    }
}