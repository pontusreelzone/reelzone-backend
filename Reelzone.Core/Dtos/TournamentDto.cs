﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Dtos
{
    public class TournamentDto
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HeaderImage { get; set; }
        public TournamentFormat Format { get; set; }
        public int StartSpins { get; set; }
        public DateTime StartDOT { get; set; }
        public DateTime EndDOT { get; set; }
        public List<TournamentPrizepoolDto> Prizepool { get; set; }
        public bool Retryable { get; set; }
        public Game Game { get; set; }
        public int TotalSpins { get; set; }
        public int TotalAttendes { get; set; }
        public bool Exclusive { get; set; }
        public List<TournamentAttendeDto> Attendes { get; set; }
        public TournamentAttendeDto UserAttended { get; set; }
        public bool Running { get; set; }
    }
}