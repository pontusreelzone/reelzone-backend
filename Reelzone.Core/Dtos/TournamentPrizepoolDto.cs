﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class TournamentPrizepoolDto
    {
        public Guid ID { get; set; }
        public int Prize { get; set; }
        public bool CoinPrize { get; set; }
        public string Position { get; set; }
    }
}