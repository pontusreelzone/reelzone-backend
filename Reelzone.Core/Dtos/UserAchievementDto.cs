﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Dtos
{
    public class UserAchievementDto
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public bool Completed { get; set; }
        public AchievementDto Achievement { get; set; }
        public DateTime CompletedTimeStamp { get; set; }
        public DateTime AwardedTimeStamp { get; set; }
        public DateTime EndingTimestamp { get; set; }
    }
}