﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Dtos
{
    public class UserActivityDto
    {
        public Guid ID { get; set; }
        public Activity Activity { get; set; }
        public User User { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}