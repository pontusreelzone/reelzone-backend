﻿using Reelzone.Core.Entities;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Dtos
{
    public class UserDto
    {
        public Guid ID { get; set; }
        public GameProvider GameProvider { get; set; }
        public string Email { get; set; }
        public string Identifier { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AdminNote { get; set; }
        public Role Role { get; set; }
        public Status Status { get; set; }
        public PayOut PayOutOption { get; set; }
        public string Username { get; set; }
        public string AvatarImage { get; set; }
        public string HeaderImage { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public CountryDto Country { get; set; }
        public bool KycVerification { get; set; }
        public bool PerformKYCProcess { get; set; }

        public ProviderType Provider { get; set; }
        public List<UserActivityDto> Activities { get; set; }
        public bool Verified { get; set; }
        public bool Banned { get; set; }
        public bool Newsletter { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExperienceLeft { get; set; }
        public int TotalExpThisLevel { get; set; }
        public long Coins { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastLoggedIn { get; set; }

        public Guid? Referred { get; set; }
    }
}