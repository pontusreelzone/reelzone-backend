﻿using System;
using System.Collections.Generic;
using Reelzone.Core.Entities;

using System.Text;

namespace Reelzone.Core.Dtos
{
    public class UserNotificationDto
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public bool Viewed { get; set; }
        public Guid User { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
