﻿using Reelzone.Core.Dtos;

namespace Reelzone.Core.Entities
{
    public class UserOauthProviderDto
    {
        public int ID { get; set; }
        public UserDto User { get; set; }
        public ProviderType Type { get; set; }
        public string Identifier { get; set; }
    }
}