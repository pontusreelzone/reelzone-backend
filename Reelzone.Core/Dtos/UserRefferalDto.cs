﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Dtos
{
    public class UserRefferalDto
    {
        public Guid ID { get; set; }
        public string User { get; set; }
        public DateTime UsedTimestamp { get; set; }
    }
}
