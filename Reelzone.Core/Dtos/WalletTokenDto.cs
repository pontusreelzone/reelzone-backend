﻿using System;
using System.Collections.Generic;
using System.Text;
using Reelzone.Core.Dtos;

namespace Reelzone.Core.Dtos
{
    public class WalletTokenDto
    {
        public Guid ID { get; set; }
        public string Token { get; set; }
        public TournamentDto Tournament { get; set; }
        public UserDto User { get; set; }
        public DateTime Created { get; set; }
    }
}