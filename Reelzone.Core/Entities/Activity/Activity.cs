﻿using System;

namespace Reelzone.Core.Entities
{
    public class Activity
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int LevelThreshold { get; set; }
        public TimeSpan TimeThreshold { get; set; }
        public string Handle { get; set; }
    }
}