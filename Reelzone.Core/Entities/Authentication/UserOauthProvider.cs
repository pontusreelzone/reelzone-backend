﻿namespace Reelzone.Core.Entities
{
    public enum ProviderType
    {
        Facebook,
        Twitch,
        Google,
        Email
    }

    public class UserOauthProvider
    {
        public int ID { get; set; }
        public User User { get; set; }
        public ProviderType Type { get; set; }
        public string Identifier { get; set; }
    }
}