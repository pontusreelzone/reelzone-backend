﻿using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Entities
{
    public class GameProvider
    {
        public Guid ID { get; set; }
        public string Image { get; set; }
        public List<User> Users { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Game> Games { get; set; }
        public string IntegrationUrl { get; set; }
        public string IntegrationConfig { get; set; }
        public string IntegrationInitScript { get; set; }
        public IntegrationType IntegrationType { get; set; }

    }
}