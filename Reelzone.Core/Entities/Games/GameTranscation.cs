﻿using System;

namespace Reelzone.Core.Entities
{
    public class GameTransaction
    {
        public Guid ID { get; set; }
        public Int64 Amount { get; set; }
        public string Description { get; set; }
        public Int64 ExternalTransactionId { get; set; }
        public User User { get; set; }
        public Tournament Tournament { get; set; }
        public DateTime Created { get; set; }
    }
}