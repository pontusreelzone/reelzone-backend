﻿using System;

namespace Reelzone.Core.Entities
{
    public class Refferal
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long Amount { get; set; }
    }
}