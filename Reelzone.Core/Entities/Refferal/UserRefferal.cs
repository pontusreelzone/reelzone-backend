﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserRefferal
    {
        public Guid ID { get; set; }
        public Refferal Refferal { get; set; }
        public User User { get; set; }
        public DateTime UsedTimestamp { get; set; }
    }
}