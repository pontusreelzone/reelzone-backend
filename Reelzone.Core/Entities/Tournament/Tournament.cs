﻿using System;
using System.Collections.Generic;

namespace Reelzone.Core.Entities
{
    public enum TournamentFormat
    {
        Bonus,
        Score,
        Multiplier
    }

    public class Tournament
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HeaderImage { get; set; }
        public TournamentFormat Format { get; set; }
        public int StartSpins { get; set; }
        public DateTime StartDOT { get; set; }
        public DateTime EndDOT { get; set; }
        public Boolean Retryable { get; set; }
        public List<TournamentPrizepool> Prizepool { get; set; }
        public Game Game { get; set; }
        public List<TournamentAttende> Attendes { get; set; }
        public bool Exclusive { get; set; }
        public bool PaidOut { get; set; }
    }
}