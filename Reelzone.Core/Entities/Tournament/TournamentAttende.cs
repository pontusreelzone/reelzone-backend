﻿using System;

namespace Reelzone.Core.Entities
{
    public class TournamentAttende
    {
        public Guid ID { get; set; }
        public Tournament Tournament { get; set; }
        public User User { get; set; }
        public Int64 Score { get; set; }
        public Int64 Balance { get; set; }
        public Int64 Spins { get; set; }
    }
}