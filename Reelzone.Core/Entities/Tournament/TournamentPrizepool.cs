﻿using System;

namespace Reelzone.Core.Entities
{
    public class TournamentPrizepool
    {
        public Guid ID { get; set; }
        public Tournament Tournament { get; set; }
        public int Prize { get; set; }
        public bool CoinPrize { get; set; }

        //TODO: Split to two Start / End
        public string Position { get; set; }
    }
}