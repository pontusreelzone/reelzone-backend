﻿using System;

namespace Reelzone.Core.Entities
{
    public enum AchievementPeriod
    {
        Day,
        Week,
        Month,
        Lifetime
    }
    public enum RewardType
    {
        Expirience
    }

    public class Achievement
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BadgeImage { get; set; }
        public bool Enabled { get; set; }
        public int Amount { get; set; }
        public AchievementPeriod Period { get; set; }
        public int LevelThreshold { get; set; }
        public TimeSpan TimeThreshold { get; set; }
        public RewardType RewardType { get; set; }
        public int RewardAmount { get; set; }
        public bool Visible { get; set; }
        public Activity Activity { get; set; }
    }
}