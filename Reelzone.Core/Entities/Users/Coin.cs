﻿using System;

namespace Reelzone.Core.Entities
{
    public class Coin
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public long Amount { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}