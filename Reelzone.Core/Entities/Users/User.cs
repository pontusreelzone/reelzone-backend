﻿using Reelzone.Core.Entities.Users;
using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;

namespace Reelzone.Core.Entities
{
    public enum Role
    {
        User,
        Provider,
        Admin
    }

    public class User
    {
        public Guid ID { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public GameProvider GameProvider { get; set; }
        public DateTime CreatedDate { get; set; }
        public Role Role { get; set; }
        public Status Status { get; set; }
        public DateTime BirthDate { get; set; }
        public string AvatarImage { get; set; }
        public PayOut PayOutOption { get; set; }
        public bool KycVerification { get; set; }
        public bool PerformKYCProcess { get; set; }
        public string HeaderImage { get; set; }
        public Country Country { get; set; }
        public string Password { get; set; }
        public string AdminNote { get; set; }
        public string Salt { get; set; }
        public List<UserActivity> Activities { get; set; }
        public int Level { get; set; }
        public List<UserAchievement> Achievements { get; set; }
        public bool Verified { get; set; }
        public bool Banned { get; set; }
        public bool Newsletter { get; set; }
        public List<Coin> Coins { get; set; }
        public DateTime UserCreated { get; set; }
        public DateTime LastLoggedIn { get; set; }

        public static implicit operator User(Guid v)
        {
            throw new NotImplementedException();
        }
    }
}