﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserAchievement
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public bool Completed { get; set; }
        public Achievement Achievement { get; set; }
        public DateTime CompletedTimeStamp { get; set; }
        public DateTime AwardedTimeStamp { get; set; }
        public DateTime EndingTimestamp { get; set; }
    }
}