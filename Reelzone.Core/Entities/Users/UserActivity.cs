﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserActivity
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public Activity Activity { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool Credited { get; set; }
    }
}