﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserExperience
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public int Value { get; set; }
        public int Description { get; set; }
    }
}