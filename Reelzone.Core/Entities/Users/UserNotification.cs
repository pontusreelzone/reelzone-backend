﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Entities.Users
{
    public class UserNotification
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public bool Viewed { get; set; }
        public User User { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
