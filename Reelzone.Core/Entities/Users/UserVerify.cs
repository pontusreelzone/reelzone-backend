﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserVerify
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public string TokenType { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}