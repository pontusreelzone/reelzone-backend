﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Entities
{
    public enum TokenType
    {
        Play,
        Tournament
    }

    public class WalletToken
    {
        public Guid ID { get; set; }
        public string Token { get; set; }
        public Tournament Tournament { get; set; }
        public User User { get; set; }
        public TokenType Type { get; set; }
        public DateTime Created { get; set; }
    }
}