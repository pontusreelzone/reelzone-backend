﻿using System;

namespace Reelzone.Core.Entities
{
    public class UserWheelSpin
    {
        public Guid ID { get; set; }
        public User User { get; set; }
        public int Amount { get; set; }
        public DateTime Timestamp { get; set; }
    }
}