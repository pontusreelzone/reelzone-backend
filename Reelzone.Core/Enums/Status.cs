﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Enums
{
    public enum Status
    {
        Active,
        Banned,
        Deleted
    }
}
