﻿using Microsoft.AspNetCore.Authorization;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reelzone.Core.Helpers.Authorization
{
    public class RoleHandler : AuthorizationHandler<RoleRequirement>
    {
        public IUserService UserService;
        public RoleHandler(IUserService userService)
        {
            UserService = userService;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {

            if (context.User.Identity.IsAuthenticated)
            {
                var admin = UserService.GetUser(
                context.User.Claims.FirstOrDefault(
                    c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value);
                //Admin
                if (requirement.Role == 2)
                {
                    if (admin.Role == (Role)requirement.Role)
                    {
                        context.Succeed(requirement);
                        return Task.CompletedTask;
                    }
                    else
                    {
                        context.Fail();
                        return Task.CompletedTask;
                    }
                }
                //Provider
                if (requirement.Role == 1)
                {
                    if (admin.Role == (Role)requirement.Role)
                    {
                        context.Succeed(requirement);
                        return Task.CompletedTask;
                    }
                    else
                    {
                        context.Fail();
                        return Task.CompletedTask;
                    }
                }
                else
                {
                    context.Fail();
                    return Task.CompletedTask;
                }



            }
            else
            {
                context.Fail();
                return Task.CompletedTask;
            }

        }
    }
}
