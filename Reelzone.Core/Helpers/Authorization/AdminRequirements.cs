﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Helpers.Authorization
{
    public class RoleRequirement : IAuthorizationRequirement
    {

        public RoleRequirement(int role)
        {
            Role = role;
        }
        public int Role { get; }

    }
}
