﻿using AutoMapper;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Interfaces;
using System;

namespace Reelzone.Core.Helpers
{
    public class Mapper : IAutoMapper
    {
        public IMapper MapperService = null;
        public MapperConfiguration Configuration = null;

        public Mapper()
        {
            Configure();
            MapperService = Configuration.CreateMapper();
        }

        private void Configure()
        {
            Configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tournament, TournamentDto>().ReverseMap();
                cfg.CreateMap<TournamentPrizepool, TournamentPrizepoolDto>().ReverseMap();
                cfg.CreateMap<TournamentAttende, TournamentAttendeDto>().ReverseMap();
                cfg.CreateMap<User, UserDto>().ReverseMap();
                cfg.CreateMap<UserAchievement, UserAchievementDto>().ReverseMap();
                cfg.CreateMap<Achievement, AchievementDto>().ReverseMap();
                cfg.CreateMap<Country, CountryDto>().ReverseMap();
                cfg.CreateMap<Game, GameDto>().ForMember(c => c.GameProvider, option => option.Ignore()).ReverseMap();
                cfg.CreateMap<GameProvider, GameProviderDto>().ReverseMap();
                cfg.CreateMap<UserNotification, UserNotificationDto>().ForMember(c => c.User, option =>option.MapFrom(X=>X.User.ID))
                                                                      .ReverseMap()
                                                                      .ForMember(c=>c.CreatedDate,option =>option.Ignore())
                                                                      .ForMember(c => c.User, option => option.Ignore());

            });
        }

        public IMapper Get()
        {
            return MapperService;
        }

        void IAutoMapper.Configure()
        {
            throw new NotImplementedException();
        }
    }
}