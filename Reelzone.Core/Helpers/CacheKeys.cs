﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Helpers
{
    public static class CacheKeys
    {
        public static string Winners { get { return "_Winners"; } }
        public static string Games { get { return "_Games"; } }
        public static string GameLeaderboard { get { return "_GameLeaderboard"; } }
        public static string Game { get { return "_Game"; } }
        public static string Providers { get { return "_Providers"; } }
        public static string Profile { get { return "_Profile"; } }
        public static string UserActivities { get { return "_UserActivities"; } }
    }
}