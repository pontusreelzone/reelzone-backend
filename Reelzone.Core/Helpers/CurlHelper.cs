﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reelzone.Core.Helpers
{
    public class CurlHelper
    {
        public async Task<string> Curl(string url, string payload)
        {
            string username = "reelzone_prod";
            string password = "PhMhYP4QdaRSeoYXZNrmS5Fm92wfXSAk";
            var authToken = Encoding.ASCII.GetBytes($"{username}:{password}");

            var content = new StringContent(payload, Encoding.UTF8, "application/json");

            var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                  Convert.ToBase64String(authToken));

            // Get the response.
            HttpResponseMessage response = await client.PostAsync(
                url,
                content);

            // Get the response content.
            HttpContent responseContent = response.Content;

            // Get the stream of the content.
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                // Write the output.
                var result = await reader.ReadToEndAsync();
                return result;
            }
        }
    }
}