﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Helpers
{
    public class DateHelper
    {
        private string TimeZone = "W. Europe Standard Time";

        public DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        public DateTime ParseTimeZone(DateTime date)
        {
            date.ToLocalTime();
            TimeZoneInfo myZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            DateTime custDateTime = TimeZoneInfo.ConvertTimeFromUtc(date, myZone);
            return custDateTime;
        }
    }
}