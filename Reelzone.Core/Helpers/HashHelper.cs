﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Reelzone.Core.Helpers
{
    public class HashHelper
    {
        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        public static string PublicKey;

        public static string GenerateSignature(string data, string publicKey)
        {
            byte[] dataBytes = Encoding.Unicode.GetBytes(data);
            var sha = SHA256.Create();
            var hashedData = sha.ComputeHash(dataBytes);

            var rsa = RSA.Create();
            rsa.FromXmlString("<RSAKeyValue>" +
                           "<Modulus>" + publicKey + "</Modulus>" +
                           "<Exponent>AQAB</Exponent>" +
                       "</RSAKeyValue>");

            var signer = new RSAPKCS1SignatureFormatter(rsa);
            signer.SetHashAlgorithm("SHA256");

            return Convert.ToBase64String(signer.CreateSignature(hashedData));
        }

        public static bool Decrypt(string data, string signature, string password)
        {
            X509Certificate2 privateCert = new X509Certificate2(Directory.GetCurrentDirectory() + "/Controllers/Providers/Kalamba/certificate_combined.pfx", password, X509KeyStorageFlags.Exportable);

            // This instance can not sign and verify with SHA256:
            RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)privateCert.PrivateKey;

            // This one can:
            RSACryptoServiceProvider privateKey1 = new RSACryptoServiceProvider();
            privateKey1.ImportParameters(privateKey.ExportParameters(true));
            bool isValid = privateKey1.VerifyData(_encoder.GetBytes(data), "SHA256", _encoder.GetBytes(signature));
            return isValid;
        }

        public static string Encryption(string data, string privateKey)
        {
            //Encrypting the text using the public key
            RSACryptoServiceProvider cipher = null;
            cipher = new RSACryptoServiceProvider();
            cipher.FromXmlString(privateKey);
            byte[] dataByte = Encoding.UTF8.GetBytes(data);
            byte[] cipherText = cipher.Encrypt(dataByte, false);
            return Convert.ToBase64String(cipherText);
        }

        public static string Encrypt(string data, string privateKey)
        {
            var rsa = new RSACryptoServiceProvider();

            rsa.FromXmlString(privateKey);
            var dataToEncrypt = _encoder.GetBytes(data);
            byte[] signature = rsa.SignData(dataToEncrypt, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            return Convert.ToBase64String(signature);
        }
    }
}