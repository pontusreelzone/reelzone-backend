﻿using log4net;
using log4net.Config;
using System.IO;
using System.Reflection;

namespace Reelzone.Core.Helpers
{
    public class LoggerService
    {
        private readonly ILog Logger;

        public LoggerService()
        {
            var logConfiguration = new FileInfo("log4net.config");
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, logConfiguration);

            Logger = LogManager.GetLogger(typeof(LoggerService));
        }

        public void Log(string payload)
        {
            Logger.Debug(payload);
        }
    }
}