﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IAchievementService
    {
        Achievement CreateAchievement(AchievementDto achievement);

        Achievement UpdateAchievement(AchievementDto achievement);

        bool DeleteAchievement(Guid id);

        List<Achievement> GetAllAchievements();
        Achievement GetAchievement(Guid id);
        List<Activity> GetAllActivities();


    }
}
