﻿using AutoMapper;

namespace Reelzone.Core.Interfaces
{
    public interface IAutoMapper
    {
        void Configure();

        IMapper Get();
    }
}