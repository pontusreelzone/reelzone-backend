﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Interfaces
{
    public interface IBusQueService
    {
        bool SendUserActivity(Guid userID, string ActivityHandle);

        bool SendUserLevelUp(Guid userID);

        bool NotifyChallengeCompleted(UserAchievement userAchievement, Guid userID);

        bool SendUserSpin(Guid attenderID, Guid tournamentID, int score);

        bool Send(string label, string topic);
    }
}