﻿using Microsoft.AspNetCore.Http;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface ICoinService
    {
        public long CreateCoin(CoinDto coin);

        public long GetAmount(Guid userId);
    }
}