﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface ICountryService
    {
        List<Country> AllCountries();
        Country CreateCountry(CountryDto country);
        Country UpdateCountry(CountryDto countryDto);
        Country AddCountry(CountryDto countryDto);
        bool DeleteCountry(Guid id);
        Country GetCountry(Guid id);
        Dictionary<string, int> GetDashboardCoutryData();

    }
}
