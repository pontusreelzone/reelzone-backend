﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IGameProviderService
    {
        GameProvider AddGameProvider(GameProviderDto providerDto);
        GameProvider UpdateGameProvider(GameProviderDto providerDto);
        bool DeleteProvider(Guid id);
        GameProvider GetProvider(Guid id);
        List<GameProvider> GetProviders();
    }
}
