﻿using Reelzone.API.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IGameService
    {

        List<string> GetNamesOfGames();

        List<Game> AllGames();

        List<Game> GetFeaturedGames();

        public List<GameProvider> GetGameProviders();

        List<GameLeaderBoardModel> GetLeaderboard();

        bool DeleteGame(Guid id);

        Game GetGame(Guid id);

        Game AddGame(GameDto gameDto);

        Game UpdateGame(GameDto gameDto);
    }
}