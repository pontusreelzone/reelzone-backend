﻿using Microsoft.AspNetCore.Http;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface IGameTransactionService
    {
         GameTransaction Create(GameTranscationDto gameTransaction);
        List<GameTransaction> FilteredGameTransaction(DateTime? start, DateTime? end, string UserID);

         GameTransaction Find(Int64 externalTransactionId);
    }
}