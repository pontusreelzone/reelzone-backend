﻿using Microsoft.AspNetCore.Http;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface IMailService
    {
        Task<bool> AddSubscriberToCampaignList(string emailAddress, string listID);

        Task DeleteSubscriberToCampaignList(string emailAddress, string listID);

        Task<bool> SendVerifiedEmail(string emailAddress);

        Task<bool> SendWinnerEmail(string emailAddress, UserDto user, TournamentDto tournament, TournamentAttendeDto tournamentAttendeDto);

        Task<bool> SendVerificationEmail(string emailAddress, string verificationLink);

        Task<bool> SendForgotPasswordEmail(string emailAddress, string verificationLink);
    }
}