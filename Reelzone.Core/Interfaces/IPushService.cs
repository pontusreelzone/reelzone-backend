﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Reelzone.Core.Interfaces
{
    public interface IPushService
    {
        Task<bool> SendAll(string eventType, string message, string title, string type);

        Task<bool> Send(Guid userId, string eventType, string message, string title, string type);
    }
}