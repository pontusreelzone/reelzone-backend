﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IRefferalService
    {
        List<RefferalDto> GetRefferals(int page);
        List<RefferalDto> GetFillteredRefferals(string code);

        Refferal CreateRefferal(RefferalDto refferalDto);
        bool DeleteRefferal(Guid ID);
        RefferalDto UpdateReferal(RefferalDto refferalDto);

    }
}
