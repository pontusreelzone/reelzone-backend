﻿using SocketIOClient;

namespace Reelzone.Core.Interfaces
{
    public interface ISocketService
    {
        void ConnectAsync();

        void Emit(string topic, string data);

        void Client_OnConnected();

        void Client_OnClosed(ServerCloseReason reason);
    }
}