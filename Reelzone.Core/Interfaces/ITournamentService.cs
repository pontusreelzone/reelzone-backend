﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Models;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface ITournamentService
    {
        List<TournamentDto> DashBoardTournaments();

        List<TournamentDto> DashBoardProviderTournaments(Guid id);

        List<DashboardDto> FilteredByGameCountAttendees(string gameID, DateTime? start, DateTime? end);

        List<DashboardDto> FilteredByGameCountSpins(string gameID, DateTime? start, DateTime? end);

        List<DashboardDto> CountAttendees(bool montly);

        List<DashboardDto> FilteredByProviderCountSpins(string providerID, DateTime? start, DateTime? end);

        List<DashboardDto> FilteredByProviderCountAttendees(string providerID, DateTime? start, DateTime? end);

        List<DashboardDto> FilteredCountSpins(DateTime? start, DateTime? end);

        List<DashboardDto> CountSpins(bool montly);

        List<DashboardDto> FilteredCountAttendees(DateTime? start, DateTime? end);

        TournamentDto CreateTournament(TournamentDto tournamentDto, string identifier, string game);

        List<TournamentDto> GetAllTournaments();

        bool isTournamentRunning(DateTime start, DateTime end);

        List<TournamentDto> GetTournaments(int amount = 0);

        bool DeleteTournament(Guid ID, string identifier);

        TournamentDto Update(TournamentDto tournamentDto, string identifier, string game);

        List<TournamentDto> GetLiveTournaments(string identifier);

        List<TournamentDto> GetHistoryTournaments(string identifier);

        TournamentDto GetTournament(Guid ID, string identifier);

        List<TournamentAttendeDto> GetTournamentWinners(int amount);

        TournamentDto GetTournament(Guid ID);

        TournamentDto GetExclusiveTournament(Guid ID, string identifier);

        List<TournamentDto> GetExclusiveTournaments(string identifier);

        TournamentAttende JoinTournament(Guid userID, Guid ID);

        TournamentAttendeDto UserAttending(Guid userID, Guid ID);

        bool SpinTournament(Guid tournamentID, Guid userID, int score);

        List<TournamentAttendeDto> GetUsersAttenders(Guid userId);

        List<TournamentAttendeDto> GetAttenders(Guid tournamentId);

        LeaderboardModel GetAttendersModel(Guid tournamentId);

        int GetAttenderPlacement(TournamentAttende attende, Guid tournamentId);

        int GetAttenderPrize(int placement, Tournament tournament);

        bool GetAttenderCoinPrize(int placement, Tournament tournament);

        TournamentAttendeDto GetAttender(Guid attende);

        TournamentAttendeDto DeductSpins(TournamentAttendeDto tournamentAttende, Int64 amount);

        TournamentAttendeDto UpdateScore(TournamentAttendeDto tournamentAttende, Int64 amount);

        TournamentAttendeDto GetAttenderByToken(string token);
    }
}