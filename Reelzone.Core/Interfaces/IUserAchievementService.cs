﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface IUserAchievmentService
    {
        void DeleteUserAchievments(Achievement achievement);

        void GenerateUserAchievements(Guid achievementID);

        List<UserAchievementDto> GetUserAchievements(Guid userID, AchievementPeriod period, bool completed = false);

        List<UserAchievementDto> GetUserAchievements(Guid userID, AchievementPeriod period);

        List<UserAchievementDto> GetUserAchievements(Guid userID);

        void GenerateUserAnchievementsJobs(AchievementPeriod period);

        void GenerateUserAnchievementsJobs(AchievementPeriod period, Guid userId);

        void GenerateUserPeriodJobs();

        TimeSpan GetPeriodInDays(AchievementPeriod period);

        bool CheckUserAnchievements(Activity activity, Guid userID);
    }
}