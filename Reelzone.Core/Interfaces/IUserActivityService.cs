﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Interfaces
{
    public interface IUserActivityService
    {
        UserActivity CreateUserActivity(Guid userID, string activityHandle);

        bool ActivityWithinThreshold(UserActivity userActivity, Activity activity);
    }
}