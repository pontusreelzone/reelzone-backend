﻿using Reelzone.Core.Entities;
using System;

namespace Reelzone.Core.Interfaces
{
    public interface IUserExperienceService
    {
        UserExperience CreateUserExperience(UserAchievement userAchievement);

        UserExperience CreateUserExperience(int amount, Guid userId);

        bool UserLevelUp(Guid userID);

        int CalculateUserLevel(Guid userID);

        int GetUserCurrentLevel(Guid userID);

        int GetCurrentUserExperience(Guid userID);

        int CalculateUserExperienceLeft(Guid userID);

        void GiveUserExperience(Guid userID, int amount);
        int GetExperienceForLevel(int level);
    }
}