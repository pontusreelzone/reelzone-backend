﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IUserNotificationService
    {
        UserNotificationDto UpdatUserNotification(UserNotificationDto userNotificationDto);
        UserNotificationDto AddUserNotification(UserNotificationDto userNotificationDto);
        bool DeleteUserNotification(Guid id);
        List<UserNotificationDto> GetUserNotifications(Guid id, bool viewed);
        public List<UserNotificationDto> GetUserNotifications(Guid id);


    }
}
