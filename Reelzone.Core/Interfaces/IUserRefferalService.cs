﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IUserRefferalService
    {
        bool RedeemRefferal(string code, Guid userId);

        List<UserReferralStatsModel> GetReferralLeaderboard();

        List<UserRefferal> GetUserRefferalsByRefferal(Guid ID);
    }
}