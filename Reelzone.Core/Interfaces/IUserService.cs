﻿using Microsoft.AspNetCore.Http;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Reelzone.Core.Models;
using Reelzone.Core.Entities.Users;

namespace Reelzone.Core.Interfaces
{
    public interface IUserService
    {
        List<string> GetCheaters();

        List<string> GiveCoins();

        List<DashboardDto> CountUsers(bool lastMonth);

        List<DashboardDto> FillterCountUsers(DateTime? start, DateTime? end);

        UserOauthProvider CreateOauthProvider(UserDto userModel);

        UserReferralStatsModel GetReferralStats(Guid userId);

        UserDto GetUserByEmail(string email);

        UserDto UpdateNote(Guid userId, string note);

        public bool SendVerifySMS(string number);

        UserDto CreateUser(UserDto userModel);

        List<CountryDto> GetCountries();

        List<UserDto> GetLimitedUsers(int howMany, int page);

        UserDto GetUserByVerifytoken(string token);

        UserOauthProvider GetOauthProvider(string identifier);

        UserDto GetUser(string Identifier);

        UserDto GetCleanUser(Guid userId);

        UserDto GetCleanUser(string identifier);

        bool DeleteUser(Guid id);

        bool VerifyKyc(Guid userId);

        UserDto UpdateUser(UserDto userDto, Guid country);

        List<UserDto> SearchUsers(int pageSize, int currentPage, FilterUserDto filters);

        UserDto GetUser(Guid userID);

        User GetUserEntity(string Identifier);

        UserProfileModel GetUserProfile(Guid userId);

        List<UserActivity> GetUserActivities(Guid userId);

        UserDto GetUserByEmailToken(string token);

        UserDto GetUserByToken(string token);

        bool UserOauthProviderConnected(string identifier);

        Task<bool> SignInAsync(string email, string password, HttpContext httpContext);

        Task<bool> SignInEmailAsync(UserDto user, HttpContext httpContext);

        UserDto UpdatePayOut(UserDto userModel);

        UserDto UpdateProfileImage(UserDto userModel);

        UserDto UpdateHeaderImage(UserDto userDto);

        UserDto UpdateVerified(UserDto userDto);

        UserDto UpdateNewsletter(UserDto userDto);

        UserDto UpdateCountry(UserDto userDto, Guid country);

        UserDto UpdateBirthDate(UserDto userModel);

        UserDto UpdateUsername(UserDto userModel);

        UserDto UpdateEmail(UserDto userModel);

        UserDto UpdatePassword(UserDto userModel);

        byte[] GenerateSaltedHash(byte[] plainText, byte[] salt);

        string GenerateUserTokenAsync(Guid userID, string tokenType);

        string GenerateToken(Guid userID);
    }
}