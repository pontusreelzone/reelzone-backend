﻿using Microsoft.AspNetCore.Http;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Reelzone.Core.Interfaces
{
    public interface IWalletService
    {
        public WalletTokenDto CreateWalletToken(WalletTokenDto walletTokenDto);

        public WalletTokenDto CreatePlayWalletToken(WalletTokenDto walletTokenDto);

        public WalletToken GetToken(string token);
    }
}