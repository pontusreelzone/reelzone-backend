﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Interfaces
{
    public interface IWheelService
    {
        public int Roll();

        public int GetCoinWinnings(int spin);

        public int SpinWheel(Guid userId);
    }
}