﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reelzone.Core.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LevelThreshold = table.Column<int>(nullable: false),
                    TimeThreshold = table.Column<TimeSpan>(nullable: false),
                    Handle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CountryIso = table.Column<string>(nullable: true),
                    CountryFlag = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GameProviders",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IntegrationUrl = table.Column<string>(nullable: true),
                    IntegrationConfig = table.Column<string>(nullable: true),
                    IntegrationInitScript = table.Column<string>(nullable: true),
                    IntegrationType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameProviders", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Refferals",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Amount = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Refferals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Achievements",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    BadgeImage = table.Column<string>(nullable: true),
                    Enabled = table.Column<bool>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Period = table.Column<int>(nullable: false),
                    LevelThreshold = table.Column<int>(nullable: false),
                    TimeThreshold = table.Column<TimeSpan>(nullable: false),
                    RewardType = table.Column<int>(nullable: false),
                    RewardAmount = table.Column<int>(nullable: false),
                    Visible = table.Column<bool>(nullable: false),
                    ActivityID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Achievements", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Achievements_Activities_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Admin = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    AvatarImage = table.Column<string>(nullable: true),
                    PayOutOption = table.Column<int>(nullable: false),
                    KycVerification = table.Column<bool>(nullable: false),
                    PerformKYC = table.Column<bool>(nullable: false),
                    HeaderImage = table.Column<string>(nullable: true),
                    CountryID = table.Column<Guid>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastLoggedIn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    HeaderImage = table.Column<string>(nullable: true),
                    BackgroundImage = table.Column<string>(nullable: true),
                    LogoImage = table.Column<string>(nullable: true),
                    CardBackgroundImage = table.Column<string>(nullable: true),
                    URL = table.Column<string>(nullable: true),
                    ExternalGameId = table.Column<int>(nullable: false),
                    GameProviderID = table.Column<Guid>(nullable: true),
                    Visible = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Games_GameProviders_GameProviderID",
                        column: x => x.GameProviderID,
                        principalTable: "GameProviders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Coins",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    Amount = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coins", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Coins_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserAchievements",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    Completed = table.Column<bool>(nullable: false),
                    AchievementID = table.Column<Guid>(nullable: true),
                    CompletedTimeStamp = table.Column<DateTime>(nullable: false),
                    AwardedTimeStamp = table.Column<DateTime>(nullable: false),
                    EndingTimestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAchievements", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserAchievements_Achievements_AchievementID",
                        column: x => x.AchievementID,
                        principalTable: "Achievements",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAchievements_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    ActivityID = table.Column<Guid>(nullable: true),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    Credited = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserActivities_Activities_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserActivities_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserExperiences",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    Value = table.Column<int>(nullable: false),
                    Description = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserExperiences", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserExperiences_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserOauthProviders",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Identifier = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserOauthProviders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserOauthProviders_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRefferals",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    RefferalID = table.Column<Guid>(nullable: true),
                    UserID = table.Column<Guid>(nullable: true),
                    UsedTimestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRefferals", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserRefferals_Refferals_RefferalID",
                        column: x => x.RefferalID,
                        principalTable: "Refferals",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRefferals_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserVerifications",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    TokenType = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    ExpireDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserVerifications", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserVerifications_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tournaments",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    HeaderImage = table.Column<string>(nullable: true),
                    Format = table.Column<int>(nullable: false),
                    StartSpins = table.Column<int>(nullable: false),
                    StartDOT = table.Column<DateTime>(nullable: false),
                    EndDOT = table.Column<DateTime>(nullable: false),
                    Retryable = table.Column<bool>(nullable: false),
                    GameID = table.Column<Guid>(nullable: true),
                    Exclusive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tournaments", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Tournaments_Games_GameID",
                        column: x => x.GameID,
                        principalTable: "Games",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GameTransactions",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Amount = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ExternalTransactionId = table.Column<long>(nullable: false),
                    UserID = table.Column<Guid>(nullable: true),
                    TournamentID = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameTransactions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GameTransactions_Tournaments_TournamentID",
                        column: x => x.TournamentID,
                        principalTable: "Tournaments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GameTransactions_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TournamentAttendes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    TournamentID = table.Column<Guid>(nullable: true),
                    UserID = table.Column<Guid>(nullable: true),
                    Score = table.Column<long>(nullable: false),
                    Balance = table.Column<long>(nullable: false),
                    Spins = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentAttendes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TournamentAttendes_Tournaments_TournamentID",
                        column: x => x.TournamentID,
                        principalTable: "Tournaments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TournamentAttendes_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TournamentPrizepools",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    TournamentID = table.Column<Guid>(nullable: true),
                    Prize = table.Column<int>(nullable: false),
                    Position = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentPrizepools", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TournamentPrizepools_Tournaments_TournamentID",
                        column: x => x.TournamentID,
                        principalTable: "Tournaments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WalletTokens",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    TournamentID = table.Column<Guid>(nullable: true),
                    UserID = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalletTokens", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WalletTokens_Tournaments_TournamentID",
                        column: x => x.TournamentID,
                        principalTable: "Tournaments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WalletTokens_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Achievements_ActivityID",
                table: "Achievements",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_Coins_UserID",
                table: "Coins",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Games_GameProviderID",
                table: "Games",
                column: "GameProviderID");

            migrationBuilder.CreateIndex(
                name: "IX_GameTransactions_TournamentID",
                table: "GameTransactions",
                column: "TournamentID");

            migrationBuilder.CreateIndex(
                name: "IX_GameTransactions_UserID",
                table: "GameTransactions",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentAttendes_TournamentID",
                table: "TournamentAttendes",
                column: "TournamentID");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentAttendes_UserID",
                table: "TournamentAttendes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentPrizepools_TournamentID",
                table: "TournamentPrizepools",
                column: "TournamentID");

            migrationBuilder.CreateIndex(
                name: "IX_Tournaments_GameID",
                table: "Tournaments",
                column: "GameID");

            migrationBuilder.CreateIndex(
                name: "IX_UserAchievements_AchievementID",
                table: "UserAchievements",
                column: "AchievementID");

            migrationBuilder.CreateIndex(
                name: "IX_UserAchievements_UserID",
                table: "UserAchievements",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_ActivityID",
                table: "UserActivities",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_UserActivities_UserID",
                table: "UserActivities",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserExperiences_UserID",
                table: "UserExperiences",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserOauthProviders_UserID",
                table: "UserOauthProviders",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRefferals_RefferalID",
                table: "UserRefferals",
                column: "RefferalID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRefferals_UserID",
                table: "UserRefferals",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CountryID",
                table: "Users",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_UserVerifications_UserID",
                table: "UserVerifications",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_WalletTokens_TournamentID",
                table: "WalletTokens",
                column: "TournamentID");

            migrationBuilder.CreateIndex(
                name: "IX_WalletTokens_UserID",
                table: "WalletTokens",
                column: "UserID");
                */
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Coins");

            migrationBuilder.DropTable(
                name: "GameTransactions");

            migrationBuilder.DropTable(
                name: "TournamentAttendes");

            migrationBuilder.DropTable(
                name: "TournamentPrizepools");

            migrationBuilder.DropTable(
                name: "UserAchievements");

            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropTable(
                name: "UserExperiences");

            migrationBuilder.DropTable(
                name: "UserOauthProviders");

            migrationBuilder.DropTable(
                name: "UserRefferals");

            migrationBuilder.DropTable(
                name: "UserVerifications");

            migrationBuilder.DropTable(
                name: "WalletTokens");

            migrationBuilder.DropTable(
                name: "Achievements");

            migrationBuilder.DropTable(
                name: "Refferals");

            migrationBuilder.DropTable(
                name: "Tournaments");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "GameProviders");
        }
    }
}