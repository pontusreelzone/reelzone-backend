﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reelzone.Core.Migrations
{
    public partial class usercreated2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}