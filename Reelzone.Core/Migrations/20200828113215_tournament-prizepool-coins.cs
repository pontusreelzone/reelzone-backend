﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reelzone.Core.Migrations
{
    public partial class tournamentprizepoolcoins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CoinPrize",
                table: "TournamentPrizepools",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoinPrize",
                table: "TournamentPrizepools");
        }
    }
}
