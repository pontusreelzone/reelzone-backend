﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reelzone.Core.Migrations
{
    public partial class ivicachanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Admin",
                table: "Users");

            migrationBuilder.AddColumn<Guid>(
                name: "GameProviderID",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Users_GameProviderID",
                table: "Users",
                column: "GameProviderID");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_GameProviders_GameProviderID",
                table: "Users",
                column: "GameProviderID",
                principalTable: "GameProviders",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_GameProviders_GameProviderID",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_GameProviderID",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "GameProviderID",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Role",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "Admin",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
