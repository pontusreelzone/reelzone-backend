﻿using System;

namespace Reelzone.Core.Models
{
    public class AchievementCompleteMessage
    {
        public Guid UserID { get; set; }
        public string AchievementName { get; set; }
    }
}