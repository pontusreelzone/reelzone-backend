﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class GameLeaderBoardModel
    {
        public UserDto User { get; set; }
        public long Coins { get; set; }
    }
}