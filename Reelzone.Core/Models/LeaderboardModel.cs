﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.Core.Models
{
    public class LeaderboardModel
    {
        public Game Game { get; set; }
        public Tournament Tournament { get; set; }
        public List<TournamentAttendeDto> Attendes { get; set; }
        public TournamentAttendeDto Attende { get; set; }
    }
}