﻿using System;

namespace Reelzone.Core.Models
{
    public class UserActivityMessage
    {
        public Guid UserID { get; set; }
        public string ActivityHandle { get; set; }
    }
}