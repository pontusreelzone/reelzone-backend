﻿using System;

namespace Reelzone.Core.Models
{
    public class UserLevelUpMessage
    {
        public Guid UserID { get; set; }
        public int NewLevel { get; set; }
    }
}