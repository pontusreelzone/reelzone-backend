﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;

namespace Reelzone.Core.Models
{
    public class UserProfileModel
    {
        public Guid ID { get; set; }
        public Role Role { get; set; }
        public string Username { get; set; }
        public string AvatarImage { get; set; }
        public string HeaderImage { get; set; }
        public CountryDto Country { get; set; }
        public ProviderType Provider { get; set; }
        public List<UserActivity> Activities { get; set; }
        public List<UserAchievementDto> Achievements { get; set; }
        public List<TournamentAttendeDto> TournamentsHistory { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExperienceLeft { get; set; }
        public int TotalExpThisLevel { get; set; }
        public long Coins { get; set; }
        public int Tournaments { get; set; }
        public int Bets { get; set; }
    }
}