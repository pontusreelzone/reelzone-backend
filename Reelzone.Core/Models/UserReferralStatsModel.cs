﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reelzone.Core.Models
{
    public class UserReferralStatsModel
    {
        public int Signups { get; set; }
        public int Earned { get; set; }
        public string Username { get; set; }
        public Guid UserId { get; set; }
    }
}