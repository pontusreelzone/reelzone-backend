﻿using System;

namespace Reelzone.Core.Models
{
    public class UserSpinMessage
    {
        public Guid AttenderID { get; set; }
        public Guid TournamentID { get; set; }
        public int Score { get; set; }
    }
}