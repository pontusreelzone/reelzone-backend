﻿using AutoMapper;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Reelzone.Core.Services
{
    public class AchievementService : IAchievementService
    {

        public DatabaseContext Context;
        public IMapper AutoMapper;

        public AchievementService(DatabaseContext dbContext, IAutoMapper autoMapper)
        {

            Context = dbContext;
            AutoMapper = autoMapper.Get();
        }

        public Achievement CreateAchievement(AchievementDto achievementDto)
        {
            using (var db = new DatabaseContext())
            {
                var activity = db.Activities.FirstOrDefault(x => x.ID == achievementDto.Activity);
                if (activity == null)
                {
                    throw new Exception("Activity does not exist");
                }
                var achievement = new Achievement()
                {
                    Name = achievementDto.Name,
                    Amount = achievementDto.Amount,
                    BadgeImage = achievementDto.BadgeImage,
                    Description = achievementDto.Description,
                    LevelThreshold = achievementDto.LevelThreshold,
                    RewardAmount = achievementDto.RewardAmount,
                    Enabled = achievementDto.Enabled,
                    Period = achievementDto.Period,
                    TimeThreshold = achievementDto.TimeThreshold,
                    RewardType = achievementDto.RewardType,
                    Visible = achievementDto.Visible,
                    Activity = activity
                };
                var created = db.Achievements.Add(achievement);
                db.SaveChanges();
                return created.Entity;
            }
        }
        public Achievement UpdateAchievement(AchievementDto achievementDto)
        {
            using (var db = new DatabaseContext())
            {
                var achievement = db.Achievements.FirstOrDefault(x => x.ID == achievementDto.ID);
                var activity = db.Activities.FirstOrDefault(x => x.ID == achievementDto.Activity);
                if (activity == null)
                {
                    throw new Exception("Activity does not exist");
                }
                if (activity == null)
                {
                    throw new Exception("Achievement does not exist");
                }

                achievement.Name = achievementDto.Name;
                achievement.Amount = achievementDto.Amount;
                achievement.BadgeImage = achievementDto.BadgeImage;
                achievement.Description = achievementDto.Description;
                achievement.LevelThreshold = achievementDto.LevelThreshold;
                achievement.RewardAmount = achievementDto.RewardAmount;
                achievement.Enabled = achievementDto.Enabled;
                achievement.Period = achievementDto.Period;
                achievement.TimeThreshold = achievementDto.TimeThreshold;
                achievement.RewardType = achievementDto.RewardType;
                achievement.Visible = achievementDto.Visible;
                achievement.Activity = activity;
                var updated = db.Achievements.Update(achievement);
                db.SaveChanges();
                return updated.Entity;
            }
        }
        public bool DeleteAchievement(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var achievement = db.Achievements.FirstOrDefault(x => x.ID == id);

                db.Achievements.Remove(achievement);
                db.SaveChanges();
                return true;
            }
        }
        public List<Achievement> GetAllAchievements()
        {
            using (var db = new DatabaseContext())
            {
                return db.Achievements.Include("Activity").ToList();
            }
        }
        public Achievement GetAchievement(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var achievement = db.Achievements.Include("Activity").FirstOrDefault(x => x.ID == id);
                if (achievement == null)
                {
                    throw new Exception("Achievement does not exist");
                };
                return achievement;
            }
        }

        public List<Activity> GetAllActivities()
        {
            using (var db = new DatabaseContext())
            {
                return db.Activities.ToList();
            }
        }
        ~AchievementService()

        {
            Context.Dispose();
        }
    }
}

