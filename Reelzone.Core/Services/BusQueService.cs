﻿using AutoMapper;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using System;
using System.Configuration;
using System.Net;
using System.Text;

namespace Reelzone.Core.Services
{
    public class BusQueService : IBusQueService
    {
        public DatabaseContext Context;
        public IMapper MapperService;

        private string QueueName = "reelzone-user-acitivty";

        private QueueClient Client = null;

        public BusQueService(DatabaseContext dbContext)
        {
            Context = dbContext;

            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;
            // Create the queue if it does not exist already
            //TODO: Get from settings
            string connectionString = ConfigurationManager.AppSettings.Get("Microsoft.ServiceBus.ConnectionString");

            Client = new QueueClient(connectionString, QueueName, ReceiveMode.ReceiveAndDelete);
        }

        public bool SendUserActivity(Guid userID, string ActivityHandle)
        {
            var UserActivityMessage = new UserActivityMessage()
            {
                UserID = userID,
                ActivityHandle = ActivityHandle
            };

            var sendMessage = Send("internal-activity", JsonConvert.SerializeObject(UserActivityMessage));

            if (sendMessage)
            {
                return true;
            }
            else
            {
                return false;
                //TODO: throw
            }
        }

        public bool SendUserLevelUp(Guid userID) //TODO: int newLevel
        {
            var UserLevelUpMessage = new UserLevelUpMessage()
            {
                UserID = userID
            };

            var sendMessage = Send("external-levelup", JsonConvert.SerializeObject(UserLevelUpMessage));

            if (sendMessage)
            {
                return true;
            }
            else
            {
                return false;
                //TODO: throw
            }
        }

        public bool NotifyChallengeCompleted(UserAchievement userAchievement, Guid userID)
        {
            var AchievementCompleteMessage = new AchievementCompleteMessage()
            {
                UserID = userID,
                AchievementName = userAchievement.Achievement.Name
            };

            var sendMessage = Send("external-challenge-complete", JsonConvert.SerializeObject(AchievementCompleteMessage));

            if (sendMessage)
            {
                return true;
            }
            else
            {
                return false;
                //TODO: throw
            }
        }

        public bool SendUserSpin(Guid attenderID, Guid tournamentID, int score)
        {
            var UserSpinMessage = new UserSpinMessage()
            {
                AttenderID = attenderID,
                TournamentID = tournamentID,
                Score = score
            };

            var sendMessage = Send("internal-spin", JsonConvert.SerializeObject(UserSpinMessage));

            if (sendMessage)
            {
                return true;
            }
            else
            {
                return false;
                //TODO: throw
            }
        }

        public bool Send(string label, string topic)
        {
            if (Client.IsClosedOrClosing == false)
            {
                var messageEntity = new Message()
                {
                    Label = label,
                    Body = Encoding.ASCII.GetBytes(topic)
                };

                var result = Client.SendAsync(messageEntity);
            }

            return false;
        }
    }
}