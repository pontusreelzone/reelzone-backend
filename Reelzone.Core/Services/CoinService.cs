﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using System.Text;
using Reelzone.Core.Dtos;
using System.Text.RegularExpressions;
using Reelzone.Core.Helpers;
using System.Data.Entity;

namespace Reelzone.Core.Services
{
    public class CoinService : ICoinService
    {
        public DatabaseContext Context;

        public CoinService(DatabaseContext dbContext)
        {
            Context = dbContext;
        }

        public long GetAmount(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                return db.Coins.Where(x => x.User.ID == userId).Sum(x => x.Amount);
            }
        }

        public long CreateCoin(CoinDto coin)
        {
            using (var db = new DatabaseContext())
            {
                var cointEntity = db.Coins.Add(new Coin()
                {
                    User = db.Users.FirstOrDefault(x => x.ID == coin.User.ID),
                    Amount = coin.Amount,
                    Description = coin.Description,
                    Created = coin.Created
                });
                db.SaveChanges();

                return db.Coins.Where(x => x.User.ID == coin.User.ID).Sum(x => x.Amount);
            }
        }

        ~CoinService()
        {
            Context.Dispose();
        }
    }
}