﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reelzone.Core.Services
{
    public class CountryService : ICountryService
    {
        public DatabaseContext Context;
        public IUserService UserService;

        public CountryService(DatabaseContext dbContext, IUserService userService)
        {
            Context = dbContext;
            UserService = userService;
        }
        public Country CreateCountry(CountryDto countryDto)
        {
            var country = new Country()
            {
                CountryFlag = countryDto.CountryFlag,
                CountryIso = countryDto.CountryIso,
                Name = countryDto.Name
            };
            var createdCountry = Context.Countries.Add(country);
            Context.SaveChanges();

            return createdCountry.Entity;
        }


        public List<Country> AllCountries()
        {
            using (var db = new DatabaseContext())
            {
                var countries = db.Countries.ToList();

                return countries;
            }
        }

        public Country UpdateCountry(CountryDto countryDto)
        {
            using (var db = new DatabaseContext())
            {
                var country = db.Countries.FirstOrDefault(x => x.ID == countryDto.ID);
                if (country == null)
                {
                    throw new Exception("Country does not exist");
                }
                country.CountryFlag = countryDto.CountryFlag;
                country.CountryIso = countryDto.CountryIso;
                country.Name = countryDto.Name;

                db.Countries.Update(country);
                db.SaveChanges();
                return country;


            }
        }

        public Country AddCountry(CountryDto countryDto)
        {
            using (var db = new DatabaseContext())
            {
                var country = new Country();
                country.CountryFlag = countryDto.CountryFlag;
                country.CountryIso = countryDto.CountryIso;
                country.Name = countryDto.Name;

                var created = db.Countries.Add(country);
                db.SaveChanges();
                return created.Entity;
            }
        }

        public bool DeleteCountry(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var country = db.Countries.FirstOrDefault(x => x.ID == id);
                if (country == null)
                {
                    throw new Exception("Country does not exist");
                }

                var users = db.Users.Where(x => x.Country.ID == id).ToList();
                foreach (var user in users)
                {
                    user.Country = null;
                }
                db.Users.UpdateRange(users);
                db.Countries.Remove(country);
                db.SaveChanges();
                return true;
            }
        }

        public Country GetCountry(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var country = db.Countries.FirstOrDefault(x => x.ID == id);
                if (country == null)
                {
                    throw new Exception("Country not found");
                }

                return country;
            }
        }
        public Dictionary<string,int> GetDashboardCoutryData()
        {
            using (var db = new DatabaseContext())
            {
                var dashboardData = new Dictionary<string, int>();

                var countries = AllCountries();

                foreach (var country in countries)
                {
                    var total = Context.Users.Where(x => x.Country == country).Count();
                    dashboardData.Add(country.CountryIso, total);
                }

                return dashboardData;
            }
        }

        ~CountryService()
        {
            Context.Dispose();
        }
    }
}
