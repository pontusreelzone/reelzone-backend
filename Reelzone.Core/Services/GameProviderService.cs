﻿using AutoMapper;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Reelzone.Core.Services
{
    public class GameProviderService : IGameProviderService
    {
        public DatabaseContext Context;

        public IMapper AutoMapper;
        public GameProviderService(DatabaseContext dbContext,IAutoMapper autoMapper) 
        {
            Context = dbContext;
            AutoMapper = autoMapper.Get();
        }


        public GameProvider AddGameProvider(GameProviderDto providerDto)
        {
            
            var provider=AutoMapper.Map<GameProviderDto,GameProvider>(providerDto);
            
            using (var db = new DatabaseContext())
            {
                var createdProvider = db.GameProviders.Add(provider);
                db.SaveChanges();

                return createdProvider.Entity;
            }
        }
        public GameProvider UpdateGameProvider(GameProviderDto providerDto)
        {
            var provider = AutoMapper.Map<GameProviderDto, GameProvider>(providerDto);
            using (var db = new DatabaseContext())
            {
                var createdProvider = db.GameProviders.Update(provider);
                db.SaveChanges();

                return createdProvider.Entity;
            }
        }
        public bool DeleteProvider(Guid id)
        {
            var provider = Context.GameProviders.FirstOrDefault(x => x.ID == id);
            if (provider == null)
            {
                return false;
            }
            //var games = Context.Games.Where(x => x.GameProvider == provider).ToList();
            //foreach (var game in games)
            //{
            //    var tournaments = Context.Tournaments.Where(x => x.Game == game).ToList();
            //    foreach (var tournament in tournaments)
            //    {
            //        tournament.Game = null;
            //        Context.Update(tournament);
            //    };


            //}
            //Context.SaveChanges();
            //Context.Games.RemoveRange(games);
            //Context.SaveChanges();

            Context.GameProviders.Remove(provider);
            Context.SaveChanges();
            return true;

        }
        public GameProvider GetProvider(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var provider = db.GameProviders.FirstOrDefault(x => x.ID == id);
                if (provider== null)
                {
                    return provider;
                }
                provider.Users = null;
                var games = db.Games.Where(x => x.GameProvider == provider).ToList();
                provider.Games = games;
                return provider;
            }
        }
        public List<GameProvider> GetProviders()
        {
            using (var db = new DatabaseContext())
            {
                var providers = db.GameProviders.ToList();
                foreach (var provider in providers)
                {
                    provider.Users = null;

                    var games = db.Games.Where(x => x.GameProvider == provider).ToList();
                    foreach (var game in games)
                    {
                        game.GameProvider = null;
                    }
                    provider.Games = games;

                }

                return providers;
            }
        }





        ~GameProviderService()

        {
            Context.Dispose();
        }
    }
}