﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reelzone.API.ApiModels;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Reelzone.Core.Services
{
    public class GameService : IGameService
    {
        public DatabaseContext Context;
        public ICoinService CoinService;
        public IMapper AutoMapper;
        public IUserService UserService;

        public GameService(DatabaseContext dbContext, IAutoMapper autoMapper, ICoinService coinService, IUserService userService)
        {
            Context = dbContext;
            AutoMapper = autoMapper.Get();
            CoinService = coinService;
            UserService = userService;
        }

        

        public List<string> GetNamesOfGames()
        {
            List<string> games = new List<string>();

            games = Context.Games.Select(e => e.Name).ToList();

            return games;
        }

        public List<Game> AllGames()
        {
            using (var db = new DatabaseContext())
            {
                var games = db.Games.Where(x => x.Visible == true).ToList();
                foreach (var game in games)
                {
                    var gameProvider = db.GameProviders.FirstOrDefault(x => x.Games.Contains(game));
                    gameProvider.Games = null;
                    game.GameProvider = gameProvider;
                }
                return games;
            }
        }

        public List<GameProvider> GetGameProviders()
        {
            using (var db = new DatabaseContext())
            {
                var providers = db.GameProviders.ToList();

                var returnProviders = new List<GameProvider>();

                foreach (var provider in providers)
                {
                    provider.Games = db.Games.Where(x => x.GameProvider.ID == provider.ID && x.Visible == true)
                        .ToList()
                        .Select(game =>
                        {
                            game.GameProvider = null;
                            return game;
                        })
                        .ToList();
                    if (provider.Games.Count() > 0)
                    {
                        returnProviders.Add(provider);
                    }
                }
                return providers;
            }
        }

        public List<Game> GetFeaturedGames()
        {
            using (var db = new DatabaseContext())
            {
                var games = db.Games.Where(x => x.Featured == true && x.Visible == true).ToList();

                return games;
            }
        }

        public List<GameLeaderBoardModel> GetLeaderboard()
        {
            using (var db = new DatabaseContext())
            {
                var leaderboard = new List<GameLeaderBoardModel>();
                var users = db.Users.ToList();
                foreach (var user in users)
                {
                    var board = new GameLeaderBoardModel()
                    {
                        Coins = CoinService.GetAmount(user.ID),
                        User = UserService.GetCleanUser(user.ID)
                    };
                    leaderboard.Add(board);
                }

                leaderboard = leaderboard.OrderByDescending(x => x.Coins).Take(25).ToList();

                return leaderboard;
            }
        }

        public Game UpdateGame(GameDto gameDto)
        {
            using (var db = new DatabaseContext())
            {
                var gameProvider = db.GameProviders.FirstOrDefault(x => x.ID == gameDto.GameProvider.ID);
                if (gameProvider == null || db.Games.FirstOrDefault(x => x.ID == gameDto.ID) == null)
                {
                    throw new Exception("Provider does not exist");
                }
                gameProvider.Games = null;
                var gameEntity = AutoMapper.Map<GameDto, Game>(gameDto);
                gameEntity.GameProvider = gameProvider;
                var local = db.Set<Game>()
                .Local
                .FirstOrDefault(entry => entry.ID.Equals(gameEntity.ID));

                // check if local is not null
                if (local != null)
                {
                    // detach
                    db.Entry(local).State = EntityState.Detached;
                }
                // set Modified flag in your entry
                db.Entry(gameEntity).State = EntityState.Modified;

                db.Games.Update(gameEntity);
                db.SaveChanges();
                gameEntity.GameProvider.Games = null;
                return gameEntity;
            }
        }

        public Game AddGame(GameDto gameDto)
        {
            using (var db = new DatabaseContext())
            {
                var gameProvider = db.GameProviders.FirstOrDefault(x => x.ID == gameDto.GameProvider.ID);
                if (gameProvider == null)
                {
                    throw new Exception("Provider does not exist");
                }

                var gameEntity = AutoMapper.Map<GameDto, Game>(gameDto);
                gameEntity.GameProvider = gameProvider;

                db.Games.Add(gameEntity);
                db.SaveChanges();
                gameEntity.GameProvider.Games = null;
                return gameEntity;
            }
        }

        public bool DeleteGame(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var game = db.Games.Include(x => x.GameProvider).FirstOrDefault(x => x.ID == id);
                var tournaments = db.Tournaments.Where(x => x.Game == game).ToList();
                foreach (var tournament in tournaments)
                {
                    tournament.Game = null;
                    db.Update(tournament);
                };

                db.SaveChanges();
                if (game == null)
                {
                    return false;
                }
                //db gameToutnamentKey = db.
                db.Remove(game);
                db.SaveChanges();
                return true;
            }
        }

        public Game GetGame(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var game = db.Games.FirstOrDefault(x => x.ID == id);
                var gameProvider = db.GameProviders.FirstOrDefault(x => x.Games.Contains(game));
                if (gameProvider == null)
                {
                    throw new Exception("Provider does not exist");
                }
                gameProvider.Games = null;
                game.GameProvider = gameProvider;
                return game;
            }
        }

        ~GameService()
        {
            Context.Dispose();
        }
    }
}