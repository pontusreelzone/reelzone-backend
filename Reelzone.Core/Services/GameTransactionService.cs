﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;

namespace Reelzone.Core.Services
{
    public class GameTransactionService : IGameTransactionService
    {
        public DatabaseContext Context;

        public GameTransactionService(DatabaseContext dbContext)
        {
            Context = dbContext;
        }

        public GameTransaction Create(GameTranscationDto transactionDto)
        {
            using (var db = new DatabaseContext())
            {
                Tournament tournament;

                if (transactionDto.Tournament != null)
                {
                    tournament = db.Tournaments.FirstOrDefault(x => x.ID == transactionDto.Tournament.ID);
                }
                else
                {
                    tournament = null;
                }

                var transaction = new GameTransaction()
                {
                    Tournament = tournament,
                    User = db.Users.FirstOrDefault(x => x.ID == transactionDto.User.ID),
                    ExternalTransactionId = transactionDto.ExternalTransactionId,
                    Description = transactionDto.Description,
                    Amount = transactionDto.Amount
                };

                var createdTransaction = db.GameTransactions.Add(transaction);
                db.SaveChanges();

                return createdTransaction.Entity;
            }
        }

        public List<GameTransaction> FilteredGameTransaction(DateTime? start, DateTime? end, string UserID)
        {
            try
            {
                var user = Context.Users.FirstOrDefault(x => x.ID == new Guid(UserID));
                if (user == null)
                {
                    throw new Exception("User not found");
                }
                if (start != null && end != null)
                {
                    return Context.GameTransactions.Include("Tournament")
                       .Where(x => x.User == user
                              && x.Created >= start
                              && x.Created <= end)
                       .ToList();
                }
                else
                {
                    throw new Exception("Start time and end time needs to be specified");
                }
            }
            catch (Exception)
            {
                throw new Exception("Invalid id for the user");
            }
        }

        public GameTransaction Find(Int64 externalTransactionId)
        {
            using (var db = new DatabaseContext())
            {
                var checkTransaction =
                db.GameTransactions.FirstOrDefault(x => x.ExternalTransactionId == externalTransactionId);

                return checkTransaction;
            }
        }

        ~GameTransactionService()
        {
            Context.Dispose();
        }
    }
}