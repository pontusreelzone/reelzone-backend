﻿using Hangfire;
using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;

namespace Reelzone.Core.Services
{
    public class HangfireService : IHangfireService
    {
        public IUserAchievmentService UserAchievementService;
        public DatabaseContext Context;

        public HangfireService(DatabaseContext dbContext, IUserAchievmentService userAchievementService)
        {
            Context = dbContext;
            UserAchievementService = userAchievementService;
        }

        public void DeclareJobs()
        {
            RecurringJob.AddOrUpdate(recurringJobId: "Achievements-Daily", methodCall: () => UserAchievementService.GenerateUserAnchievementsJobs(AchievementPeriod.Day), Cron.Daily);
            RecurringJob.AddOrUpdate(recurringJobId: "Achievements-Weekly", methodCall: () => UserAchievementService.GenerateUserAnchievementsJobs(AchievementPeriod.Week), Cron.Weekly);
            RecurringJob.AddOrUpdate(recurringJobId: "Achievements-Monthly", methodCall: () => UserAchievementService.GenerateUserAnchievementsJobs(AchievementPeriod.Month), Cron.Monthly);
        }
    }
}