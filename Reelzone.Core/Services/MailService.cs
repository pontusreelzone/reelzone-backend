﻿using Hangfire;
using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.IO;
using System.Net;
using MailChimp;
using MailChimp.Net.Models;
using MailChimp.Net.Interfaces;
using MailChimp.Net;
using Mandrill;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MailChimp.Net.Core;
using System.Reflection.Metadata.Ecma335;
using Newtonsoft.Json;
using System.Configuration;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using Microsoft.AspNetCore.Identity;
using Reelzone.Core.Dtos;

namespace Reelzone.Core.Services
{
    public class MailService : IMailService
    {
        public IUserExperienceService ExperienceService;
        public DatabaseContext Context;
        public MailChimpManager mailChimpManager;
        public MandrillApi mandrill;

        public MailService(DatabaseContext dbContext, IUserExperienceService userExperienceService)
        {
            var mailchimpAPI = ConfigurationManager.AppSettings.Get("MailchimpAPIKey");
            var mandrillAPI = ConfigurationManager.AppSettings.Get("MandrillAPIKey");
            var mandrillURL = ConfigurationManager.AppSettings.Get("MandrillURL");

            Context = dbContext;
            ExperienceService = userExperienceService;
            mailChimpManager = new MailChimpManager("b6f8bb19f7c03f74e0991d92e126a644-us8");
            mandrill = new MandrillApi(mandrillAPI);
        }

        //TODO move to config
        //private static readonly IMailChimpManager mailChimpManager = new MailChimpManager("b6f8bb19f7c03f74e0991d92e126a644 - us8");

        public async Task<bool> AddSubscriberToCampaignList(string emailAddress, string listID)
        {
            var listId = listID;

            //Create member
            var member = new Member
            {
                EmailAddress = emailAddress,
                StatusIfNew = Status.Subscribed,
                TimestampSignup = DateTime.UtcNow.ToString("s"),
            };

            //Add member to list
            var model = await this.mailChimpManager.Members.AddOrUpdateAsync("2837110d95", member);
            return true;
        }

        public async Task DeleteSubscriberToCampaignList(string emailAddress, string listID)
        {
            var listId = listID;

            //Add member to list
            await this.mailChimpManager.Members.DeleteAsync(listId, emailAddress);
        }

        public async Task<bool> SendVerificationEmail(string emailAddress, string verificationLink)
        {
            string mandrillTemplateId = "Reelzone-Reactiviation-Template";

            var emailMessage = new EmailMessage
            {
                To = new[] { new EmailAddress(emailAddress) },
                FromEmail = "no-reply@reelzone.app",
                Subject = "Important Notice Reminder - Reelzone",
                FromName = "Reelzone"
            };

            emailMessage.AddGlobalVariable("LINK", verificationLink);
            emailMessage.AddGlobalVariable("INTERNAL_USERNAME", emailAddress);

            var result = await mandrill.SendMessageTemplate(
                 new SendMessageTemplateRequest(emailMessage, mandrillTemplateId, null));

            return true;
        }

        public async Task<bool> BanUser(string emailAddress, string message)
        {
            string mandrillTemplateId = "reelzone-ban-user";

            var emailMessage = new EmailMessage
            {
                MergeLanguage = TemplateSyntax.Handlebars,
                To = new[] { new EmailAddress(emailAddress) },
                FromEmail = "no-reply@reelzone.app",
                Subject = "Banned"
            };

            emailMessage.AddGlobalVariable("message", message);

            var result = await mandrill.SendMessageTemplate(
                 new SendMessageTemplateRequest(emailMessage, mandrillTemplateId, null));

            return true;
        }

        public async Task<bool> SendWinnerEmail(string emailAddress, UserDto user, TournamentDto tournament, TournamentAttendeDto tournamentAttendeDto)
        {
            string mandrillTemplateId = "reelzone-winner";

            var emailMessage = new EmailMessage
            {
                To = new[] { new EmailAddress(emailAddress) },
                FromEmail = "no-reply@reelzone.app",
                Subject = "You Are a Winner"
            };

            emailMessage.AddGlobalVariable("INTERNAL_USERNAME", user.Username);
            emailMessage.AddGlobalVariable("INTERNAL_TOURNAMENT", tournament.Name);
            if (tournamentAttendeDto.CoinPrize == true)
            {
                emailMessage.AddGlobalVariable("INTERNAL_WIN", tournamentAttendeDto.CurrentPrize + "coins");
            }
            else
            {
                emailMessage.AddGlobalVariable("INTERNAL_WIN", "€" + tournamentAttendeDto.CurrentPrize);
            }
            emailMessage.AddGlobalVariable("INTERNAL_TOURNAMENT_DATE", tournament.StartDOT.ToShortDateString());

            var result = await mandrill.SendMessageTemplate(
                 new SendMessageTemplateRequest(emailMessage, mandrillTemplateId, null));

            return true;
        }

        public async Task<bool> SendVerifiedEmail(string emailAddress)
        {
            string mandrillTemplateId = "reelzone-account-verified";

            var emailMessage = new EmailMessage
            {
                To = new[] { new EmailAddress(emailAddress) },
                FromEmail = "no-reply@reelzone.app",
                Subject = "Account Verified"
            };

            var result = await mandrill.SendMessageTemplate(
                 new SendMessageTemplateRequest(emailMessage, mandrillTemplateId, null));

            return true;
        }

        public async Task<bool> SendForgotPasswordEmail(string emailAddress, string verificationLink)
        {
            string mandrillTemplateId = "reelzone-forgot-password";

            var emailMessage = new EmailMessage
            {
                To = new[] { new EmailAddress(emailAddress) },
                FromEmail = "no-reply@reelzone.app",
                Subject = "Reset Password"
            };

            emailMessage.AddGlobalVariable("LINK", verificationLink);

            var result = await mandrill.SendMessageTemplate(
                 new SendMessageTemplateRequest(emailMessage, mandrillTemplateId, null));
            return true;
        }
    }
}