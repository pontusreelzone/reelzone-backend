﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using System.Text;
using Reelzone.Core.Dtos;
using System.Text.RegularExpressions;
using Reelzone.Core.Helpers;
using System.Data.Entity;
using PusherServer;
using System.Threading.Tasks;

namespace Reelzone.Core.Services
{
    public class PushService : IPushService
    {
        public Pusher Pusher;

        public PushService()
        {
            var options = new PusherOptions
            {
                Cluster = "eu",
                Encrypted = true
            };

            this.Pusher = new Pusher(
               "1071843",
               "3230a40788f2cbe7753a",
               "b513eb33d5b26d8d3373",
               options);
        }

        public async Task<bool> SendAll(string eventType, string message, string title, string type)
        {
            var result = await this.Pusher.TriggerAsync(
              "reelzone-all",
              eventType,
              new { message = message, title = title, type = type });
            return true;
        }

        public async Task<bool> Send(Guid userId, string eventType, string message, string title, string type)
        {
            var result = await this.Pusher.TriggerAsync(
              "reelzone-" + userId.ToString(),
              eventType,
              new { message = message, title = title, type = type });
            return true;
        }
    }
}