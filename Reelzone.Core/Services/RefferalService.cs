﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;


namespace Reelzone.Core.Services
{
    public class RefferalService : IRefferalService
    {
        public DatabaseContext Context;
        public IUserRefferalService UserRefferalService;
        public IMapper AutoMapper;      
        public IUserService UserService;
      

        

        public RefferalService(DatabaseContext dbContext
                                                        ,IUserRefferalService userRefferalService
                                                        , IUserService userService
                                                        , IAutoMapper autoMapper)
        {
            Context = dbContext;
            UserService = userService;
            AutoMapper = autoMapper.Get();
            UserRefferalService = userRefferalService;

        }

        public List<RefferalDto> GetRefferals(int page)
        {
            using (var db = new DatabaseContext())
            {
                var refferals = db.Refferals.Skip(page*50).Take(50).ToList();
               
                var refferalsDto = new List<RefferalDto>();
                if (refferals == null)
                {
                    return refferalsDto;
                }
                foreach (var refferal in refferals)
                {
                    RefferalDto refferalDto = new RefferalDto()
                    {
                        ID = refferal.ID,
                        Amount = refferal.Amount,
                        Code = refferal.Code,
                        Description = refferal.Description,
                        UsersRefferals = new List<UserRefferalDto>()
                        
                    };
                    var userRefferals = UserRefferalService.GetUserRefferalsByRefferal(refferal.ID);
                    foreach (var userRefferal in userRefferals)
                    {
                        UserRefferalDto userRefferalDto = new UserRefferalDto()
                        {
                            ID = userRefferal.ID,
                            UsedTimestamp = userRefferal.UsedTimestamp,
                            User = userRefferal.User.Username
                        };
                        refferalDto.UsersRefferals.Add(userRefferalDto);
                    }
                    refferalsDto.Add(refferalDto);
                }
                return refferalsDto;
            }
        }
        public List<RefferalDto> GetFillteredRefferals(string code)
        {
            using (var db = new DatabaseContext())
            {
                var refferals = db.Refferals.Where(x=>x.Code.Contains(code)).ToList();

                var refferalsDto = new List<RefferalDto>();
                if (refferals == null)
                {
                    return refferalsDto;
                }
                foreach (var refferal in refferals)
                {
                    RefferalDto refferalDto = new RefferalDto()
                    {
                        ID = refferal.ID,
                        Amount = refferal.Amount,
                        Code = refferal.Code,
                        Description = refferal.Description,
                        UsersRefferals = new List<UserRefferalDto>()

                    };
                    var userRefferals = UserRefferalService.GetUserRefferalsByRefferal(refferal.ID);
                    foreach (var userRefferal in userRefferals)
                    {
                        UserRefferalDto userRefferalDto = new UserRefferalDto()
                        {
                            ID = userRefferal.ID,
                            UsedTimestamp = userRefferal.UsedTimestamp,
                            User = userRefferal.User.Username
                        };
                        refferalDto.UsersRefferals.Add(userRefferalDto);
                    }
                    refferalsDto.Add(refferalDto);
                }
                return refferalsDto;
            }
        }

        

        public Refferal CreateRefferal(RefferalDto refferalDto)
        {
            var code = Context.Refferals.FirstOrDefault(x => x.Code == refferalDto.Code);
            if (code!=null)
            {
                throw new Exception("Code allreay exists");
            }

            Refferal refferal = new Refferal()
            {
                Amount = refferalDto.Amount,
                Code = refferalDto.Code,
                Description = refferalDto.Description,
            };
            var r =  Context.Refferals.Add(refferal).Entity;
            Context.SaveChanges();
            return r;

        }
        public bool DeleteRefferal(Guid ID)
        {
            using (var db = new DatabaseContext())
            {
                var refferal = db.Refferals.FirstOrDefault(x => x.ID == ID);
                if (refferal ==null)
                {
                    throw new Exception("Refferal  doesn't exists");
                }
                var userRefferals = UserRefferalService.GetUserRefferalsByRefferal(ID);
                foreach (var userRefferal in userRefferals)
                {
                    userRefferal.Refferal = null;
                }
                db.UserRefferals.UpdateRange(userRefferals);
                db.Refferals.Remove(refferal);
                db.SaveChanges();
                return true;
            }
        }
        public RefferalDto UpdateReferal(RefferalDto refferalDto)
        {
            using (var db = new DatabaseContext())
            {
                var code = db.Refferals.FirstOrDefault(x => x.ID == refferalDto.ID);
                if (code == null)
                {
                    throw new Exception("Code doesn't exists");
                }
                code.Amount = refferalDto.Amount;
                code.Code = refferalDto.Code;
                code.Description = refferalDto.Description;
                db.Refferals.Update(code);
                db.SaveChanges();

                var userRefferals = UserRefferalService.GetUserRefferalsByRefferal(code.ID);
                foreach (var userRefferal in userRefferals)
                {
                    UserRefferalDto userRefferalDto = new UserRefferalDto()
                    {
                        ID = userRefferal.ID,
                        UsedTimestamp = userRefferal.UsedTimestamp,
                        User = userRefferal.User.Username
                    };
                    refferalDto.UsersRefferals.Add(userRefferalDto);
                }




                return refferalDto;
            }
        }


        ~RefferalService()
        {
            Context.Dispose();
        }
    }
    
}
