﻿using Reelzone.Core.Interfaces;
using SocketIOClient;
using System.Text;

namespace Reelzone.Core.Services
{
    public class SocketService : ISocketService
    {
        public SocketIO client;
        public bool connected = false;

        public SocketService()
        {
            client = new SocketIO("http://localhost:4001");
            client.OnConnected += Client_OnConnected;
            client.OnClosed += Client_OnClosed;
            ConnectAsync();
        }

        public async void ConnectAsync()
        {
            await client.ConnectAsync();
        }

        public async void Emit(string topic, string data)
        {
            if (connected)
            {
                await client.EmitAsync(topic, new
                {
                    body = new
                    {
                        data = Encoding.UTF8.GetBytes(data.ToString()),
                        mimeType = "text/plain"
                    }
                });
            }
        }

        public void Client_OnConnected()
        {
            connected = true;
        }

        public void Client_OnClosed(ServerCloseReason reason)
        {
            connected = false;
        }
    }
}