﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Azure.Amqp.Framing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Reelzone.Core.Services
{
    public class TournamentService : ITournamentService
    {
        public IUserActivityService UserActivityService;
        public IBusQueService BusQueService;
        public ICoinService CoinService;
        public DatabaseContext Context;
        public IMapper AutoMapper;
        public static DateHelper DateHelper;

        public TournamentService(IUserActivityService userActivityService, IBusQueService busQueService, IAutoMapper autoMapper, ICoinService coinService)
        {
            UserActivityService = userActivityService;
            BusQueService = busQueService;
            Context = new DatabaseContext();
            AutoMapper = autoMapper.Get();
            DateHelper = new DateHelper();
            CoinService = coinService;
        }

        public TournamentDto Update(TournamentDto tournamentDto, string identifier, string game)
        {
            //check if there is existing tournament with a given ID
            var tournament = Context.Tournaments.FirstOrDefault(x => x.ID == tournamentDto.ID);

            Guid gameId = new Guid();
            try
            {
                gameId = new Guid(game);
            }
            catch (Exception)
            {
                throw;
            }
            //check if there is a given game
            tournamentDto.Game = Context.Games.FirstOrDefault(g => g.ID == gameId);
            if (tournamentDto.Game == null)
            {
                throw new Exception("Game not received");
            }
            //Check if the start time is before the end time
            int result = DateTime.Compare(tournamentDto.StartDOT, tournamentDto.EndDOT);
            if (result >= 0)
            {
                throw new Exception("Start needs to be before end date");
            }
            //UPDATE tournament
            if (tournament != null)
            {
                tournament.Retryable = tournamentDto.Retryable;
                tournament.Name = tournamentDto.Name;
                tournament.Description = tournamentDto.Description;
                tournament.Format = tournamentDto.Format;
                tournament.StartDOT = tournamentDto.StartDOT;
                tournament.EndDOT = tournamentDto.EndDOT;
                tournament.StartSpins = tournamentDto.StartSpins;
                tournament.HeaderImage = tournamentDto.HeaderImage;
                tournament.Game = tournamentDto.Game;
                tournament.Exclusive = tournamentDto.Exclusive;

                var updated = Context.Tournaments.Update(tournament);
                Context.SaveChanges();

                //Check if there is an existing prize pool to update or
                //if it does not match any of the  existing to create new

                var prizePools = Context.TournamentPrizepools
                                .Where(x => x.Tournament.ID == tournament.ID)
                                .ToList();
                TournamentPrizepool Prz = new TournamentPrizepool();
                bool exist = false;
                //check if exist and update if does not exist add it
                foreach (var prizepoolDto in tournamentDto.Prizepool)
                {
                    exist = false;
                    foreach (var prizepool in prizePools)
                    {
                        if (prizepool.ID == prizepoolDto.ID && (prizepool.Position != prizepoolDto.Position || prizepool.Prize != prizepoolDto.Prize))
                        {
                            exist = true;
                            prizepool.Prize = prizepoolDto.Prize;
                            prizepool.Position = prizepoolDto.Position;
                            prizepool.CoinPrize = prizepoolDto.CoinPrize;
                            Context.TournamentPrizepools.Update(prizepool);
                            Context.SaveChanges();
                        }
                        if (prizepool.ID == prizepoolDto.ID)
                        {
                            exist = true;
                        }
                    }
                    if (!exist)
                    {
                        Prz.Tournament = updated.Entity;
                        Prz.Prize = prizepoolDto.Prize;
                        Prz.Position = prizepoolDto.Position;
                        Prz.CoinPrize = prizepoolDto.CoinPrize;

                        Context.TournamentPrizepools.Add(Prz);
                        Context.SaveChanges();
                    }
                    Prz = new TournamentPrizepool();
                }
                foreach (var prizepool in prizePools)
                {
                    exist = false;
                    foreach (var prizepoolDto in tournamentDto.Prizepool)
                    {
                        if (prizepool.ID == prizepoolDto.ID)
                        {
                            exist = true;
                        }
                    }
                    if (!exist)
                    {
                        Context.TournamentPrizepools.Remove(prizepool);
                        Context.SaveChanges();
                    }
                }
                var updatedTournament = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                return updatedTournament;
            }
            else
                throw new Exception("Tournament not received");
        }

        #region Filters

        public int GetAttenderPlacement(TournamentAttende attende)
        {
            using (var db = new DatabaseContext())
            {
                var attendes = db.TournamentAttendes.Where(x => x.Tournament.ID == attende.Tournament.ID).OrderByDescending(x => x.Score).ToList();

                return attendes.FindIndex(c => c.ID == attende.ID) + 1;
            }
        }

        public List<TournamentDto> DashBoardTournaments()
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments
                                      .OrderByDescending(t => t.StartDOT <= DateTime.UtcNow)
                                      .Take(10)
                                      .ToList();

                var tournamentsDto = new List<TournamentDto>();
                foreach (var tournament in tournaments)
                {
                    tournament.Attendes = db.TournamentAttendes.Include(x => x.User)
                                             .Where(a => a.Tournament == tournament)
                                             .OrderByDescending(a => a.Score)
                                             .Take(15)
                                             .ToList();
                    var atendesDto = new List<TournamentAttendeDto>();

                    foreach (var item in tournament.Attendes)
                    {
                        var attendee = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(item);
                        attendee.Placement = GetAttenderPlacement(item);
                        attendee.CurrentPrize = GetAttenderPrize(attendee.Placement, tournament);
                        attendee.Tournament = null;
                        attendee.User = null;
                        attendee.UserName = item.User.Username;
                        atendesDto.Add(attendee);
                    }
                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                    try
                    {
                        atendesDto.OrderBy(x => x.Placement);
                    }
                    catch (Exception)
                    {
                        tournamentDto.Attendes = atendesDto;
                    }

                    tournamentDto.TotalSpins = TournamentSpins(tournament);

                    tournamentDto.TotalAttendes = db.TournamentAttendes
                                                  .Where(a => a.Tournament == tournament).Count();
                    tournamentDto.Attendes = atendesDto;

                    tournamentsDto.Add(tournamentDto);
                }
                return tournamentsDto;
            }
        }

        public List<DashboardDto> FilteredCountAttendees(DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                if (start != null && end == null)
                {
                    return db.TournamentAttendes
                                  .Where(x => x.Tournament.StartDOT >= start)
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
                if (start == null && end != null)
                {
                    return db.TournamentAttendes
                                  .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start)
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
                else
                {
                    return db.TournamentAttendes
                                  .Where(x => x.Tournament.EndDOT <= start)
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
            }
        }

        public List<DashboardDto> FilteredCountSpins(DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                if (start != null && end == null)
                {
                    return db.GameTransactions
                                  .Where(x => x.Tournament.StartDOT >= start && x.Description == "Bet on tournament")
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
                if (start == null && end != null)
                {
                    return db.GameTransactions
                                  .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start && x.Description == "Bet on tournament")
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
                else
                {
                    return db.GameTransactions
                                  .Where(x => x.Tournament.EndDOT <= start && x.Description == "Bet on tournament")
                                   .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                }
            }
        }

        public List<DashboardDto> FilteredByProviderCountAttendees(string providerID, DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (start != null && end == null)
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    if (start == null && end != null)
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    else
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.EndDOT <= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                       .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Wrong id for the provider");
                }
            }
        }

        public List<DashboardDto> FilteredByProviderCountSpins(string providerID, DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (start != null && end == null)
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    if (start == null && end != null)
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    else
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.EndDOT <= start && x.Tournament.Game.GameProvider.ID == new Guid(providerID))
                                       .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Wrong id for the provider");
                }
            }
        }

        public List<DashboardDto> FilteredByGameCountAttendees(string gameID, DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    var game = db.Games.FirstOrDefault(x => x.ID == new Guid(gameID));
                    if (start != null && end == null)
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.Game == game)
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    if (start == null && end != null)
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start && x.Tournament.Game == game)
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    else
                    {
                        return db.TournamentAttendes
                                      .Where(x => x.Tournament.EndDOT <= start && x.Tournament.Game == game)
                                       .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Wrong id for the game");
                }
            }
        }

        public List<DashboardDto> FilteredByGameCountSpins(string gameID, DateTime? start, DateTime? end)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    var game = Context.Games.FirstOrDefault(x => x.ID == new Guid(gameID));
                    if (start != null && end == null)
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.Game == game && x.Description == "Bet on tournament")
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    if (start == null && end != null)
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.StartDOT >= start && x.Tournament.EndDOT <= start && x.Tournament.Game == game && x.Description == "Bet on tournament")
                                      .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                    else
                    {
                        return db.GameTransactions
                                      .Where(x => x.Tournament.EndDOT <= start && x.Tournament.Game == game && x.Description == "Bet on tournament")
                                       .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                      .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                      {
                                          Year = key.Year,
                                          Month = key.Month,
                                          Day = key.Day,
                                          Total = group.Count()
                                      }).OrderBy(a => a.Year)
                                        .ThenBy(a => a.Month)
                                        .ThenBy(a => a.Day)
                                        .ToList();
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Wrong id for the game");
                }
            }
        }

        public int TournamentSpins(Tournament tournament)
        {
            using (var db = new DatabaseContext())
            {
                return db.GameTransactions.Where(x => x.Tournament == tournament && x.Description == "Bet on tournament").Count();
            }
        }

        public List<DashboardDto> CountSpins(bool montly)
        {
            using (var db = new DatabaseContext())
            {
                if (montly)
                {
                    return db.GameTransactions
                                  .Where(x => x.Tournament.StartDOT >= DateTime.UtcNow.AddDays(-31) && x.Description == "Bet on tournament")
                                 .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
                }
                return db.GameTransactions.Where(x => x.Description == "Bet on tournament").Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month })
                          .GroupBy(x => new { x.Year, x.Month }, (key, group) => new DashboardDto()
                          {
                              Year = key.Year,
                              Month = key.Month,
                              Day = null,
                              Total = group.Count()
                          }).OrderBy(a => a.Year)
                  .ThenBy(a => a.Month)
                  .ToList();
            }
        }

        public List<DashboardDto> CountAttendees(bool montly)
        {
            using (var db = new DatabaseContext())
            {
                if (montly)
                {
                    return db.TournamentAttendes
                                  .Where(x => x.Tournament.StartDOT >= DateTime.UtcNow.AddDays(-31))
                                  .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month, u.Tournament.StartDOT.Day })
                                  .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                                  {
                                      Year = key.Year,
                                      Month = key.Month,
                                      Day = key.Day,
                                      Total = group.Count()
                                  }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
                }
                return db.TournamentAttendes
                              .Select(u => new { u.Tournament.StartDOT.Year, u.Tournament.StartDOT.Month })
                              .GroupBy(x => new { x.Year, x.Month }, (key, group) => new DashboardDto()
                              {
                                  Year = key.Year,
                                  Month = key.Month,
                                  Day = null,
                                  Total = group.Count()
                              }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ToList();
            }
        }

        public List<TournamentDto> DashBoardProviderTournaments(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments.Where(x => x.Game.GameProvider.ID == id)
                                                .Include(x => x.Attendes)
                                                .ThenInclude(x => x.User)
                                                .OrderByDescending(t => t.StartDOT <= DateTime.UtcNow)
                                                .Take(10)
                                                .ToList();
                var tournamentsDto = new List<TournamentDto>();
                foreach (var tournament in tournaments)
                {
                    var atendesDto = new List<TournamentAttendeDto>();
                    tournament.Prizepool = db.TournamentPrizepools
                       .Where(x => x.Tournament.ID == tournament.ID)
                       .Select(x => new TournamentPrizepool
                       {
                           Prize = x.Prize,
                           CoinPrize = x.CoinPrize,
                           Position = x.Position
                       })
                       .ToList();
                    foreach (var item in tournament.Attendes)
                    {
                        var attendee = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(item);
                        attendee.Placement = GetAttenderPlacement(item, tournament.ID);
                        attendee.CurrentPrize = GetAttenderPrize(attendee.Placement, tournament);
                        attendee.CoinPrize = GetAttenderCoinPrize(attendee.Placement, tournament);
                        attendee.Tournament = null;
                        attendee.User = null;
                        attendee.UserName = item.User.Username;
                        atendesDto.Add(attendee);
                    }
                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                    tournamentDto.TotalSpins = TournamentSpins(tournament);
                    tournamentDto.Attendes = atendesDto;
                    tournamentsDto.Add(tournamentDto);
                }
                return tournamentsDto;
            }
        }

        #endregion Filters

        public TournamentDto CreateTournament(TournamentDto tournamentDto, string identifier, string game)
        {
            //check if there is existing tournament with a given ID
            var tournament = Context.Tournaments.FirstOrDefault(x => x.ID == tournamentDto.ID);

            if (tournament != null)
            {
                throw new Exception("Tournament not received");
            }

            Guid gameId = new Guid();
            try
            {
                gameId = new Guid(game);
            }
            catch (Exception)
            {
                throw;
            }

            tournamentDto.Game = Context.Games.FirstOrDefault(g => g.ID == gameId);
            if (tournamentDto.Game == null)
            {
                return null;
            }
            //(a.Start.Date >= startDate.Date && a.Start.Date <= endDate)
            var sameTournament = Context
                                        .Tournaments
                                        .Any
                                        (t => t.Name == tournamentDto.Name
                                        && t.StartDOT >= tournamentDto.StartDOT
                                        && t.EndDOT <= tournamentDto.EndDOT
                                        && t.Game.Name == game);
            if (sameTournament)
            {
                return null;
            }
            int result = DateTime.Compare(tournamentDto.StartDOT, tournamentDto.EndDOT);
            if (result >= 0)
            {
                throw new Exception("Start needs to be before end date");
            }
            //Create tournament
            else
            {
                tournament = new Tournament()
                {
                    Retryable = tournamentDto.Retryable,
                    StartSpins = tournamentDto.StartSpins,
                    Name = tournamentDto.Name,
                    Description = tournamentDto.Description,
                    Format = tournamentDto.Format,
                    StartDOT = tournamentDto.StartDOT,
                    EndDOT = tournamentDto.EndDOT,
                    HeaderImage = tournamentDto.HeaderImage,
                    Game = tournamentDto.Game,
                    Exclusive = tournamentDto.Exclusive
                };
                //Add the prizepool

                var createdTournament = Context.Tournaments.Add(tournament);
                Context.SaveChanges();

                TournamentPrizepool prizepool = new TournamentPrizepool();
                List<TournamentPrizepool> prizepools = new List<TournamentPrizepool>();

                foreach (var prizepooldto in tournamentDto.Prizepool)
                {
                    prizepool = new TournamentPrizepool
                    {
                        Tournament = createdTournament.Entity,
                        Position = prizepooldto.Position,
                        CoinPrize = prizepooldto.CoinPrize,
                        Prize = prizepooldto.Prize
                    };

                    prizepools.Add(prizepool);
                }

                Context.TournamentPrizepools.AddRange(prizepools);
                Context.SaveChanges();

                var createdTournamentAndPrizepools = GetTournament(createdTournament.Entity.ID);

                var CreatedTournament = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                return CreatedTournament;
            }
        }

        public bool DeleteTournament(Guid ID, string identifier)
        {
            var tournament = Context.Tournaments.Include("Game").FirstOrDefault(x => x.ID == ID);
            if (tournament == null)
            {
                throw new Exception("No tournament in the database like that");
            }

            var Prizepools = Context.TournamentPrizepools.Where(x => x.Tournament.ID == ID);

            Context.TournamentPrizepools.RemoveRange(Prizepools);

            var atendees = Context.TournamentAttendes.Where(x => x.Tournament.ID == ID);
            Context.TournamentAttendes.RemoveRange(atendees);
            var gt = Context.GameTransactions.Where(x => x.Tournament.ID == ID);
            Context.GameTransactions.RemoveRange(gt);
            Context.SaveChanges();

            Context.Tournaments.Remove(tournament);
            Context.SaveChanges();

            return true;
        }

        public bool isTournamentRunning(DateTime start, DateTime end)
        {
            var currentTime = DateHelper.ParseTimeZone(DateTime.UtcNow);
            if ((currentTime >= start && currentTime <= end))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<TournamentDto> GetTournaments(int amount = 0)
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments.Include("Game").Where(x => x.Exclusive == false).OrderBy(x => x.StartDOT).ToList();

                var tournamentsDtos = new List<TournamentDto>();

                foreach (Tournament tournament in tournaments)
                {
                    if (DateHelper.ParseTimeZone(DateTime.UtcNow) <= tournament.StartDOT)
                    {
                        var prizePool = db.TournamentPrizepools
                                   .Where(x => x.Tournament.ID == tournament.ID)
                                   .Select(x => new TournamentPrizepool
                                   {
                                       ID = x.ID,
                                       Prize = x.Prize,
                                       CoinPrize = x.CoinPrize,
                                       Position = x.Position
                                   })
                                   .ToList();

                        tournament.Prizepool = prizePool;

                        var TournamentAttendes = db.TournamentAttendes
                                      .Where(x => x.Tournament.ID == tournament.ID)
                                      .Select(x => new TournamentAttende
                                      {
                                          Score = x.Score,
                                          Spins = x.Spins,
                                          User = new User()
                                          {
                                              AvatarImage = x.User.AvatarImage,
                                              Level = x.User.Level,
                                              ID = x.User.ID,
                                              Username = x.User.Username
                                          }
                                      })
                                      .ToList();

                        tournament.Attendes = TournamentAttendes;

                        var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                        tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);

                        tournamentsDtos.Add(tournamentDto);
                    }
                }
                if (amount != 0)
                {
                    var limitedTournaments = tournamentsDtos.Take(amount).ToList();
                    return limitedTournaments;
                }
                else
                {
                    var limitedTournaments = tournamentsDtos.ToList();
                    return limitedTournaments;
                }
            }
        }

        public List<TournamentDto> GetAllTournaments()
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments.Include("Game").ToList();

                var tournamentsDtos = new List<TournamentDto>();

                foreach (Tournament tournament in tournaments)
                {
                    var prizePool = db.TournamentPrizepools
                               .Where(x => x.Tournament.ID == tournament.ID)
                               .Select(x => new TournamentPrizepool
                               {
                                   ID = x.ID,
                                   Prize = x.Prize,
                                   CoinPrize = x.CoinPrize,
                                   Position = x.Position
                               })
                               .ToList();

                    tournament.Prizepool = prizePool;

                    var TournamentAttendes = db.TournamentAttendes
                                  .Where(x => x.Tournament.ID == tournament.ID)
                                  .Select(x => new TournamentAttende
                                  {
                                      Score = x.Score,
                                      Spins = x.Spins,
                                      User = new User()
                                      {
                                          AvatarImage = x.User.AvatarImage,
                                          Level = x.User.Level,
                                          ID = x.User.ID,
                                          Username = x.User.Username
                                      }
                                  })
                                  .ToList();

                    tournament.Attendes = TournamentAttendes;

                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                    tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);
                    tournamentsDtos.Add(tournamentDto);
                }

                var limitedTournaments = tournamentsDtos.ToList();

                return limitedTournaments;
            }
        }

        public List<TournamentAttendeDto> GetAttenders(Guid tournamentId)
        {
            using (var db = new DatabaseContext())
            {
                var tournamentAttendes = db.TournamentAttendes.Include("User").Where(x => x.Tournament.ID == tournamentId && x.User.Banned == false).OrderByDescending(x => x.Score).Take(10).ToList();
                var tournament = db.Tournaments.Include("Game").FirstOrDefault(x => x.ID == tournamentId);
                if (tournamentAttendes.Count() > 0)
                {
                    var tournamentAttendesDtos = tournamentAttendes.Select(attende =>
                    {
                        var placement = GetAttenderPlacement(attende, tournament.ID);
                        var attendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(attende);
                        attende.User = new User()
                        {
                            ID = attende.User.ID,
                            AvatarImage = attende.User.AvatarImage,
                            Coins = attende.User.Coins,
                            Level = attende.User.Level,
                            Username = attende.User.Username
                        };
                        attendeDto.Placement = placement;
                        attendeDto.CurrentPrize = GetAttenderPrize(placement, tournament);
                        attendeDto.CoinPrize = GetAttenderCoinPrize(placement, tournament);
                        attende.Tournament = null;
                        return attendeDto;
                    }).ToList();

                    return tournamentAttendesDtos;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<TournamentAttendeDto> GetUsersAttenders(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var tournamentAttendes = db.TournamentAttendes.Include("User").Include("Tournament").Where(x => x.User.ID == userId).OrderByDescending(x => x.Tournament.StartDOT).ToList();
                if (tournamentAttendes.Count() > 0)
                {
                    var tournamentAttendesDtos = tournamentAttendes.Select(attende =>
                    {
                        var tournament = db.Tournaments.FirstOrDefault(x => x.ID == attende.Tournament.ID);
                        var placement = GetAttenderPlacement(attende, tournament.ID);
                        var attendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(attende);
                        attende.User = new User()
                        {
                            ID = attende.User.ID,
                            AvatarImage = attende.User.AvatarImage,
                            Coins = attende.User.Coins,
                            Level = attende.User.Level,
                            Username = attende.User.Username
                        };
                        attendeDto.Placement = placement;
                        attendeDto.CurrentPrize = GetAttenderPrize(placement, tournament);
                        attendeDto.CoinPrize = GetAttenderCoinPrize(placement, tournament);
                        attende.Tournament = null;
                        attendeDto.User = null;
                        attendeDto.Tournament.Attendes = null;
                        return attendeDto;
                    }).ToList().Where(x => DateHelper.ParseTimeZone(DateTime.UtcNow) > x.Tournament.EndDOT).ToList();

                    return tournamentAttendesDtos;
                }
                else
                {
                    return null;
                }
            }
        }

        public LeaderboardModel GetAttendersModel(Guid tournamentId)
        {
            using (var db = new DatabaseContext())
            {
                var tournamentAttendes = db.TournamentAttendes.Include("User").Where(x => x.Tournament.ID == tournamentId).OrderByDescending(x => x.Score).Take(30).ToList();
                var tournament = db.Tournaments.Include("Game").FirstOrDefault(x => x.ID == tournamentId);
                if (tournamentAttendes.Count() > 0)
                {
                    if (tournament.PaidOut == true)
                    {
                        var tournamentAttendesDtos = tournamentAttendes.Select(attende =>
                        {
                            var placement = GetAttenderPlacement(attende, tournament.ID);
                            var attendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(attende);
                            attendeDto.Placement = placement;
                            attendeDto.User = new UserDto()
                            {
                                ID = attende.User.ID,
                                AvatarImage = attende.User.AvatarImage,
                                Level = attende.User.Level,
                                Username = attende.User.Username
                            };
                            attendeDto.CurrentPrize = GetAttenderPrize(placement, tournament);
                            attendeDto.CoinPrize = GetAttenderCoinPrize(placement, tournament);
                            attendeDto.Tournament = null;
                            return attendeDto;
                        }).ToList();

                        tournament.Attendes = null;

                        var model = new LeaderboardModel()
                        {
                            Attendes = tournamentAttendesDtos.Where(x => x != null).ToList(),
                            Tournament = tournament
                        };

                        return model;
                    }
                    else
                    {
                        var tournamentAttendesDtos = tournamentAttendes.Select(attende =>
                        {
                            if (attende.User.Banned == false)
                            {
                                var placement = GetAttenderPlacement(attende, tournament.ID);
                                var attendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(attende);
                                attendeDto.Placement = placement;
                                attendeDto.User = new UserDto()
                                {
                                    ID = attende.User.ID,
                                    AvatarImage = attende.User.AvatarImage,
                                    Level = attende.User.Level,
                                    Username = attende.User.Username
                                };
                                attendeDto.CurrentPrize = GetAttenderPrize(placement, tournament);
                                attendeDto.CoinPrize = GetAttenderCoinPrize(placement, tournament);
                                attendeDto.Tournament = null;
                                return attendeDto;
                            }
                            else
                            {
                                return null;
                            }
                        }).ToList();

                        tournament.Attendes = null;

                        var model = new LeaderboardModel()
                        {
                            Attendes = tournamentAttendesDtos.Where(x => x != null).ToList(),
                            Tournament = tournament
                        };

                        return model;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public List<TournamentDto> GetLiveTournaments(string identifier)
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments.Include("Game").Where(x => x.Exclusive == false).ToList();

                var user = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                var tournamentsDtos = new List<TournamentDto>();

                foreach (Tournament tournament in tournaments)
                {
                    if (DateHelper.ParseTimeZone(DateTime.UtcNow) >= tournament.StartDOT && DateHelper.ParseTimeZone(DateTime.UtcNow) <= tournament.EndDOT)
                    {
                        var prizePool = db.TournamentPrizepools
                                 .Where(x => x.Tournament.ID == tournament.ID)
                                 .Select(x => new TournamentPrizepool
                                 {
                                     Prize = x.Prize,
                                     CoinPrize = x.CoinPrize,
                                     Position = x.Position
                                 })
                                 .ToList();

                        tournament.Prizepool = prizePool;

                        var TournamentAttendes = db.TournamentAttendes
                                      .Where(x => x.Tournament.ID == tournament.ID)
                                      .Select(x => new TournamentAttende
                                      {
                                          Score = x.Score,
                                          Spins = x.Spins,
                                          User = new User()
                                          {
                                              AvatarImage = x.User.AvatarImage,
                                              Level = x.User.Level,
                                              ID = x.User.ID,
                                              Username = x.User.Username
                                          }
                                      })
                                      .ToList();

                        tournament.Attendes = TournamentAttendes;

                        var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                        var userAttended = UserAttending(user.User.ID, tournament.ID);

                        if (userAttended != null)
                        {
                            tournamentDto.UserAttended = userAttended;
                        }
                        tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);

                        tournamentsDtos.Add(tournamentDto);
                    }
                }

                return tournamentsDtos;
            }
        }

        //TODO: Add live logic
        public List<TournamentDto> GetHistoryTournaments(string identifier)
        {
            using (var db = new DatabaseContext())
            {
                var currentTime = DateHelper.ParseTimeZone(DateTime.UtcNow);
                var tournaments = db.Tournaments.Include("Game").Where(x => x.EndDOT <= currentTime).OrderByDescending(x => x.EndDOT).Take(8).ToList();

                var user = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                var tournamentsDtos = new List<TournamentDto>();

                foreach (Tournament tournament in tournaments)
                {
                    var prizePool = db.TournamentPrizepools
                                .Where(x => x.Tournament.ID == tournament.ID)
                                .Select(x => new TournamentPrizepool
                                {
                                    Prize = x.Prize,
                                    CoinPrize = x.CoinPrize,
                                    Position = x.Position
                                })
                                .ToList();

                    tournament.Prizepool = prizePool;

                    var TournamentAttendes = db.TournamentAttendes
                                    .Where(x => x.Tournament.ID == tournament.ID)
                                    .Select(x => new TournamentAttende
                                    {
                                        Score = x.Score,
                                        Spins = x.Spins,
                                        User = new User()
                                        {
                                            AvatarImage = x.User.AvatarImage,
                                            Level = x.User.Level,
                                            ID = x.User.ID,
                                            Username = x.User.Username
                                        }
                                    })
                                    .ToList();

                    tournament.Attendes = TournamentAttendes;

                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                    var userAttended = UserAttending(user.User.ID, tournament.ID);

                    if (userAttended != null)
                    {
                        tournamentDto.UserAttended = userAttended;
                    }
                    tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);
                    tournamentsDtos.Add(tournamentDto);
                }

                var limitedTournaments = tournamentsDtos.ToList();

                return limitedTournaments;
            }
        }

        public List<TournamentDto> GetExclusiveTournaments(string identifier)
        {
            using (var db = new DatabaseContext())
            {
                var tournamentList = db.Tournaments.Include("Game").Include("Attendes").Where(x => x.Exclusive == true).ToList();
                var tournaments = new List<TournamentDto>();
                if (tournamentList.Count() > 0)
                {
                    foreach (var tournament in tournamentList)
                    {
                        var gameProvider = db.GameProviders.FirstOrDefault(x => x.Games.Contains(tournament.Game));
                        gameProvider.Games = null;

                        tournament.Game.GameProvider = gameProvider;

                        var user = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                        var prizePool = db.TournamentPrizepools
                                        .Where(x => x.Tournament.ID == tournament.ID)
                                        .Select(x => new TournamentPrizepool
                                        {
                                            Prize = x.Prize,
                                            CoinPrize = x.CoinPrize,
                                            Position = x.Position
                                        })
                                        .ToList();

                        var TournamentAttendes = db.TournamentAttendes
                                      .Where(x => x.Tournament.ID == tournament.ID)
                                      .Select(x => new TournamentAttende
                                      {
                                          Score = x.Score,
                                          Spins = x.Spins,
                                          User = new User()
                                          {
                                              AvatarImage = x.User.AvatarImage,
                                              Level = x.User.Level,
                                              ID = x.User.ID,
                                              Username = x.User.Username
                                          }
                                      })
                                      .OrderByDescending(x => x.Score)
                                      .ToList();

                        tournament.Attendes = TournamentAttendes;

                        tournament.Prizepool = prizePool;

                        var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                        var userAttended = UserAttending(user.User.ID, tournament.ID);

                        if (userAttended != null)
                        {
                            tournamentDto.UserAttended = userAttended;
                        }
                        tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);

                        tournaments.Add(tournamentDto);
                    }
                    return tournaments;
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentDto GetExclusiveTournament(Guid ID, string identifier)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.Include("Game").Include("Attendes").FirstOrDefault(x => x.Exclusive == true);

                if (tournament != null)
                {
                    var gameProvider = db.GameProviders.FirstOrDefault(x => x.Games.Contains(tournament.Game));
                    gameProvider.Games = null;

                    tournament.Game.GameProvider = gameProvider;

                    var user = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                    var prizePool = db.TournamentPrizepools
                                    .Where(x => x.Tournament.ID == tournament.ID)
                                    .Select(x => new TournamentPrizepool
                                    {
                                        Prize = x.Prize,
                                        CoinPrize = x.CoinPrize,
                                        Position = x.Position
                                    })
                                    .ToList();

                    var TournamentAttendes = db.TournamentAttendes
                                  .Where(x => x.Tournament.ID == tournament.ID)
                                  .Select(x => new TournamentAttende
                                  {
                                      Score = x.Score,
                                      Spins = x.Spins,
                                      User = new User()
                                      {
                                          AvatarImage = x.User.AvatarImage,
                                          Level = x.User.Level,
                                          ID = x.User.ID,
                                          Username = x.User.Username
                                      }
                                  })
                                  .OrderByDescending(x => x.Score)
                                  .ToList();

                    tournament.Attendes = TournamentAttendes;

                    tournament.Prizepool = prizePool;

                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                    var userAttended = UserAttending(user.User.ID, tournament.ID);

                    if (userAttended != null)
                    {
                        tournamentDto.UserAttended = userAttended;
                    }
                    tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);

                    return tournamentDto;
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentDto GetTournament(Guid ID, string identifier)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.Include("Game").Include("Attendes").FirstOrDefault(x => x.ID == ID);

                var gameProvider = db.GameProviders.FirstOrDefault(x => x.Games.Contains(tournament.Game));
                gameProvider.Games = null;

                tournament.Game.GameProvider = gameProvider;

                var user = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                var prizePool = db.TournamentPrizepools
                                .Where(x => x.Tournament.ID == tournament.ID)
                                .Select(x => new TournamentPrizepool
                                {
                                    Prize = x.Prize,
                                    CoinPrize = x.CoinPrize,
                                    Position = x.Position
                                })
                                .ToList();

                var TournamentAttendes = db.TournamentAttendes
                              .Where(x => x.Tournament.ID == tournament.ID)
                              .Select(x => new TournamentAttende
                              {
                                  Score = x.Score,
                                  Spins = x.Spins,
                                  User = new User()
                                  {
                                      AvatarImage = x.User.AvatarImage,
                                      Level = x.User.Level,
                                      ID = x.User.ID,
                                      Username = x.User.Username
                                  }
                              })
                              .OrderByDescending(x => x.Score)
                              .ToList();

                tournament.Attendes = TournamentAttendes;

                tournament.Prizepool = prizePool;

                var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);

                var userAttended = UserAttending(user.User.ID, tournament.ID);

                if (userAttended != null)
                {
                    tournamentDto.UserAttended = userAttended;
                }
                tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);

                return tournamentDto;
            }
        }

        public List<TournamentAttendeDto> GetTournamentWinners(int amount)
        {
            using (var db = new DatabaseContext())
            {
                var tournaments = db.Tournaments.Include("Game").Include("Attendes").OrderBy(x => x.EndDOT).Take(100).ToList();
                var winners = new List<TournamentAttendeDto>();
                foreach (var tournament in tournaments)
                {
                    var prizePool = db.TournamentPrizepools
                               .Where(x => x.Tournament.ID == tournament.ID)
                               .Select(x => new TournamentPrizepool
                               {
                                   Prize = x.Prize,
                                   CoinPrize = x.CoinPrize,
                                   Position = x.Position
                               })
                               .ToList();

                    var winner = db.TournamentAttendes
                            .Include("Tournament")
                            .Where(x => x.Tournament.ID == tournament.ID)
                             .Select(x => new TournamentAttende
                             {
                                 User = x.User,
                                 Score = x.Score,
                                 Tournament = x.Tournament
                             })
                            .OrderByDescending(x => x.Score)
                            .ToList();

                    if (winner.Count() > 0)
                    {
                        var tournamentAttendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(winner.First());
                        tournamentAttendeDto.Game = tournament.Game;
                        tournamentAttendeDto.Placement = 1;
                        tournamentAttendeDto.CurrentPrize = GetAttenderPrize(1, tournament);
                        tournamentAttendeDto.CoinPrize = GetAttenderCoinPrize(1, tournament);
                        tournamentAttendeDto.TournamentID = tournamentAttendeDto.Tournament.ID;
                        tournamentAttendeDto.Tournament = null;
                        winners.Add(tournamentAttendeDto);
                    }
                }
                return winners.Take(amount).ToList();
            }
        }

        public TournamentDto GetTournament(Guid ID)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.Include("Game").Include("Attendes").FirstOrDefault(x => x.ID == ID);

                if (tournament != null)
                {
                    tournament.Game = db.Games.Include("GameProvider").FirstOrDefault(x => x.ID == tournament.Game.ID);

                    var prizePool = db.TournamentPrizepools
                        .Where(x => x.Tournament.ID == tournament.ID)
                        .Select(x => new TournamentPrizepool
                        {
                            Prize = x.Prize,
                            CoinPrize = x.CoinPrize,
                            Position = x.Position
                        })
                        .ToList();

                    var TournamentAttendes = db.TournamentAttendes
                        .Where(x => x.Tournament.ID == tournament.ID)
                        .Select(x => new TournamentAttende
                        {
                            Score = x.Score,
                            Spins = x.Spins,
                            User = new User()
                            {
                                AvatarImage = x.User.AvatarImage,
                                Level = x.User.Level,
                                ID = x.User.ID,
                                Username = x.User.Username
                            }
                        })
                        .ToList();

                    tournament.Attendes = TournamentAttendes;

                    tournament.Prizepool = prizePool;

                    var tournamentDto = AutoMapper.Map<Tournament, TournamentDto>(tournament);
                    tournamentDto.Running = isTournamentRunning(tournamentDto.StartDOT, tournamentDto.EndDOT);
                    return tournamentDto;
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentAttende JoinTournament(Guid userID, Guid ID)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.Include("Attendes").FirstOrDefault(x => x.ID == ID);
                var currentTime = DateHelper.ParseTimeZone(DateTime.UtcNow);
                if (tournament != null && (currentTime >= tournament.StartDOT && currentTime <= tournament.EndDOT))
                {
                    var attendes = db.TournamentAttendes.Include("User").Where(x => x.Tournament.ID == ID).ToList();
                    var checkAttende = attendes.Where(x => x.User.ID == userID).ToList();

                    /*if (tournament.Exclusive == true)
                    {
                        var refferal = db.UserRefferals.FirstOrDefault(x => x.Refferal.Code == "CasinoMeister2020");
                        if (refferal == null)
                        {
                            return null;
                        }
                    }*/

                    if (checkAttende.Count() == 0)
                    {
                        var attende = new TournamentAttende()
                        {
                            Score = 0,
                            Spins = tournament.StartSpins,
                            Balance = tournament.StartSpins * 100,
                            User = Context.Users.FirstOrDefault(x => x.ID == userID)
                        };

                        tournament.Attendes.Add(attende);

                        db.SaveChanges();

                        return attende;
                    }
                    else if (tournament.Retryable == true)
                    {
                        var coins = CoinService.GetAmount(userID);

                        //Check if balance is enough
                        if (coins >= 5000)
                        {
                            var user = db.Users.FirstOrDefault(x => x.ID == userID);
                            var userDto = AutoMapper.Map<User, UserDto>(user);

                            //Remove 5000 coins from user, if retrying
                            var amount = CoinService.CreateCoin(new CoinDto()
                            {
                                Amount = -5000,
                                Created = DateHelper.ParseTimeZone(DateTime.UtcNow),
                                Description = "Retrying tournament " + tournament.Name,
                                User = userDto
                            });

                            //Reset the spins, balance & score
                            var attende = checkAttende.First();
                            attende.Spins = tournament.StartSpins;
                            attende.Balance = tournament.StartSpins * 100;
                            attende.Score = 0;
                            db.SaveChanges();

                            return attende;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentAttendeDto UserAttending(Guid userID, Guid ID)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.Include("Attendes").FirstOrDefault(x => x.ID == ID);

                if (tournament != null)
                {
                    tournament.Attendes = db.TournamentAttendes.Include("Tournament").Include("User").Where(x => x.Tournament.ID == ID).ToList();
                    var checkAttende = tournament.Attendes.Where(x => x.User != null && x.User.ID == userID).ToList();

                    if (checkAttende.Count() > 0)
                    {
                        var attende = checkAttende
                               .Select(x => new TournamentAttende
                               {
                                   Score = x.Score,
                                   Spins = x.Spins,
                                   ID = x.ID,
                               })
                            .First();

                        var attendeDto = AutoMapper.Map<TournamentAttende, TournamentAttendeDto>(attende);

                        attendeDto.UserID = userID;

                        attendeDto.User = new UserDto()
                        {
                            ID = checkAttende.First().User.ID,
                            AvatarImage = checkAttende.First().User.AvatarImage,
                            Coins = db.Coins.Where(x => x.User.ID == checkAttende.First().User.ID).Sum(x => x.Amount),
                            Level = checkAttende.First().User.Level,
                            Username = checkAttende.First().User.Username
                        };

                        attendeDto.Placement = GetAttenderPlacement(attende, tournament.ID);

                        attendeDto.AttendeID = checkAttende.First().ID;

                        return attendeDto;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public bool SpinTournament(Guid tournamentID, Guid userID, int score)
        {
            var userAttending = UserAttending(userID, tournamentID);

            if (userAttending != null && userAttending.Spins > 0)
            {
                var attender = Context.TournamentAttendes.Include("Tournament").Include("User").FirstOrDefault(x => x.ID == userAttending.ID);
                attender.Spins = attender.Spins - 1;
                attender.Score = attender.Score + score;
                attender.Tournament = Context.Tournaments.FirstOrDefault(x => x.ID == tournamentID);
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetAttenderPlacement(TournamentAttende attende, Guid tournamentId)
        {
            using (var db = new DatabaseContext())
            {
                attende.Tournament = db.Tournaments.FirstOrDefault(x => x.ID == tournamentId);
                if (attende.Tournament != null)
                {
                    var attendes = db.TournamentAttendes.Where(x => x.Tournament.ID == attende.Tournament.ID).OrderByDescending(x => x.Score).ToList();

                    return attendes.FindIndex(c => c.ID == attende.ID) + 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int GetAttenderPrize(int placement, Tournament tournament)
        {
            using (var db = new DatabaseContext())
            {
                var prizepool = db.TournamentPrizepools.Where(x => x.Tournament.ID == tournament.ID).ToList();
                var prizeResult = 0;
                foreach (var prize in prizepool)
                {
                    var positions = prize.Position.Split("-").Select(x => Int32.Parse(x)).ToList();
                    if (positions.Count() > 1)
                    {
                        if (positions[0] >= placement && positions[1] <= placement)
                        {
                            prizeResult = prize.Prize;
                        }
                    }
                    else
                    {
                        if (positions[0] == placement)
                        {
                            prizeResult = prize.Prize;
                        }
                    }
                }
                return prizeResult;
            }
        }

        public bool GetAttenderCoinPrize(int placement, Tournament tournament)
        {
            using (var db = new DatabaseContext())
            {
                var prizepool = db.TournamentPrizepools.Where(x => x.Tournament.ID == tournament.ID).ToList();
                bool coinPrize = false;
                foreach (var prize in prizepool)
                {
                    var positions = prize.Position.Split("-").Select(x => Int32.Parse(x)).ToList();
                    if (positions.Count() > 1)
                    {
                        if (positions[0] >= placement && positions[1] <= placement)
                        {
                            coinPrize = prize.CoinPrize;
                        }
                    }
                    else
                    {
                        if (positions[0] == placement)
                        {
                            coinPrize = prize.CoinPrize;
                        }
                    }
                }
                return coinPrize;
            }
        }

        //Filters For the Dashboard
        public TournamentAttendeDto GetAttender(Guid attende)
        {
            using (var db = new DatabaseContext())
            {
                var userAttending = db.TournamentAttendes.Include("User").Include("Tournament").FirstOrDefault(x => x.ID == attende);
                if (userAttending != null)
                {
                    var tournament = userAttending.Tournament;
                    var placement = GetAttenderPlacement(userAttending, userAttending.Tournament.ID);
                    var tournamentEntity = new TournamentAttendeDto()
                    {
                        Spins = userAttending.Spins,
                        Score = userAttending.Score,
                        Balance = userAttending.Balance,
                        ID = userAttending.ID,
                        User = new UserDto()
                        {
                            AvatarImage = userAttending.User.AvatarImage,
                            Username = userAttending.User.Username,
                            ID = userAttending.User.ID,
                            Level = userAttending.User.Level
                        },
                        Tournament = AutoMapper.Map<Tournament, TournamentDto>(tournament),
                        Placement = placement,
                        CurrentPrize = GetAttenderPrize(placement, tournament),
                        CoinPrize = GetAttenderCoinPrize(placement, tournament)
                    };
                    tournamentEntity.Tournament.Attendes = null;

                    return tournamentEntity;
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentAttendeDto DeductSpins(TournamentAttendeDto tournamentAttende, Int64 amount)
        {
            using (var db = new DatabaseContext())
            {
                var attende = db.TournamentAttendes.Include("Tournament").FirstOrDefault(x => x.ID == tournamentAttende.ID);

                if (attende != null && attende.Spins > 0)
                {
                    attende.Spins = attende.Spins - 1;
                    db.SaveChanges();

                    return new TournamentAttendeDto()
                    {
                        Spins = attende.Spins,
                        Score = attende.Score,
                        Balance = attende.Balance
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentAttendeDto UpdateScore(TournamentAttendeDto tournamentAttende, Int64 amount)
        {
            using (var db = new DatabaseContext())
            {
                var attende = db.TournamentAttendes.Include("Tournament").FirstOrDefault(x => x.ID == tournamentAttende.ID);

                if (attende != null)
                {
                    if (attende.Tournament.Format == TournamentFormat.Score)
                    {
                        attende.Score = attende.Score + amount;
                    }

                    if (attende.Tournament.Format == TournamentFormat.Multiplier)
                    {
                        if ((amount / 100) > attende.Score)
                        {
                            attende.Score = amount / 100;
                        }
                    }

                    attende.Balance = attende.Spins * 100;

                    db.SaveChanges();

                    return new TournamentAttendeDto()
                    {
                        Spins = attende.Spins,
                        Score = attende.Score,
                        Balance = attende.Balance
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public TournamentAttendeDto GetAttenderByToken(string token)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.WalletTokens.Include("User").Include("Tournament").FirstOrDefault(x => x.Token == token);

                if (user != null)
                {
                    var userAttending = db.TournamentAttendes.Include("Tournament").FirstOrDefault(x => x.User.ID == user.User.ID && x.Tournament.ID == user.Tournament.ID);

                    if (userAttending != null)
                    {
                        return new TournamentAttendeDto()
                        {
                            Spins = userAttending.Spins,
                            Score = userAttending.Score,
                            ID = userAttending.ID,
                            Tournament = AutoMapper.Map<Tournament, TournamentDto>(userAttending.Tournament),
                            Balance = userAttending.Balance
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        ~TournamentService()

        {
            Context.Dispose();
        }
    }
}