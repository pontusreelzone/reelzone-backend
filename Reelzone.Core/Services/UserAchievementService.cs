﻿using AutoMapper;
using Hangfire;
using Microsoft.Azure.Amqp.Framing;
using Microsoft.EntityFrameworkCore;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Reelzone.Core.Services
{
    public class UserAchievementService : IUserAchievmentService
    {
        public IBusQueService BusQueService;
        public DatabaseContext Context;
        public IUserExperienceService UserExperienceService;
        public IMapper AutoMapper;

        public UserAchievementService(IBusQueService busQueService, DatabaseContext dbContext, IUserExperienceService userExperienceService, IAutoMapper autoMapper)
        {
            BusQueService = busQueService;
            Context = dbContext;
            UserExperienceService = userExperienceService;
            AutoMapper = autoMapper.Get();
        }

        public void DeleteUserAchievments(Achievement achievement)
        {
            using (var db = new DatabaseContext())
            {
                var deleteUserAchievements = Context.UserAchievements.Include("Achievement").Where(x => x.Completed == false && x.Achievement == achievement).ToList();

                foreach (UserAchievement userAchievement in deleteUserAchievements)
                {
                    Context.UserAchievements.Remove(userAchievement);
                }

                Context.SaveChanges();
            }
        }

        public void GenerateUserAchievements(Guid achievementID) //bool?
        {
            using (var db = new DatabaseContext())
            {
                var achievement = db.Achievements.FirstOrDefault(x => x.ID == achievementID);
                var obligatedUsers = db.Users.Where(x => x.Level >= achievement.LevelThreshold).ToList();

                foreach (User user in obligatedUsers)
                {
                    var userAchievments = db.UserAchievements.FirstOrDefault(x => x.Achievement.ID == achievement.ID && x.Completed == true && x.User.ID == user.ID);

                    if (userAchievments == null)
                    {
                        db.UserAchievements.Add(new UserAchievement()
                        {
                            Achievement = achievement,
                            AwardedTimeStamp = DateTime.Now,
                            Completed = false,
                            User = user,
                            EndingTimestamp = DateTime.Now.Add(GetPeriodInDays(achievement.Period))
                        });
                    }
                }

                db.SaveChanges();
            }
        }

        public void GenerateUserAchievements(Guid achievementID, Guid userId) //bool?
        {
            using (var db = new DatabaseContext())
            {
                var achievement = db.Achievements.FirstOrDefault(x => x.ID == achievementID);
                var user = db.Users.FirstOrDefault(x => x.ID == userId);

                if (user == null)
                {
                    var userAchievments = db.UserAchievements.FirstOrDefault(x => x.Achievement.ID == achievement.ID && x.Completed == true && x.User.ID == userId);

                    if (userAchievments == null)
                    {
                        db.UserAchievements.Add(new UserAchievement()
                        {
                            Achievement = achievement,
                            AwardedTimeStamp = DateTime.Now,
                            Completed = false,
                            User = user,
                            EndingTimestamp = DateTime.Now.Add(GetPeriodInDays(achievement.Period))
                        });
                    }

                    db.SaveChanges();
                }
            }
        }

        public List<UserAchievementDto> GetUserAchievements(Guid userID, AchievementPeriod period, bool completed = false)
        {
            using (var db = new DatabaseContext())
            {
                var achievements = db.UserAchievements.Include("Achievement")
               .Where(x => x.User.ID == userID && x.Completed == completed && x.Achievement.Period == period)
               .Select(x => new UserAchievement
               {
                   Achievement = x.Achievement,
                   Completed = x.Completed,
                   AwardedTimeStamp = x.AwardedTimeStamp,
                   CompletedTimeStamp = x.CompletedTimeStamp,
                   EndingTimestamp = x.EndingTimestamp,
                   ID = x.ID
               })
               .ToList();

                var achievementsDtos = AutoMapper.Map<List<UserAchievement>, List<UserAchievementDto>>(achievements);

                return achievementsDtos;
            }
        }

        public List<UserAchievementDto> GetUserAchievements(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var achievements = db.UserAchievements.Include("Achievement")
               .Where(x => x.User.ID == userID && x.Completed == true)
               .Select(x => new UserAchievement
               {
                   Achievement = x.Achievement,
                   Completed = x.Completed,
                   AwardedTimeStamp = x.AwardedTimeStamp,
                   CompletedTimeStamp = x.CompletedTimeStamp,
                   EndingTimestamp = x.EndingTimestamp,
                   ID = x.ID
               })
               .ToList();

                var achievementsDtos = AutoMapper.Map<List<UserAchievement>, List<UserAchievementDto>>(achievements);

                return achievementsDtos;
            }
        }

        public List<UserAchievementDto> GetUserAchievements(Guid userID, AchievementPeriod period)
        {
            using (var db = new DatabaseContext())
            {
                var achievements = db.UserAchievements.Include("Achievement")
               .Where(x => x.User.ID == userID && x.Achievement.Period == period)
               .Select(x => new UserAchievement
               {
                   Achievement = x.Achievement,
                   Completed = x.Completed,
                   AwardedTimeStamp = x.AwardedTimeStamp,
                   CompletedTimeStamp = x.CompletedTimeStamp,
                   EndingTimestamp = x.EndingTimestamp,
                   ID = x.ID
               })
               .ToList();

                var achievementsDtos = AutoMapper.Map<List<UserAchievement>, List<UserAchievementDto>>(achievements);

                return achievementsDtos;
            }
        }

        public void GenerateUserAnchievementsJobs(AchievementPeriod period)
        {
            using (var db = new DatabaseContext())
            {
                var achievements = db.Achievements.Where(x => x.Enabled && x.Period == period).ToList();

                foreach (Achievement achievement in achievements)
                {
                    DeleteUserAchievments(achievement);

                    BackgroundJob.Enqueue(methodCall: () => GenerateUserAchievements(achievement.ID));
                }
            }
        }

        public void GenerateUserAnchievementsJobs(AchievementPeriod period, Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var achievements = db.Achievements.Where(x => x.Enabled && x.Period == period).ToList();

                foreach (Achievement achievement in achievements)
                {
                    //DeleteUserAchievments(achievement);

                    BackgroundJob.Enqueue(methodCall: () => GenerateUserAchievements(achievement.ID, userId));
                }
            }
        }

        public void GenerateUserPeriodJobs()
        {
            RecurringJob.AddOrUpdate(() => GenerateUserAnchievementsJobs(AchievementPeriod.Day), "0 0 * * *");
            RecurringJob.AddOrUpdate(() => GenerateUserAnchievementsJobs(AchievementPeriod.Month), "0 0 1 * *");
            RecurringJob.AddOrUpdate(() => GenerateUserAnchievementsJobs(AchievementPeriod.Week), "0 0 * * 1");
            RecurringJob.AddOrUpdate(() => CheckAllUserAnchievements(), "*/5 * * * *");
        }

        public TimeSpan GetPeriodInDays(AchievementPeriod period)
        {
            var timespan = new TimeSpan();
            switch (period.ToString())
            {
                case "Day":
                    timespan += TimeSpan.FromDays(1);
                    break;

                case "Week":
                    timespan += TimeSpan.FromDays(7);
                    break;

                case "Month":
                    timespan += TimeSpan.FromDays(DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
                    break;

                case "Year":
                    timespan += TimeSpan.FromDays(365 * 100);
                    break;
            }

            return timespan;
        }

        public bool CheckAllUserAnchievements()
        {
            using (var db = new DatabaseContext())
            {
                var userAchievements = db.UserAchievements
               .Include("Achievement")
               .Include("User")
               .Where(x => x.Completed == false)
               .ToList();

                foreach (UserAchievement userAchievement in userAchievements)
                {
                    var achievement = db.Achievements.Include("Activity").FirstOrDefault(x => x.ID == userAchievement.Achievement.ID);

                    //userAchievement.Achievement;
                    var achievementActivities = db.UserActivities
                        .Where(x => x.Activity.ID == achievement.Activity.ID && x.Credited == false && DateTime.Now < userAchievement.EndingTimestamp).ToList();

                    if (achievement.Amount >= achievementActivities.Count())
                    {
                        userAchievement.Completed = true;
                        userAchievement.CompletedTimeStamp = DateTime.Now;
                        UserExperienceService.CreateUserExperience(userAchievement);
                    }

                    achievementActivities.ForEach(x => { x.Credited = true; });
                    db.SaveChanges();
                }

                return true;
            }
        }

        public bool CheckUserAnchievements(Activity activity, Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var userAchievements = db.UserAchievements
               .Include("Achievement")
               .Include("User")
               .Where(x => x.User.ID == userID && x.Achievement.Activity.ID == activity.ID && x.Completed == false)
               .ToList();

                foreach (UserAchievement userAchievement in userAchievements)
                {
                    var achievement = db.Achievements.Include("Activity").FirstOrDefault(x => x.ID == userAchievement.Achievement.ID);

                    //userAchievement.Achievement;
                    var achievementActivities = db.UserActivities
                        .Where(x => x.Activity.ID == achievement.Activity.ID && x.Credited == false && DateTime.Now < userAchievement.EndingTimestamp).ToList();

                    if (achievement.Amount >= achievementActivities.Count())
                    {
                        userAchievement.Completed = true;
                        userAchievement.CompletedTimeStamp = DateTime.Now;
                        var NotifyUser = BusQueService.NotifyChallengeCompleted(userAchievement, userID);
                        UserExperienceService.CreateUserExperience(userAchievement);
                    }

                    achievementActivities.ForEach(x => { x.Credited = true; });
                }
                db.SaveChanges();

                return true;
            }
        }

        ~UserAchievementService()
        {
            Context.Dispose();
        }
    }
}