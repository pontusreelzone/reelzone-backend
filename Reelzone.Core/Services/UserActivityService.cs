﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;

namespace Reelzone.Core.Services
{
    public class UserActivityService : IUserActivityService
    {
        public IUserExperienceService ExperienceService;
        public IUserAchievmentService UserAchievementService;
        public DatabaseContext Context;

        public UserActivityService(IUserExperienceService experienceService, IUserAchievmentService userAchievementService, DatabaseContext dbContext)
        {
            ExperienceService = experienceService;
            UserAchievementService = userAchievementService;
            Context = dbContext;
        }

        public UserActivity CreateUserActivity(Guid userID, string activityHandle)
        {
            using (var db = new DatabaseContext())
            {
                //Check for user and activity
                var user = db.Users.FirstOrDefault(x => x.ID == userID);
                var activity = db.Activities.FirstOrDefault(x => x.Handle == activityHandle);
                //Not found either, return erroa
                if (user != null && activity != null)
                {
                    //Check user activity
                    var userActivity = db.UserActivities.FirstOrDefault(x => x.Activity.ID == activity.ID && x.User.ID == user.ID);

                    if (userActivity == null)
                    {
                        //First time user did activity
                        var userActivityEntity = db.UserActivities
                              .Add(new UserActivity()
                              {
                                  Activity = activity,
                                  TimeStamp = DateTime.Now,
                                  User = user,
                                  Credited = false
                              })
                              .Entity;

                        db.SaveChanges();

                        return userActivityEntity;
                    }
                    else
                    {
                        //User has done the same activity
                        //Check if the action is within time threshold to prevent spam
                        if (ActivityWithinThreshold(userActivity, activity) == false)
                        {
                            var userActivityEntity = db.UserActivities
                                .Add(new UserActivity()
                                {
                                    Activity = activity,
                                    TimeStamp = DateTime.Now,
                                    User = user
                                })
                                .Entity;

                            db.SaveChanges();

                            return userActivityEntity;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public bool ActivityWithinThreshold(UserActivity userActivity, Activity activity)
        {
            var timeDiff = userActivity.TimeStamp - DateTime.Now;

            if (activity.TimeThreshold > timeDiff || activity.TimeThreshold.TotalSeconds == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        ~UserActivityService()
        {
            Context.Dispose();
        }
    }
}