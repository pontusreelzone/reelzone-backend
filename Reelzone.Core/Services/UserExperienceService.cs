﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;

namespace Reelzone.Core.Services
{
    public class UserExperienceService : IUserExperienceService
    {
        public IBusQueService BusQueService;
        public DatabaseContext Context;
        public IUserActivityService UserActivityService;
        public IPushService PushService;

        public UserExperienceService(DatabaseContext dbContext, IBusQueService busQueService, IPushService pushService)
        {
            Context = dbContext;
            BusQueService = busQueService;
            PushService = pushService;
        }

        public UserExperience CreateUserExperience(UserAchievement userAchievement)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userAchievement.User.ID);

                var userExpericenceEntity = db.UserExperiences
                    .Add(new UserExperience()
                    {
                        Value = userAchievement.Achievement.RewardAmount,
                        User = user
                    })
                    .Entity;

                db.SaveChanges();

                var userLevelUp = UserLevelUp(userAchievement.User.ID);

                if (userLevelUp)
                {
                    var updateUserLevel = UpdateUserLevel(userAchievement.User.ID);

                    if (updateUserLevel)
                    {
                        PushService.Send(userAchievement.User.ID, "event", "You just leveled up! Congrats " + userAchievement.User.Username, "Wohoo!", "success");
                    }
                }
                return null;
            }
        }

        public UserExperience CreateUserExperience(int amount, Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userId);

                var userExpericenceEntity = db.UserExperiences
                    .Add(new UserExperience()
                    {
                        Value = amount,
                        User = user
                    })
                    .Entity;

                db.SaveChanges();

                var userLevelUp = UserLevelUp(userId);

                if (userLevelUp)
                {
                    var updateUserLevel = UpdateUserLevel(userId);
                }
                return null;
            }
        }

        public bool UserLevelUp(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                //Check user
                var user = db.Users.FirstOrDefault(x => x.ID == userID);
                if (user != null)
                {
                    //Get current total experience
                    var currentUserExperience = GetCurrentUserExperience(user.ID);
                    //get the current user level
                    var currentLevel = GetUserCurrentLevel(user.ID);
                    //calculate level from totalexperience
                    var calculateLevel = CalculateUserLevel(user.ID);

                    if (currentLevel < calculateLevel)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool UpdateUserLevel(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                if (user != null)
                {
                    var newUserLevel = CalculateUserLevel(user.ID);

                    user.Level = newUserLevel;

                    //UserActivityService.CreateUserActivity(user.ID, "UserLeveledUp");

                    db.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int CalculateUserLevel(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                int threshold = 50;

                if (user != null)
                {
                    var currentUserExperience = GetCurrentUserExperience(user.ID);

                    int CalculatetedLevel = (int)(0.5 + Math.Sqrt(1 + 8 * (currentUserExperience) / (threshold)) / 2);

                    return CalculatetedLevel;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int CalculateUserExperienceLeft(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                int calculateExperienceNeeded;

                if (user != null)
                {
                    //get the exp needed to level up
                    var expforlevel = GetExperienceForLevel(user.Level);
                    //get the users current total exp
                    var currentexp = GetCurrentUserExperience(user.ID);
                    //if the current exp is greater then what is needed for the current level then level the user
                    if (currentexp > expforlevel)
                    {
                        //see if the user levels up
                        var userLevelUp = UserLevelUp(user.ID);
                        //if true then update the user level
                        if (userLevelUp)
                        {
                            //update level
                            var updateUserLevel = UpdateUserLevel(user.ID);
                            //if all went well send to busque
                            if (updateUserLevel)
                            {
                                BusQueService.SendUserLevelUp(user.ID);
                                //get the new level
                                var newlevel = GetUserCurrentLevel(user.ID);

                                //get the exp needed to level up for the NEW level
                                expforlevel = GetExperienceForLevel(newlevel);
                            }
                            else
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            return 0;
                        }
                    }

                    //calculate the needed exp
                    calculateExperienceNeeded = expforlevel - currentexp;

                    return calculateExperienceNeeded;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int GetUserCurrentLevel(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                if (user != null)
                {
                    int currentLevel = user.Level;

                    return currentLevel;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int GetCurrentUserExperience(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                if (user != null)
                {
                    int currentUserExperience = db.UserExperiences.Where(x => x.User.ID == user.ID).Sum(x => x.Value);

                    return currentUserExperience;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int GetExperienceForLevel(int level)
        {
            int threshold = 50;

            int NextLevel = level + 1;

            int experienceForLevel = (int)(((Math.Pow(NextLevel, 2) - NextLevel) * threshold) / 2);

            return experienceForLevel;
        }

        public void GiveUserExperience(Guid userID, int amount)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userID);

                db.UserExperiences.Add(new UserExperience()
                {
                    User = user,
                    Value = amount
                });

                var userLevelUp = UserLevelUp(userID);

                if (userLevelUp)
                {
                    var updateUserLevel = UpdateUserLevel(userID);

                    if (updateUserLevel)
                    {
                        BusQueService.SendUserLevelUp(userID);
                    }
                }

                db.SaveChanges();
            }
        }

        ~UserExperienceService()
        {
            Context.Dispose();
        }
    }
}