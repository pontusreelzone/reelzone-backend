﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reelzone.Core.Services
{
    public class UserNotificationService : IUserNotificationService
    {
        public DatabaseContext Context;
        public IMapper AutoMapper;
        public IUserService UserService;

        public UserNotificationService(DatabaseContext dbContext, IAutoMapper autoMapper, IUserService userService)
        {
            Context = dbContext;
            AutoMapper = autoMapper.Get();
            UserService = userService;
        }

        public UserNotificationDto AddUserNotification(UserNotificationDto userNotificationDto)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userNotificationDto.User);
                if (user == null)
                {
                    throw new Exception("User not found");
                }

                var userNotificationEntity = AutoMapper.Map<UserNotificationDto, UserNotification>(userNotificationDto);
                userNotificationEntity.User = user;

                var createduserNotificationEntity = db.UserNotifications.Add(userNotificationEntity);
                db.SaveChanges();
                userNotificationDto = AutoMapper.Map<UserNotification, UserNotificationDto>(createduserNotificationEntity.Entity);
                return userNotificationDto;
            }
        }
        public UserNotificationDto UpdatUserNotification(UserNotificationDto userNotificationDto)

        {
            using (var db = new DatabaseContext())
            {
                var userNotification = db.UserNotifications.FirstOrDefault(x => x.ID == userNotificationDto.ID);
                if (userNotification == null)
                {
                    throw new Exception("Notification not found");
                }
                var user = db.Users.FirstOrDefault(x => x.ID == userNotificationDto.User);
                if (user == null)
                {
                    throw new Exception("User not found");
                }

                userNotification = AutoMapper.Map<UserNotificationDto, UserNotification>(userNotificationDto);
                var createduserNotificationEntity = db.UserNotifications.Update(userNotification);
                db.SaveChanges();
                userNotificationDto = AutoMapper.Map<UserNotification, UserNotificationDto>(createduserNotificationEntity.Entity);
                return userNotificationDto;

            }
        }



        public bool DeleteUserNotification(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var userNotification = db.UserNotifications.FirstOrDefault(x => x.ID == id);
                if (userNotification == null)
                {
                    throw new Exception("Notification not found");
                }
                db.UserNotifications.Remove(userNotification);
                db.SaveChanges();
                return true;
            }
        }

        public List<UserNotificationDto> GetUserNotifications(Guid id, bool viewed)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == id);
                if (user == null)
                {
                    throw new Exception("User not found");
                }
                var notifications = new List<UserNotification>();
                var notificationsDto = new List<UserNotificationDto>();
                if (viewed)
                {
                    notifications = db.UserNotifications
                             .Where(x => x.User == user && x.Viewed == true)
                             .OrderByDescending(x => x.CreatedDate)
                             .ToList();
                    foreach (var notification in notifications)
                    {
                        notificationsDto.Add(AutoMapper.Map<UserNotification, UserNotificationDto>(notification));
                    }
                    return notificationsDto;
                }
                else
                {
                    notifications = db.UserNotifications
                             .Where(x => x.User == user && x.Viewed == false)
                             .OrderByDescending(x => x.CreatedDate)
                             .ToList();
                    foreach (var notification in notifications)
                    {
                        notificationsDto.Add(AutoMapper.Map<UserNotification, UserNotificationDto>(notification));
                    }
                    return notificationsDto;
                }

            }
        }
        public List<UserNotificationDto> GetUserNotifications(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == id);
                if (user == null)
                {
                    throw new Exception("User not found");
                }
              
                var notificationsDto = new List<UserNotificationDto>();

                var notifications = db.UserNotifications
                             .Where(x => x.User == user)
                             .OrderByDescending(x => x.CreatedDate)
                             .ToList();
                    foreach (var notification in notifications)
                    {
                        notificationsDto.Add(AutoMapper.Map<UserNotification, UserNotificationDto>(notification));
                    }
                    return notificationsDto;
           

            }
        }

        ~UserNotificationService()
        {
            Context.Dispose();
        }
    }
}
