﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Reelzone.Core.Services
{
    public class UserRefferalService : IUserRefferalService
    {
        public DatabaseContext Context;

        public IMapper AutoMapper;
        public ICoinService CoinService;
        public IUserService UserService;
        public DateHelper DateHelper;

        public UserRefferalService(DatabaseContext dbContext, ICoinService coinService, IUserService userService)
        {
            Context = dbContext;
            CoinService = coinService;
            UserService = userService;
            DateHelper = new DateHelper();
        }

        public List<UserRefferal> GetUserRefferalsByRefferal(Guid ID)
        {
            using (var db = new DatabaseContext())
            {
                return db.UserRefferals.Include(x => x.User).Where(x => x.Refferal.ID == ID).ToList();
            }
        }

        public List<UserReferralStatsModel> GetReferralLeaderboard()
        {
            using (var db = new DatabaseContext())
            {
                var result = new List<UserReferralStatsModel>();

                var signups = db.UserRefferals.Include("User")//.Where(x => x.UsedTimestamp >= new DateTime(2020, 09, 13))
                    .GroupBy(x => x.Refferal.ID)
                    .Select(group => new ReferralLeaderboardModel
                    {
                        RefferalId = group.Key,
                        Count = group.Count()
                    })
                   .OrderByDescending(x => x.Count)
                   .Take(10)
                   .ToList();

                foreach (var signup in signups)
                {
                    try
                    {
                        var refferal = db.Refferals.FirstOrDefault(x => x.ID == signup.RefferalId);
                        var user = db.Users.FirstOrDefault(x => x.ID == new Guid(refferal.Code.ToString()));
                        if (user != null)
                        {
                            result.Add(new UserReferralStatsModel()
                            {
                                Earned = 0,
                                Signups = signup.Count,
                                Username = user.Username
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return result;
            }
        }

        public bool RedeemRefferal(string code, Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userId);
                var codeEntity = db.Refferals.FirstOrDefault(x => x.Code == code);
                if (codeEntity != null)
                {
                    var alreadyUsed = db.UserRefferals.FirstOrDefault(x => x.User.ID == user.ID);
                    if (alreadyUsed == null && codeEntity.Code != user.ID.ToString())
                    {
                        var userRefferd = db.UserRefferals.Add(new UserRefferal()
                        {
                            Refferal = codeEntity,
                            UsedTimestamp = DateHelper.ParseTimeZone(DateTime.UtcNow),
                            User = user
                        });

                        db.SaveChanges();

                        var coin = CoinService.CreateCoin(new CoinDto()
                        {
                            Amount = codeEntity.Amount,
                            User = UserService.GetUser(user.ID),
                            Description = "Redeemed coins with " + codeEntity.Code,
                            Created = DateHelper.ParseTimeZone(DateTime.UtcNow)
                        });

                        var userCode = db.Users.FirstOrDefault(x => x.ID.ToString() == codeEntity.Code);
                        if (userCode != null)
                        {
                            var userCoin = CoinService.CreateCoin(new CoinDto()
                            {
                                Amount = 5000,
                                User = UserService.GetUser(new Guid(codeEntity.Code)),
                                Description = "Earned from reffering " + codeEntity.Code,
                                Created = DateHelper.ParseTimeZone(DateTime.UtcNow)
                            });
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        ~UserRefferalService()
        {
            Context.Dispose();
        }
    }
}