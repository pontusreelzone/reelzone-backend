using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Reelzone.Core.Helpers;
using Reelzone.Core.Enums;
using Microsoft.Azure.Amqp.Framing;
using Reelzone.Core.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;

namespace Reelzone.Core.Services
{
    public class UserService : IUserService
    {
        public IUserExperienceService ExperienceService;
        public DatabaseContext Context;
        public DateHelper DateHelper;
        public ICoinService CoinService;
        public IUserAchievmentService UserAchievmentService;
        private readonly RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();

        public IMapper AutoMapper;

        public UserService(IUserExperienceService userExperienceService, IAutoMapper autoMapper, ICoinService coinService, IUserAchievmentService userAchievmentService)
        {
            Context = new DatabaseContext();
            ExperienceService = userExperienceService;
            AutoMapper = autoMapper.Get();
            DateHelper = new DateHelper();
            CoinService = coinService;
            UserAchievmentService = userAchievmentService;
        }

        public List<string> GetCheaters()
        {
            using (var db = new DatabaseContext())
            {
                var cheatedTransactions = new List<string>();
                var users = db.Users.ToList();
                foreach (var user in users)
                {
                    var transactions = db.GameTransactions.Include(x => x.Tournament).Where(x => x.User.ID == user.ID && (x.Tournament != null || x.Tournament == null) && x.Created > new DateTime(2020, 09, 01)).ToList();
                    if (transactions.Count() > 0)
                    {
                        for (int i = 0; i < transactions.Count; i++)
                        {
                            GameTransaction transaction = transactions[i];
                            if (i > 0)
                            {
                                int index = i - 1;
                                GameTransaction latestTransaction = transactions[index];
                                if (latestTransaction.Description == "Bet on tournament" && transaction.Description == "Win on tournament")
                                {
                                    if (latestTransaction.Tournament == null && transaction.Tournament != null)
                                    {
                                        if (transaction.Amount > 100)
                                        {
                                            cheatedTransactions.Add("User " + transaction.User.Username + " with email " + transaction.User.Email + " cheated on " + transaction.Tournament.Name + "the date of " + transaction.Tournament.StartDOT + " by winning �" + ((transaction.Amount / 100)).ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return cheatedTransactions;
            }
        }

        public List<string> GiveCoins()
        {
            using (var db = new DatabaseContext())
            {
                var coinTransaction = new List<string>();
                var users = db.Users.ToList();
                foreach (var user in users)
                {
                    var amount = user.Level * 300;
                    amount = 10000 + amount;
                    var userEntity = GetUser(user.ID);

                    var coin = CoinService.CreateCoin(new CoinDto()
                    {
                        Amount = amount,
                        Created = DateHelper.ParseTimeZone(DateTime.UtcNow),
                        Description = "Monthly Coins",
                        User = userEntity
                    });

                    coinTransaction.Add("Adding coins for user " + user.Username + " amount " + coin);
                }
                return coinTransaction;
            }
        }

        public List<UserDto> SearchUsers(int pageSize, int currentPage, FilterUserDto filters)
        {
            if (filters.Filters.Count == 0)
            {
                return GetLimitedUsers(pageSize, currentPage);
            }
            List<UserDto> usersDto = new List<UserDto>();
            List<User> users = new List<User>();

            using (var db = new DatabaseContext())
            {
                foreach (var filter in filters.Filters)
                {
                    if (filter.Name == "username")
                    {
                        users.AddRange(db.Users.Include("Country").Where(x => x.Username.Contains(filter.Filter)).ToList());
                    }
                    if (filter.Name == "admin")
                    {
                        int admin;
                        if (filter.Filter == "true")
                        {
                            admin = 2;
                        }
                        else
                        {
                            admin = 0;
                        }
                        users = db.Users.Include("Country").Where(x => x.Role == (Role)admin).ToList();
                    }
                    if (filter.Name == "country")
                    {
                        users = db.Users.Include("Country").Where(x => x.Country.Name.Contains(filter.Filter)).ToList();
                    }
                    if (filter.Name == "dob")
                    {
                        var date = new DateTime();
                        try
                        {
                            date = DateTime.Parse(filter.Filter);
                        }
                        catch (Exception)
                        {
                            return null;
                        }
                        users = db.Users.Include("Country").Where(x => x.BirthDate == date).ToList();
                    }
                    if (filter.Name == "status")
                    {
                        try
                        {
                            Status status = (Status)int.Parse(filter.Filter);
                            users = db.Users.Include("Country").Where(x => x.Status == status).ToList();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    if (filter.Name == "email")
                    {
                        users = db.Users.Include("Country").Where(x => x.Email.Contains(filter.Filter)).ToList();
                    }
                    if (filter.Name == "level")
                    {
                        int level;
                        try
                        {
                            level = int.Parse(filter.Filter);
                        }
                        catch (Exception)
                        {
                            return null;
                        }
                        users = db.Users.Include("Country").Where(x => x.Level == level).ToList();
                    }
                    //if (filter.Name == "coins")
                    //{
                    //    long coinsAmount;
                    //    List<Coin> coins = new List<Coin>();
                    //    try
                    //    {
                    //        coinsAmount = long.Parse(filter.Filter);
                    //        coins = Context.Coins.Where(x => x.Amount == coinsAmount).ToList();
                    //    }
                    //    catch (Exception)
                    //    {
                    //        return null;
                    //    }
                    //    users = Context.Users.Include("Country").Where(x => x.Username.Contains(filter.Filter)).ToList();

                    //}

                    //if (filter.Filter == "verified")
                    //{
                    //    bool verified;
                    //    if (filter.Filter == "true")
                    //    {
                    //        verified = true;
                    //    }
                    //    else
                    //    {
                    //        verified = false;
                    //    }
                    //    users = Context.Users.Include("Country").Where(x => x.Verified ==(verified)).ToList();

                    //}
                }
                users = users.GroupBy(x => x.ID).Select(x => x.First()).ToList();

                users = users.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                foreach (var userentity in users)
                {
                    var country = db.Countries.Where(x => x.ID == userentity.Country.ID).FirstOrDefault();
                    if (country == null)
                    {
                        return null;
                    }

                    userentity.Country = country;
                    var coins = db.Coins.Where(x => x.User.ID == userentity.ID).Sum(x => x.Amount);
                    var user = AutoMapper.Map<User, UserDto>(userentity);
                    user.Coins = coins;
                    usersDto.Add(user);
                }

                if (usersDto.Count > 0)
                {
                    return usersDto;
                }
                else
                {
                    return null;
                }
            }
        }

        public UserOauthProvider CreateOauthProvider(UserDto userModel)
        {
            using (var db = new DatabaseContext())
            {
                var oauthIdentifier = db.UserOauthProviders.FirstOrDefault(x => x.Identifier == userModel.Identifier);
                if (oauthIdentifier == null)
                {
                    //Create the oauth provider & attach that to the user
                    var oauthProvider = new UserOauthProvider()
                    {
                        Identifier = userModel.Identifier,
                        Type = userModel.Provider
                    };

                    var createdProvider = db.UserOauthProviders.Add(oauthProvider).Entity;

                    db.SaveChanges();

                    return createdProvider;
                }
                else
                {
                    return null;
                }
            }
        }

        public UserDto UpdateVerified(UserDto userDto)
        {
            var user = Context.Users.FirstOrDefault(x => x.ID == userDto.ID);

            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.User.ID == user.ID);

            if (user != null)
            {
                user.Verified = true;
                Context.SaveChanges();

                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto UpdateNewsletter(UserDto userDto)
        {
            var user = Context.Users.FirstOrDefault(x => x.ID == userDto.ID);

            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.User.ID == user.ID);

            if (user != null)
            {
                user.Newsletter = true;
                Context.SaveChanges();

                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto GetUserByEmailToken(string token)
        {
            try
            {
                using (var db = new DatabaseContext())
                {
                    //Check for provider with same identifier
                    var validTokenUser = db.UserVerifications.Include("User").FirstOrDefault(x =>
                    x.Token == token &&
                    x.ExpireDate > DateTime.Now);

                    if (validTokenUser != null)
                    {
                        //Check for provider with same identifier

                        var user = new UserDto()
                        {
                            ID = validTokenUser.User.ID,
                            Email = validTokenUser.User.Email,
                            Username = validTokenUser.User.Username,
                            AvatarImage = validTokenUser.User.AvatarImage,
                            HeaderImage = validTokenUser.User.HeaderImage,
                            Level = validTokenUser.User.Level,
                            Experience = ExperienceService.GetCurrentUserExperience(validTokenUser.ID),
                            ExperienceLeft = ExperienceService.CalculateUserExperienceLeft(validTokenUser.ID)
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool VerifyKyc(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userId);

                if (user == null)
                {
                    return false;
                }

                user.KycVerification = true;
                user.PerformKYCProcess = false;
                db.SaveChanges();
                return true;
            }
        }

        public UserDto UpdateNote(Guid userId, string note)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userId);

                if (user == null)
                {
                    throw new Exception("User not found");
                }
                try
                {
                    user.AdminNote = note;
                    db.Users.Update(user);
                    db.SaveChanges();
                    var userdto = AutoMapper.Map<User, UserDto>(user);
                    return userdto;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public UserDto UpdateUser(UserDto userDto, Guid country)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userDto.ID);

                if (user == null)
                {
                    return null;
                }

                var countryExist = db.Countries.FirstOrDefault(x => x.ID == country);
                if (countryExist == null)
                {
                    return null;
                }
                int years = DateTime.Now.Year - userDto.BirthDate.Year;
                if (userDto.Coins > 0)
                {
                    CoinService.CreateCoin(new CoinDto()
                    {
                        Amount = userDto.Coins,
                        Description = "Admin added coins",
                        User = userDto,
                        Created = DateHelper.ParseTimeZone(DateTime.UtcNow)
                    });
                }
                if (DateTime.Now.Month < userDto.BirthDate.Month
                    || (DateTime.Now.Month == userDto.BirthDate.Month
                    && DateTime.Now.Day < userDto.BirthDate.Day))
                    years--;

                if (years < 18)
                {
                    return null;
                }
                if (userDto.Role != (Role)1)
                {
                    user.GameProvider = null;
                }
                else
                {
                    user.GameProvider = userDto.GameProvider;
                }
                user.Status = userDto.Status;
                user.Username = userDto.Username;
                user.Role = (Role)userDto.Role;
                user.AvatarImage = userDto.AvatarImage;
                user.BirthDate = userDto.BirthDate;
                user.Country = countryExist;
                user.Email = userDto.Email;
                user.KycVerification = userDto.KycVerification;
                user.PerformKYCProcess = userDto.PerformKYCProcess;
                user.Verified = user.Verified;
                db.Users.Update(user);
                db.SaveChanges();

                return userDto;
            }
        }

        public UserDto UpdatePayOut(UserDto userDto)
        {
            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == userDto.Identifier);

            var user = Context.Users.FirstOrDefault(x => x.ID == oauthIdentifier.User.ID);

            if (user != null)
            {
                user.PayOutOption = userDto.PayOutOption;
                Context.SaveChanges();

                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto UpdateProfileImage(UserDto userDto)
        {
            var user = Context.Users.FirstOrDefault(x => x.ID == userDto.ID);

            if (user != null)
            {
                user.AvatarImage = userDto.AvatarImage;
                Context.SaveChanges();

                return GetUser(user.ID);
            }
            else
                return null;
        }

        public UserDto UpdateHeaderImage(UserDto userDto)
        {
            var user = Context.Users.FirstOrDefault(x => x.ID == userDto.ID);

            if (user != null)
            {
                user.HeaderImage = userDto.HeaderImage;
                Context.SaveChanges();

                return GetUser(user.ID);
            }
            else
                return null;
        }

        public UserDto UpdateCountry(UserDto userDto, Guid country)
        {
            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == userDto.Identifier);

            var user = Context.Users.FirstOrDefault(x => x.ID == oauthIdentifier.User.ID);

            if (user != null)
            {
                //Check if it is in the list of countries
                var countryExist = Context.Countries.FirstOrDefault(x => x.ID == country);
                if (countryExist == null)
                {
                    return null;
                }
                user.Country = countryExist;
                Context.SaveChanges();

                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto UpdateBirthDate(UserDto userDto)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userDto.ID);

                if (user != null)
                {
                    int years = DateTime.Now.Year - userDto.BirthDate.Year;

                    if (DateTime.Now.Month < userDto.BirthDate.Month
                        || (DateTime.Now.Month == userDto.BirthDate.Month
                        && DateTime.Now.Day < userDto.BirthDate.Day))
                        years--;

                    if (years < 18)
                    {
                        return null;
                    }

                    user.BirthDate = userDto.BirthDate;
                    db.SaveChanges();

                    return GetUser(userDto.ID);
                }
                else
                    return null;
            }
        }

        public UserDto UpdateUsername(UserDto userDto)
        {
            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == userDto.Identifier);

            var user = Context.Users.FirstOrDefault(x => x.ID == oauthIdentifier.User.ID);

            var sameUserName = Context.Users.FirstOrDefault(x => x.Username == userDto.Username);

            if (user != null)
            {
                if (sameUserName != null)
                {
                    return null;
                }

                user.Username = userDto.Username;
                Context.SaveChanges();
                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto UpdateEmail(UserDto userDto)
        {
            var oauthIdentifier = Context.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == userDto.Identifier);

            var user = Context.Users.FirstOrDefault(x => x.ID == oauthIdentifier.User.ID);

            var sameEmail = Context.Users.FirstOrDefault(x => x.Email == userDto.Email);

            if (user != null)
            {
                if (sameEmail != null)
                {
                    return null;
                }

                user.Email = userDto.Email;
                Context.SaveChanges();

                return GetUser(oauthIdentifier.Identifier);
            }
            else
                return null;
        }

        public UserDto UpdatePassword(UserDto userDto)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == userDto.ID);

                if (user != null)
                {
                    Random rnd = new Random();
                    Byte[] random = new Byte[10];

                    user.Password = Convert.ToBase64String(GenerateSaltedHash(Encoding.UTF8.GetBytes(userDto.Password), random));
                    user.Salt = Convert.ToBase64String(random);
                    db.SaveChanges();

                    return GetUser(userDto.ID);
                }
                else
                    return null;
            }
        }

        public UserReferralStatsModel GetReferralStats(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                var signups = db.UserRefferals.Include("User").Where(x => x.Refferal.Code == userId.ToString() && x.UsedTimestamp >= new DateTime(2020, 09, 13)).ToList();
                var earned = 0;

                foreach (var signup in signups)
                {
                    var attended = db.TournamentAttendes.Where(x => x.User.ID == signup.User.ID).Count();
                    if (attended >= 5)
                    {
                        earned = earned + 2;
                    }
                }
                if (earned > 0)
                {
                    return new UserReferralStatsModel()
                    {
                        Earned = earned,
                        Signups = signups.Count(),
                        UserId = userId
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public bool SendVerifySMS(string number)
        {
            TwilioClient.Init("AC22679f74ed1a8aa3378051f665ba2ef6", "1327b8094551c7fac999391dea679bbd");

            var response = new VoiceResponse();
            response.Say("Welcome to Reelzone Sander! Hope you like to play here.", voice: "woman", language: "en-EN");

            //+46707200214
            var call = CallResource.Create(
                new PhoneNumber("+46707200214"),
                from: new PhoneNumber("+12702015520"),
                twiml: response.ToString()
            );

            return true;
        }

        public UserDto CreateUser(UserDto userModel)
        {
            using (var db = new DatabaseContext())
            {
                var checkEmail = db.Users.FirstOrDefault(x => x.Email == userModel.Email);
                if (checkEmail == null)
                {
                    //Check for provider with same identifier
                    var oauthIdentifier = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == userModel.Identifier);

                    var errorMessage = string.Empty;
                    //*
                    if (userModel.Password != null)
                    {
                        Random rnd = new Random();
                        Byte[] random = new Byte[10];

                        userModel.Password = Convert.ToBase64String(GenerateSaltedHash(Encoding.UTF8.GetBytes(userModel.Password), random));
                        //var test_salt = Convert.ToBase64String(random);
                        userModel.Salt = Convert.ToBase64String(random);
                        //userModel.Password = CreateMD5(BitConverter.ToString(HashPasswordV2(userModel.Password, _rng)));  //System.Text.Encoding.UTF8.GetString(HashPasswordV2(userModel.Password, _rng));
                    }

                    //Not found - Create a user and a provider attached to the new user
                    if (oauthIdentifier == null)
                    {
                        var country = db.Countries.FirstOrDefault(x => x.Name == userModel.Country.Name);
                        if (country == null)
                        {
                            return null;
                        }
                        //Create user objec with datapoints
                        var UserObj = new User()
                        {
                            Email = userModel.Email,
                            Username = userModel.Username,
                            Status = 0,
                            BirthDate = userModel.BirthDate,
                            //TODO Move up to *
                            Password = userModel.Provider == ProviderType.Email ? userModel.Password : null,
                            Salt = userModel.Provider == ProviderType.Email ? userModel.Salt : null,
                            AvatarImage = userModel.AvatarImage,
                            HeaderImage = "https://res.cloudinary.com/reelzone/image/upload/v1596819934/tvzpiiiu6mncrnsd9pwf.png",
                            Country = country,
                            UserCreated = DateHelper.ParseTimeZone(DateTime.UtcNow),
                            LastLoggedIn = DateHelper.ParseTimeZone(DateTime.UtcNow)
                        };

                        //Add it to the context
                        var createdUser = db.Users.Add(UserObj).Entity;

                        //Create the oauth provider & attach that to the user
                        var oauthProvider = new UserOauthProvider()
                        {
                            User = createdUser,
                            Identifier = userModel.Identifier,
                            Type = userModel.Provider
                        };

                        //Add it to the context
                        var createdProvider = db.UserOauthProviders.Add(oauthProvider).Entity;
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        //TODO: Goes to quick after login
                        var userDto = GetUser(createdProvider.Identifier);

                        CoinService.CreateCoin(new CoinDto()
                        {
                            Amount = 10000,
                            Description = "Welcome coins",
                            User = userDto,
                            Created = DateHelper.ParseTimeZone(DateTime.UtcNow)
                        });

                        db.Refferals.Add(new Refferal()
                        {
                            Amount = 5000,
                            Code = userDto.ID.ToString(),
                            Description = userDto.Username + " personal refferal code."
                        });

                        db.SaveChanges();

                        //UserAchievmentService.GenerateUserAnchievementsJobs(AchievementPeriod.Day, userDto.ID);
                        //UserAchievmentService.GenerateUserAnchievementsJobs(AchievementPeriod.Month, userDto.ID);
                        //UserAchievmentService.GenerateUserAnchievementsJobs(AchievementPeriod.Week, userDto.ID);
                        //UserAchievmentService.GenerateUserAnchievementsJobs(AchievementPeriod.Lifetime, userDto.ID);

                        return userDto;
                    }
                    // Since we made the updates separately there is no need to update here
                    else
                    {
                        return null;
                        ////Update user
                        //if (oauthIdentifier.User != null)
                        //{
                        //    var user = Context.Users.FirstOrDefault(x => x.ID == oauthIdentifier.User.ID);

                        //    user.AvatarImage = userModel.AvatarImage;
                        //    user.Email = userModel.Email;
                        //    Context.SaveChanges();

                        //    return GetUser(oauthIdentifier.Identifier);
                        //}
                        //else
                        //{
                        //    //Create user object with datapoints
                        //    var UserObj = new User()
                        //    {
                        //        Email = userModel.Email,
                        //        Username = userModel.Username,
                        //        Password = null,
                        //        AvatarImage = userModel.AvatarImage,
                        //        HeaderImage = null,
                        //        Country = userModel.Country,
                        //    };

                        //    //Add it to the context
                        //    var createdUser = Context.Users.Add(UserObj).Entity;
                        //    oauthIdentifier.User = createdUser;

                        //    Context.SaveChanges();

                        //    return GetUser(oauthIdentifier.Identifier);
                        //}
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
              new byte[plainText.Length + salt.Length];

            for (int i = 0; i < plainText.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainText[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[plainText.Length + i] = salt[i];
            }

            return algorithm.ComputeHash(plainTextWithSaltBytes);
        }

        //TODO Move to a helper
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private bool PasswordValidation(string password, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            //Check for null och whitespace
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new Exception("Password should not be empty");
            }

            var hasLength = new Regex(@".{8,}");
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            //Check the criteria return true/false
            if (!hasLowerChar.IsMatch(password))
            {
                ErrorMessage = "Password should contain at least 1 lowercase letter";
                return false;
            }
            else if (!hasUpperChar.IsMatch(password))
            {
                ErrorMessage = "Password should contain at least 1 uppercase letter";
                return false;
            }
            else if (!hasLength.IsMatch(password))
            {
                ErrorMessage = "Password should be at least 8 characters";
                return false;
            }
            else if (!hasNumber.IsMatch(password))
            {
                ErrorMessage = "Password should contain at least 1 number";
                return false;
            }
            else if (!hasSymbols.IsMatch(password))
            {
                ErrorMessage = "Password should contain at least 1 special case characters";
                return false;
            }
            else
            {
                return true;
            }
        }

        //TODO mod output CreateMD5
        private static byte[] HashPasswordV2(string password, RandomNumberGenerator rng)
        {
            const KeyDerivationPrf Pbkdf2Prf = KeyDerivationPrf.HMACSHA1; // default for Rfc2898DeriveBytes
            const int Pbkdf2IterCount = 1000; // default for Rfc2898DeriveBytes
            const int Pbkdf2SubkeyLength = 256 / 8; // 256 bits
            const int SaltSize = 128 / 8; // 128 bits
                                          // Produce a version 2 text hash
            byte[] salt = new byte[SaltSize];
            rng.GetBytes(salt);
            byte[] subkey = KeyDerivation.Pbkdf2(password, salt, Pbkdf2Prf, Pbkdf2IterCount, Pbkdf2SubkeyLength);
            var outputBytes = new byte[1 + SaltSize + Pbkdf2SubkeyLength];
            outputBytes[0] = 0x00; // format marker
            Buffer.BlockCopy(salt, 0, outputBytes, 1, SaltSize);
            Buffer.BlockCopy(subkey, 0, outputBytes, 1 + SaltSize, Pbkdf2SubkeyLength);
            return outputBytes;
        }

        public UserOauthProvider GetOauthProvider(string identifier)
        {
            using (var db = new DatabaseContext())
            {
                //Check for provider with same identifier
                var oauthIdentifier = db.UserOauthProviders.FirstOrDefault(x => x.Identifier == identifier);

                if (oauthIdentifier != null)
                {
                    return oauthIdentifier;
                }
                else
                {
                    return null;
                }
            }
        }

        public UserDto GetUserByToken(string token)
        {
            try
            {
                using (var db = new DatabaseContext())
                {
                    var now = DateHelper.ParseTimeZone(DateTime.UtcNow);
                    //Check for provider with same identifier
                    var validTokenUser = db.WalletTokens.Include("User").FirstOrDefault(x =>
                    x.Token == token /*&&
                    x.Created.AddMinutes(5) > now*/);

                    if (validTokenUser != null)
                    {
                        //Check for provider with same identifier
                        var coins = db.Coins.Where(x => x.User.ID == validTokenUser.User.ID).Sum(x => x.Amount);
                        var user = new UserDto()
                        {
                            ID = validTokenUser.User.ID,
                            Email = validTokenUser.User.Email,
                            Username = validTokenUser.User.Username,
                            AvatarImage = validTokenUser.User.AvatarImage,
                            HeaderImage = validTokenUser.User.HeaderImage,
                            Status = validTokenUser.User.Status,
                            Level = validTokenUser.User.Level,
                            Experience = ExperienceService.GetCurrentUserExperience(validTokenUser.ID),
                            ExperienceLeft = ExperienceService.CalculateUserExperienceLeft(validTokenUser.ID),
                            Coins = coins
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public UserDto GetUserByVerifytoken(string token)
        {
            try
            {
                using (var db = new DatabaseContext())
                {
                    var now = DateHelper.ParseTimeZone(DateTime.UtcNow);
                    //Check for provider with same identifier
                    var validTokenUser = db.UserVerifications.Include("User").FirstOrDefault(x =>
                    x.Token == token);

                    if (validTokenUser != null)
                    {
                        //Check for provider with same identifier
                        var coins = db.Coins.Where(x => x.User.ID == validTokenUser.User.ID).Sum(x => x.Amount);
                        var user = new UserDto()
                        {
                            ID = validTokenUser.User.ID,
                            Email = validTokenUser.User.Email,
                            Username = validTokenUser.User.Username,
                            AvatarImage = validTokenUser.User.AvatarImage,
                            HeaderImage = validTokenUser.User.HeaderImage,
                            Status = validTokenUser.User.Status,
                            Level = validTokenUser.User.Level,
                            Experience = ExperienceService.GetCurrentUserExperience(validTokenUser.ID),
                            ExperienceLeft = ExperienceService.CalculateUserExperienceLeft(validTokenUser.ID),
                            Coins = coins
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public User GetUserEntity(string Identifier)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var oauthIdentifier = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == Identifier);

                    if (oauthIdentifier != null)
                    {
                        return oauthIdentifier.User;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public UserDto GetCleanUser(string Identifier)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var oauthIdentifier = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == Identifier);

                    if (oauthIdentifier != null)
                    {
                        var user = new UserDto()
                        {
                            ID = oauthIdentifier.User.ID,
                            Username = oauthIdentifier.User.Username,
                            AvatarImage = oauthIdentifier.User.AvatarImage,
                            HeaderImage = oauthIdentifier.User.HeaderImage,
                            Status = oauthIdentifier.User.Status,
                            Role = oauthIdentifier.User.Role,
                            Provider = oauthIdentifier.Type,
                            Level = oauthIdentifier.User.Level
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public UserDto GetCleanUser(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var User = db.Users.FirstOrDefault(x => x.ID == userId);

                    if (User != null)
                    {
                        var user = new UserDto()
                        {
                            ID = User.ID,
                            Username = User.Username,
                            AvatarImage = User.AvatarImage,
                            HeaderImage = User.HeaderImage,
                            Status = User.Status,
                            Role = User.Role,
                            Level = User.Level
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public UserDto GetUser(string Identifier)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var oauthIdentifier = db.UserOauthProviders.Include(x => x.User).ThenInclude(x => x.GameProvider).FirstOrDefault(x => x.Identifier == Identifier);

                    if (oauthIdentifier != null)
                    {
                        if (oauthIdentifier.User.GameProvider != null)
                        {
                            oauthIdentifier.User.GameProvider.Games = null;
                            oauthIdentifier.User.GameProvider.Users = null;
                        }

                        var coins = db.Coins.Where(x => x.User.ID == oauthIdentifier.User.ID).Sum(x => x.Amount);
                        var referral = db.UserRefferals.Include("Refferal").FirstOrDefault(x => x.User.ID == oauthIdentifier.User.ID);
                        var user = new UserDto()
                        {
                            ID = oauthIdentifier.User.ID,
                            Email = oauthIdentifier.User.Email,
                            Username = oauthIdentifier.User.Username,
                            AvatarImage = oauthIdentifier.User.AvatarImage,
                            HeaderImage = oauthIdentifier.User.HeaderImage,
                            Status = oauthIdentifier.User.Status,
                            Role = oauthIdentifier.User.Role,
                            Provider = oauthIdentifier.Type,
                            Identifier = oauthIdentifier.Identifier,
                            Level = oauthIdentifier.User.Level,
                            Experience = ExperienceService.GetCurrentUserExperience(oauthIdentifier.User.ID),
                            ExperienceLeft = ExperienceService.CalculateUserExperienceLeft(oauthIdentifier.User.ID),
                            GameProvider = oauthIdentifier.User.GameProvider,
                            Coins = coins,
                            BirthDate = oauthIdentifier.User.BirthDate,
                            PerformKYCProcess = oauthIdentifier.User.PerformKYCProcess,
                            KycVerification = oauthIdentifier.User.KycVerification,
                            CreatedDate = oauthIdentifier.User.UserCreated,
                            Referred = (referral?.Refferal.ID),
                            Newsletter = true,
                            Banned = oauthIdentifier.User.Banned
                        };

                        return user;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public List<UserActivity> GetUserActivities(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var user = db.Users.FirstOrDefault(x => x.ID == userId);

                    if (user != null)
                    {
                        var activities = db.UserActivities
                            .Include("Activity")
                            .Where(x => x.User.ID == user.ID &&
                            (x.Activity.Handle == "UserJoinedTournament" ||
                            x.Activity.Handle == "UserLeveledUp"))
                            .OrderByDescending(x => x.TimeStamp)
                            .Take(5).ToList();
                        foreach (var activity in activities)
                        {
                            activity.User = null;
                        }
                        return activities;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public UserProfileModel GetUserProfile(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    //Check for provider with same identifier
                    var user = db.Users.FirstOrDefault(x => x.ID == userId);

                    if (user != null)
                    {
                        var coins = db.Coins.Where(x => x.User.ID == user.ID).Sum(x => x.Amount);
                        //var activities = db.UserActivities.Where(x => x.User.ID == user.ID).ToList();
                        var tournaments = db.TournamentAttendes.Where(x => x.User.ID == user.ID).ToList().Count();
                        var spins = db.GameTransactions.Where(x => x.User.ID == user.ID && (x.Description == "Bet on tournament" || x.Description == "Betting on game")).ToList().Count();
                        var userProfileModel = new UserProfileModel()
                        {
                            ID = user.ID,
                            Username = user.Username,
                            AvatarImage = user.AvatarImage,
                            HeaderImage = user.HeaderImage,
                            Role = user.Role,
                            Level = user.Level,
                            Experience = ExperienceService.GetCurrentUserExperience(user.ID),
                            ExperienceLeft = ExperienceService.CalculateUserExperienceLeft(user.ID),
                            Coins = coins,
                            //Activities = activities,
                            Bets = spins,
                            Tournaments = tournaments
                        };

                        return userProfileModel;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public UserDto GetUser(Guid userID)
        {
            using (var db = new DatabaseContext())
            {
                //Check for provider with same identifier
                var userObj = db.Users.FirstOrDefault(x => x.ID == userID);

                if (userObj != null)
                {
                    var coins = db.Coins.Where(x => x.User.ID == userObj.ID).Sum(x => x.Amount);
                    var user = new UserDto()
                    {
                        ID = userObj.ID,
                        Email = userObj.Email,
                        Status = userObj.Status,
                        Username = userObj.Username,
                        AvatarImage = userObj.AvatarImage,
                        HeaderImage = userObj.HeaderImage,
                        Coins = coins
                    };

                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public UserDto GetUserByEmail(string email)
        {
            using (var db = new DatabaseContext())
            {
                var userObj = db.Users.FirstOrDefault(x => x.Email == email);

                if (userObj != null)
                {
                    var coins = db.Coins.Where(x => x.User.ID == userObj.ID).Sum(x => x.Amount);
                    var user = new UserDto()
                    {
                        ID = userObj.ID,
                        Email = userObj.Email,
                        Status = userObj.Status,
                        Username = userObj.Username,
                        AvatarImage = userObj.AvatarImage,
                        HeaderImage = userObj.HeaderImage,
                        Role = userObj.Role,
                        Coins = coins,
                        Banned = userObj.Banned
                    };

                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<CountryDto> GetCountries()
        {
            using (var db = new DatabaseContext())
            {
                var countries = db.Countries.ToList();
                List<CountryDto> countriesDto = new List<CountryDto>();
                foreach (var country in countries)
                {
                    var countryDto = AutoMapper.Map<Country, CountryDto>(country);
                    countriesDto.Add(countryDto);
                }

                return countriesDto;
            }
        }

        public bool UserOauthProviderConnected(string identifier)
        {
            using (var db = new DatabaseContext())
            {
                //Check for provider with same identifier
                var userOauthProviderCheck = db.UserOauthProviders.Include("User").FirstOrDefault(x => x.Identifier == identifier);

                if (userOauthProviderCheck != null)
                {
                    if (userOauthProviderCheck.User == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        //Function for signin after registration
        public async Task<bool> SignInEmailAsync(UserDto user, HttpContext httpContext)
        {
            var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.Email)
                    };
            if (user.Role == (Role)2)
            {
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));
            }
            if (user.Role == (Role)1)
            {
                claims.Add(new Claim(ClaimTypes.Role, "Provider"));
            }

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                //AllowRefresh = <bool>,
                // Refreshing the authentication session should be allowed.

                //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                // The time at which the authentication ticket expires. A
                // value set here overrides the ExpireTimeSpan option of
                // CookieAuthenticationOptions set with AddCookie.

                //IsPersistent = true,
                // Whether the authentication session is persisted across
                // multiple requests. When used with cookies, controls
                // whether the cookie's lifetime is absolute (matching the
                // lifetime of the authentication ticket) or session-based.

                //IssuedUtc = <DateTimeOffset>,
                // The time at which the authentication ticket was issued.

                //RedirectUri = <string>
                // The full path or absolute URI to be used as an http
                // redirect response value.
            };

            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);

            return true;
        }

        //Function for signin
        public async Task<bool> SignInAsync(string email, string password, HttpContext httpContext)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.Email == email);
                if (user != null)
                {
                    //password = CreateMD5(BitConverter.ToString(HashPasswordV2(password, _rng)));

                    var salt = user.Salt;

                    password = Convert.ToBase64String(GenerateSaltedHash(Encoding.UTF8.GetBytes(password), Convert.FromBase64String(salt)));

                    //User email & password is correct
                    if (user.Password == password)
                    {
                        var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.Email)
                    };
                        if (user.Role == (Role)2)
                        {
                            claims.Add(new Claim(ClaimTypes.Role, "Admin"));
                        }
                        if (user.Role == (Role)1)
                        {
                            claims.Add(new Claim(ClaimTypes.Role, "Provider"));
                        }

                        var claimsIdentity = new ClaimsIdentity(
                            claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                        return true;
                    }
                    else
                    {
                        throw new Exception("Wrong password");
                    }
                }
                else
                {
                    throw new Exception("Wrong email");
                }
            }
        }

        public List<UserDto> GetLimitedUsers(int howMany, int page)
        {
            using (var db = new DatabaseContext())
            {
                var users = db.Users.Include("Country").Include(x => x.GameProvider).Skip((page - 1) * howMany).Take(howMany).ToList();
                List<UserDto> usersDto = new List<UserDto>();

                if (users != null)
                {
                    foreach (var userentity in users)
                    {
                        var coins = db.Coins.Where(x => x.User.ID == userentity.ID).Sum(x => x.Amount);
                        var country = db.Countries.Where(x => x.ID == userentity.Country.ID).FirstOrDefault();
                        if (country == null)
                        {
                            return null;
                        }

                        userentity.Country = country;
                        if (userentity.GameProvider != null)
                        {
                            userentity.GameProvider.Users = null;
                        }

                        var user = AutoMapper.Map<User, UserDto>(userentity);
                        user.Coins = coins;
                        usersDto.Add(user);
                    }

                    return usersDto;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<DashboardDto> CountUsers(bool lastMonth)
        {
            if (lastMonth == true)
            {
                return Context.Users.Where(x => x.CreatedDate >= DateTime.UtcNow.AddDays(-31)).Select(u => new { u.CreatedDate.Year, u.CreatedDate.Month, u.CreatedDate.Day })
                       .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                       {
                           Year = key.Year,
                           Month = key.Month,
                           Day = key.Day,
                           Total = group.Count()
                       }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
            }
            return Context.Users.Select(u => new { u.CreatedDate.Year, u.CreatedDate.Month })
                       .GroupBy(x => new { x.Year, x.Month }, (key, group) => new DashboardDto()
                       {
                           Year = key.Year,
                           Month = key.Month,
                           Day = null,
                           Total = group.Count()
                       }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ToList();
        }

        public List<DashboardDto> FillterCountUsers(DateTime? start, DateTime? end)
        {
            if (start == null && end == null)
            {
                return CountUsers(false);
            }
            if (start != null && end != null)
            {
                return Context.Users.Where(x => x.CreatedDate >= start && x.CreatedDate <= end).Select(u => new { u.CreatedDate.Year, u.CreatedDate.Month, u.CreatedDate.Day })
                       .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                       {
                           Year = key.Year,
                           Month = key.Month,
                           Day = key.Day,
                           Total = group.Count()
                       }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
            }
            else if (start != null && end == null)
            {
                return Context.Users.Where(x => x.CreatedDate >= start).Select(u => new { u.CreatedDate.Year, u.CreatedDate.Month, u.CreatedDate.Day })
                       .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                       {
                           Year = key.Year,
                           Month = key.Month,
                           Day = key.Day,
                           Total = group.Count()
                       }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
            }
            else
            {
                return Context.Users.Where(x => x.CreatedDate <= end).Select(u => new { u.CreatedDate.Year, u.CreatedDate.Month, u.CreatedDate.Day })
                       .GroupBy(x => new { x.Year, x.Month, x.Day }, (key, group) => new DashboardDto()
                       {
                           Year = key.Year,
                           Month = key.Month,
                           Day = key.Day,
                           Total = group.Count()
                       }).OrderBy(a => a.Year)
                                 .ThenBy(a => a.Month)
                                 .ThenBy(a => a.Day)
                                 .ToList();
            }
        }

        public bool DeleteUser(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var user = db.Users.FirstOrDefault(x => x.ID == id);
                if (user != null)
                {
                    var coins = db.Coins.Where(x => x.User == user).ToList();
                    if (coins.Count != 0)
                    {
                        db.Coins.RemoveRange(coins);
                    }
                    var traqnsactions = db.GameTransactions.Where(x => x.User == user).ToList();
                    if (traqnsactions.Count != 0)
                    {
                        foreach (var traqnsaction in traqnsactions)
                        {
                            traqnsaction.User = null;
                        }
                        db.GameTransactions.UpdateRange(traqnsactions);
                    }

                    var tas = db.TournamentAttendes.Where(x => x.User == user).ToList();
                    if (tas.Count != 0)
                    {
                        foreach (var ta in tas)
                        {
                            ta.User = null;
                        }
                        db.TournamentAttendes.UpdateRange(tas);
                    }

                    var uas = db.UserAchievements.Where(x => x.User == user).ToList();
                    if (uas.Count != 0)
                    {
                        db.UserAchievements.RemoveRange(uas);
                    }
                    var useractivities = db.UserActivities.Where(x => x.User == user).ToList();
                    if (useractivities.Count != 0)
                    {
                        db.UserActivities.RemoveRange(useractivities);
                    }

                    var ue = db.UserExperiences.Where(x => x.User == user).ToList();
                    if (ue.Count != 0)
                    {
                        db.UserExperiences.RemoveRange(ue);
                    }
                    var uops = db.UserOauthProviders.Where(x => x.User == user).ToList();
                    if (uops.Count != 0)
                    {
                        db.UserOauthProviders.RemoveRange(uops);
                    }
                    var ur = db.UserRefferals.Where(x => x.User == user).ToList();
                    if (ur.Count != 0)
                    {
                        db.UserRefferals.RemoveRange(ur);
                    }
                    db.SaveChanges();
                    var wt = db.WalletTokens.FirstOrDefault(x => x.User == user);
                    if (wt != null)
                    {
                        db.WalletTokens.Remove(wt);
                    }
                    db.SaveChanges();

                    db.Users.Remove(user);
                    db.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
        }

        public string GenerateUserTokenAsync(Guid userID, string tokenType)
        {
            string token;

            var user = Context.Users.FirstOrDefault(x => x.ID == userID);

            if (user != null)
            {
                var userVerify = new UserVerify();

                token = GenerateToken(userID);

                if (tokenType == "Email")
                {
                    userVerify = new UserVerify()
                    {
                        User = user,
                        Token = token,
                        TokenType = tokenType,
                        ExpireDate = DateTime.Today.AddDays(3)
                    };
                }
                else if (tokenType == "PasswordReset")
                {
                    userVerify = new UserVerify()
                    {
                        User = user,
                        Token = token,
                        TokenType = tokenType,
                        ExpireDate = DateTime.Today.AddMinutes(5)
                    };
                }
                else
                {
                    token = null;
                }

                Context.UserVerifications.Add(userVerify);

                Context.SaveChanges();

                return token;
            }
            return null;
        }

        public string GenerateToken(Guid userID)
        {
            var mySecret = "tfsad312e4^&%&^%&^hqsffb2%%%";
            var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userID.ToString()),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return CreateMD5(tokenHandler.WriteToken(token));
        }

        ~UserService()
        {
            Context.Dispose();
        }
    }
}