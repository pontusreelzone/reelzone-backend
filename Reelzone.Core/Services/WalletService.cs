﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using System.Text;
using Reelzone.Core.Dtos;
using System.Text.RegularExpressions;
using Reelzone.Core.Helpers;
using System.Data.Entity;

namespace Reelzone.Core.Services
{
    public class WalletService : IWalletService
    {
        public IUserService UserService;
        public DatabaseContext Context;
        public DateHelper DateHelper;

        public WalletService(DatabaseContext dbContext, IUserService userService)
        {
            Context = dbContext;
            UserService = userService;
            DateHelper = new DateHelper();
        }

        public WalletTokenDto CreateWalletToken(WalletTokenDto walletTokenDto)
        {
            using (var db = new DatabaseContext())
            {
                var tournament = db.Tournaments.FirstOrDefault(x => x.ID == walletTokenDto.Tournament.ID);
                if (tournament != null)
                {
                    /*var alreadyCreatedToken = db.WalletTokens.FirstOrDefault(x =>
                         x.Tournament.ID == tournament.ID && x.User.ID == walletTokenDto.User.ID &&
                         x.Created.AddMinutes(5) < DateTime.Now)

                     if (alreadyCreatedToken == null)
                     {*/
                    Random rnd = new Random();
                    Byte[] random = new Byte[10];

                    var token = Regex.Replace(Convert.ToBase64String(UserService.GenerateSaltedHash(
                            Encoding.UTF8.GetBytes(tournament.ID.ToString() + walletTokenDto.User.ID.ToString() + DateHelper.ParseTimeZone(DateTime.UtcNow)),
                            random)), @"[^0-9a-zA-Z:,]+", "");

                    var newWalletToken = new WalletToken()
                    {
                        User = db.Users.FirstOrDefault(x => x.ID == walletTokenDto.User.ID),
                        Tournament = tournament,
                        Token = token,
                        Created = DateHelper.ParseTimeZone(DateTime.UtcNow),
                        Type = TokenType.Tournament
                    };

                    db.WalletTokens.Add(newWalletToken);

                    db.SaveChanges();

                    var walletDto = new WalletTokenDto()
                    {
                        Token = newWalletToken.Token,
                        Created = newWalletToken.Created
                    };

                    return walletDto;
                    /*}
                    else
                    {
                        var walletDto = new WalletTokenDto()
                        {
                            Token = alreadyCreatedToken.Token,
                            Created = alreadyCreatedToken.Created
                        };

                        return walletDto;
                    }*/
                }
                else
                {
                    return null;
                }
            }
        }

        public WalletTokenDto CreatePlayWalletToken(WalletTokenDto walletTokenDto)
        {
            using (var db = new DatabaseContext())
            {
                /*var alreadyCreatedToken = db.WalletTokens.FirstOrDefault(x =>
                        x.Tournament.ID == tournament.ID && x.User.ID == walletTokenDto.User.ID &&
                        x.Created.AddMinutes(5) < DateTime.Now)

                    if (alreadyCreatedToken == null)
                    {*/
                Random rnd = new Random();
                Byte[] random = new Byte[10];

                var token = Regex.Replace(Convert.ToBase64String(UserService.GenerateSaltedHash(
                        Encoding.UTF8.GetBytes(walletTokenDto.User.ID.ToString() + DateHelper.ParseTimeZone(DateTime.UtcNow)),
                        random)), @"[^0-9a-zA-Z:,]+", "");

                var newWalletToken = new WalletToken()
                {
                    User = db.Users.FirstOrDefault(x => x.ID == walletTokenDto.User.ID),
                    Token = token,
                    Created = DateHelper.ParseTimeZone(DateTime.UtcNow),
                    Type = TokenType.Play
                };

                db.WalletTokens.Add(newWalletToken);

                db.SaveChanges();

                var walletDto = new WalletTokenDto()
                {
                    Token = newWalletToken.Token,
                    Created = newWalletToken.Created
                };

                return walletDto;
            }
        }

        public WalletToken GetToken(string token)
        {
            using (var db = new DatabaseContext())
            {
                var tokenEntity = db.WalletTokens.Include("Tournament").FirstOrDefault(x => x.Token == token);
                return tokenEntity;
            }
        }

        ~WalletService()
        {
            Context.Dispose();
        }
    }
}