﻿using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using System.Text;
using Reelzone.Core.Dtos;
using System.Text.RegularExpressions;
using Reelzone.Core.Helpers;
using System.Data.Entity;

namespace Reelzone.Core.Services
{
    public class WheelService : IWheelService
    {
        public DatabaseContext Context;
        public DateHelper DateHelper;
        public Random Random = new Random();

        public WheelService(DatabaseContext dbContext)
        {
            Context = dbContext;
            DateHelper = new DateHelper();
        }

        public int Roll()
        {
            int num = Random.Next(0, 100);

            //10%
            if (num <= 10)
            {
                return 7;
            }
            //25%
            else if (num > 10 && num <= 35)
            {
                return 5;
            }
            //35%
            else if (num > 35 && num <= 70)
            {
                return 1;
            }
            //10%
            else if (num > 70 && num <= 80)
            {
                return 3;
            }
            //10%
            else if (num > 80 && num <= 90)
            {
                return 4;
            }
            //5%
            else if (num > 90 && num <= 95)
            {
                return 2;
            }
            //5%
            else if (num > 95)
            {
                return 6;
            }
            else
            {
                return 1;
            }
        }

        public int GetCoinWinnings(int spin)
        {
            int amount = 0;
            switch (spin)
            {
                case 1:
                    amount = 5000;
                    break;

                case 2:
                    amount = 20000;
                    break;

                case 3:
                    amount = 10000;
                    break;

                case 4:
                    amount = 15000;
                    break;

                case 5:
                    amount = 1000;
                    break;

                case 6:
                    amount = 50000;
                    break;
            }
            return amount;
        }

        public int SpinWheel(Guid userId)
        {
            using (var db = new DatabaseContext())
            {
                // Define your startDate. In our case it would be 9/20/2014 00:00:00
                DateTime startDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);

                // Define your endDate. In our case it would be 9/21/2014 00:00:00
                DateTime endDate = startDate.AddDays(1);

                var canRoll = db.UserWheelSpins.FirstOrDefault(x => x.User.ID == userId && (x.Timestamp >= startDate && x.Timestamp <= endDate));

                if (canRoll == null)
                {
                    var roll = Roll();

                    db.UserWheelSpins.Add(new UserWheelSpin()
                    {
                        Amount = GetCoinWinnings(roll),
                        Timestamp = DateHelper.ParseTimeZone(DateTime.UtcNow),
                        User = db.Users.FirstOrDefault(x => x.ID == userId)
                    });

                    db.SaveChanges();

                    return roll;
                }
                else
                {
                    return 0;
                }
            }
        }

        ~WheelService()
        {
            Context.Dispose();
        }
    }
}