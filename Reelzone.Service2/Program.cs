using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Reelzone.Core.Contexts;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Services;

namespace Reelzone.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureContainer<ContainerBuilder>(builder =>
                {
                    builder.RegisterType<UserService>()
                        .As<IUserService>()
                        .UsingConstructor(typeof(DatabaseContext), typeof(IUserExperienceService))
                        .SingleInstance();

                    builder.RegisterType<UserActivityService>()
                        .As<IUserActivityService>()
                        .UsingConstructor(typeof(IUserExperienceService), typeof(IUserAchievmentService), typeof(DatabaseContext))
                        .SingleInstance();

                    builder.RegisterType<DatabaseContext>()
                        .SingleInstance();

                    builder.RegisterType<BusQueService>()
                        .As<IBusQueService>()
                        .UsingConstructor(typeof(DatabaseContext))
                        .SingleInstance();

                    builder.RegisterType<Mapper>()
                        .As<IAutoMapper>()
                        .SingleInstance();

                    builder.RegisterType<TournamentService>()
                        .As<ITournamentService>()
                        .UsingConstructor(typeof(IUserActivityService), typeof(IBusQueService), typeof(DatabaseContext), typeof(IAutoMapper))
                        .SingleInstance();

                    builder.RegisterType<UserAchievementService>()
                        .As<IUserAchievmentService>()
                        .UsingConstructor(typeof(IBusQueService), typeof(DatabaseContext), typeof(IUserExperienceService), typeof(IAutoMapper))
                        .SingleInstance();

                    builder.RegisterType<UserExperienceService>()
                        .As<IUserExperienceService>()
                        .UsingConstructor(typeof(DatabaseContext), typeof(IBusQueService))
                        .SingleInstance();
                })
                .ConfigureServices(services =>
                {
                    services.AddHostedService<Worker>();
                    services.AddDbContext<DatabaseContext>();
                    services.AddTransient<DatabaseContext>();
                })
                .Build();

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                });
    }
}