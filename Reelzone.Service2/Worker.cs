using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Reelzone.Service
{
    public class Worker : BackgroundService
    {
        // The name of your queue
        private const string QueueName = "reelzone-user-acitivty";

        // QueueClient is thread-safe. Recommended that you cache
        // rather than recreating it on every request
        public QueueClient Client;

        public static LoggerService LoggerService;
        public static IUserActivityService UserActivityService;
        public static ITournamentService TournamentService;

        public Worker(IUserActivityService userActivityService, ITournamentService tournamentService)
        {
            LoggerService = new LoggerService();
            UserActivityService = userActivityService;
            TournamentService = tournamentService;

            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // Create the queue if it does not exist already
            string connectionString = ConfigurationManager.AppSettings.Get("Microsoft.ServiceBus.ConnectionString");

            Client = new QueueClient(connectionString, QueueName, ReceiveMode.ReceiveAndDelete);

            // Configure the message handler options in terms of exception handling, number of concurrent messages to deliver, etc.
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                // Maximum number of concurrent calls to the callback ProcessMessagesAsync(), set to 1 for simplicity.
                // Set it according to how many messages the application wants to process in parallel.
                MaxConcurrentCalls = 1,

                // Indicates whether the message pump should automatically complete the messages after returning from user callback.
                // False below indicates the complete operation is handled by the user callback as in ProcessMessagesAsync().
                AutoComplete = false
            };

            // Register the function that processes messages.
            Client.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
            LoggerService.Log("Listening for messages on que " + QueueName + "...");
            Console.WriteLine("Listening for messages on que " + QueueName + "...");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
            }
        }

        private static async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            // Process the message.

            //TODO: Change to a smarter solution and enum for the message.label
            switch (message.Label)
            {
                case "internal-activity":
                    var UserActivityMessage = JsonConvert.DeserializeObject<UserActivityMessage>(Encoding.Default.GetString(message.Body));
                    UserActivityService.CreateUserActivity(UserActivityMessage.UserID, UserActivityMessage.ActivityHandle);
                    Console.WriteLine($"Received message: UserID:{UserActivityMessage.UserID} Activity:{UserActivityMessage.ActivityHandle}");
                    LoggerService.Log($"Received message: UserID:{UserActivityMessage.UserID} Activity:{UserActivityMessage.ActivityHandle}");
                    break;

                case "internal-spin":
                    var UserSpinMessage = JsonConvert.DeserializeObject<UserSpinMessage>(Encoding.Default.GetString(message.Body));
                    var attender = TournamentService.GetAttender(UserSpinMessage.AttenderID);
                    TournamentService.SpinTournament(UserSpinMessage.TournamentID, attender.User.ID, UserSpinMessage.Score);
                    Console.WriteLine($"Received message: UserID:{UserSpinMessage.AttenderID} spinned and Won:{UserSpinMessage.Score} in tournament {UserSpinMessage.TournamentID}");
                    LoggerService.Log($"Received message: UserID:{UserSpinMessage.AttenderID} spinned and Won:{UserSpinMessage.Score} in tournament {UserSpinMessage.TournamentID}");
                    break;
            }
        }

        // Use this handler to examine the exceptions received on the message pump.
        private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }
    }
}