﻿using Reelzone.Core.Entities;
using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class AdminUpdateUserApiModel
    {

        [Required]
        [EmailAddress]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",
         ErrorMessage = "Not an email")]
        public string Email { get; set; }
        [Required]
        public Role Role { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        public string GameProvider { get; set; }
        public long Coins { get; set; }

        [Required]
        public string Username { get; set; }
        [Required]
        public string AvatarImage { get; set; }
        [Required]
        public bool KycVerification { get; set; }
        [Required]

        public bool PerformKYC { get; set; }

        [Required]
        public Status Status { get; set; }

        [Required]
        public Guid Country { get; set; }

    }
}
