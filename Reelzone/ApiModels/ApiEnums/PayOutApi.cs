﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.ApiEnums
{
    public enum PayOutApi
    {
        None,
        BankTransfer,
        Trustly
    }
}
