﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class Dashboard
    {
        public List<DashboardDto> Attendees { get; set; }
        public List<DashboardDto> Spins { get; set; }
        public List<DashboardDto> Users { get; set; }
        public List<TournamentDto> Tournaments { get; set; }


    }
}
