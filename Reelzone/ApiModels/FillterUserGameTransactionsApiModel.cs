﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class FillterUserGameTransactionsApiModel
    {
        public string User { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}
