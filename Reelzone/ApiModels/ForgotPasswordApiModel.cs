﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Reelzone.ApiModels
{
    public class ForgotPasswordApiModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}