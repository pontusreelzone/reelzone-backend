﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class GameApiModel
    {
        public string ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string HeaderImage { get; set; }
        [Required]
        public string BackgroundImage { get; set; }
        [Required]
        public string LogoImage { get; set; }
        [Required]
        public string CardBackgroundImage { get; set; }
        [Required]
        public string URL { get; set; }
        [Required]
        public int ExternalGameId { get; set; }
        [Required]
        public string GameProviderID { get; set; }
        [Required]
        public bool Visible { get; set; }
    }
}
