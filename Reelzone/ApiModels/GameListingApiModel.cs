﻿using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class GameListingApiModel
    {
        public List<GameProvider> Providers { get; set; }
        public List<Game> Featured { get; set; }
    }
}