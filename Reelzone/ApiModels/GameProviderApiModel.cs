﻿using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class GameProviderApiModel
    {
        public string ID { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string IntegrationUrl { get; set; }
        [Required]
        public string IntegrationConfig { get; set; }
        [Required]
        public string IntegrationInitScript { get; set; }
        [Required]
        public IntegrationType IntegrationType { get; set; }
    }
}
