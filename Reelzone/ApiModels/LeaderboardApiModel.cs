﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class LeaderboardApiModel
    {
        public Game Game { get; set; }
        public Tournament Tournament { get; set; }
        public List<TournamentAttende> Attendes { get; set; }
    }
}