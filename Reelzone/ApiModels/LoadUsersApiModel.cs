﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class LoadUsersApiModel
    {
        [Required]
        public int HowMany { get; set; }
        [Required]
        public int Page { get; set; }

    }
}
