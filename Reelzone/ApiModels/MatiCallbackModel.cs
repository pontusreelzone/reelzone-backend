﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class MatiMetaData
    {
        public string ID { get; set; }
    }
    public class MatiDetails
    {
        public MatiAgeData age { get; set; }
    }

    public class MatiAgeData
    {
        public int data { get; set; }
    }

    public class MatiStepData
    {
        public MatiStepDataDOB dateOfBirth { get; set; }
    }

    public class MatiStepDataDOB
    {
        public string value { get; set; }
    }

    public class MatiStep
    {
        public int status { get; set; }
        public string id { get; set; }
        public MatiStepData data { get; set; }
    }

    public class MatiCallbackModel
    {
        public string eventName { get; set; }
        public MatiMetaData metadata { get; set; }
        public MatiDetails details { get; set; }
        public MatiStep step { get; set; }
        public string identityStatus { get; set; }
        public string status { get; set; }
    }
}
