﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class AuthenticateApiModel : BaseApiModel
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }
    }
}