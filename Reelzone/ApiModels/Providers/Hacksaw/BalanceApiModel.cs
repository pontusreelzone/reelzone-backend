﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
{
    "action":"Bet",
    "secret":"your_secret",
    "externalPlayerId":"3",
    "amount":100,
    "currency":"EUR",
    "roundId":12,
    "gameId":1,
    "gameSessionId":11,
    "externalSessionId":"",
    "transactionId":211134774
}
 */

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class BalanceApiModel : BaseApiModel
    {
        [JsonPropertyName("amount")]
        public int Amount { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("roundId")]
        public int RoundId { get; set; }

        [JsonPropertyName("gameSessionId")]
        public int GameSessionId { get; set; }

        [JsonPropertyName("transactionId")]
        public int TransactionId { get; set; }
    }
}