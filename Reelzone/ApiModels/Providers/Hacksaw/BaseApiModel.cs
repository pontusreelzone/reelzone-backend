﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class BaseApiModel
    {
        [JsonPropertyName("action")]
        public string Action { get; set; }

        [JsonPropertyName("secret")]
        public string Secret { get; set; }

        [JsonPropertyName("externalPlayerId")]
        public string ExternalPlayerId { get; set; }

        [JsonPropertyName("externalSessionId")]
        public int ExternalSessionId { get; set; }

        [JsonPropertyName("gameId")]
        public int GameId { get; set; }
    }
}