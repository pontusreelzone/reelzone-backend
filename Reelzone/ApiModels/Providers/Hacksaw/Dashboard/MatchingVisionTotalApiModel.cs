﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw.Dashboard
{
    public class MatchingVisionTotalApiModel
    {
        public int Clicks { get; set; }
        public int Deposits { get; set; }
        public int Signups { get; set; }
        public long Total_earnings { get; set; }
        public int FTD { get; set; }
    }
}
