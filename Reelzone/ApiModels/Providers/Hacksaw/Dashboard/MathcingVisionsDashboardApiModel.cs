﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw.Dashboard
{
    public class MathcingVisionsDashboardApiModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public MatchingVisionTotalApiModel  Total { get; set; }
    }
}
