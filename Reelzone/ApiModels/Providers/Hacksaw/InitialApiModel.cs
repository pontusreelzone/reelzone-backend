﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
{
    "action": "Balance",
    "secret": "your_secret"
}
 */

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class InitialApiModel
    {
        [JsonPropertyName("action")]
        public string Action { get; set; }

        [JsonPropertyName("secret")]
        public string Secret { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("amount")]
        public long Amount { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("roundId")]
        public long RoundId { get; set; }

        [JsonPropertyName("gameSessionId")]
        public long GameSessionId { get; set; }

        [JsonPropertyName("transactionId")]
        public long TransactionId { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("jackpotAmount")]
        public long JackpotAmount { get; set; }

        [JsonPropertyName("rolledBackTransactionId")]
        public long RolledBackTransactionId { get; set; }

        [JsonPropertyName("externalPlayerId")]
        public string ExternalPlayerId { get; set; }

        [JsonPropertyName("externalSessionId")]
        public string ExternalSessionId { get; set; }

        [JsonPropertyName("gameId")]
        public int GameId { get; set; }
    }
}