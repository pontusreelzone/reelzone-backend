﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    /**
     {
        "externalPlayerId":"3",
        "name":"Christopher",
        "accountCurrency":"EUR",
        "accountBalance":9330,
        "externalSessionId":"",
        "languageId":"en",
        "countryId":"RS",
        "birthDate":"1980-01-01",
        "registrationDate":"2018-01-11",
        "brandId":"brand_123",
        "gender":"m",
        "statusCode":0,
        "statusMessage":""
    }
     */

    public class AuthenticateResponseApiModel
    {
        [JsonPropertyName("externalPlayerId")]
        public string ExternalPlayerId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("accountCurrency")]
        public string AccountCurrency { get; set; }

        [JsonPropertyName("accountBalance")]
        public Int64 AccountBalance { get; set; }

        [JsonPropertyName("externalSessionId")]
        public string ExternalSessionId { get; set; }

        [JsonPropertyName("languageId")]
        public string LanguageId { get; set; }

        [JsonPropertyName("countryId")]
        public string CountryId { get; set; }

        [JsonPropertyName("birthDate")]
        public String BirthDate { get; set; }

        [JsonPropertyName("registrationDate")]
        public String RegistrationDate { get; set; }

        [JsonPropertyName("brandId")]
        public string BrandId { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("statusCode")]
        public StatusCodes StatusCode { get; set; }

        [JsonPropertyName("statusMessage")]
        public string StatusMessage { get; set; }
    }
}