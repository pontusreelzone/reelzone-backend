﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    /**
     {
        "accountBalance":9330,
        "accountCurrency":"EUR",
        "statusCode":0,
        "statusMessage":""
    }
     */

    public class BalanceResponseApiModel
    {
        [JsonPropertyName("accountBalance")]
        public Int64 AccountBalance { get; set; }

        [JsonPropertyName("accountCurrency")]
        public string AccountCurrency { get; set; }

        [JsonPropertyName("statusCode")]
        public StatusCodes StatusCode { get; set; }

        [JsonPropertyName("statusMessage")]
        public string StatusMessage { get; set; }
    }
}