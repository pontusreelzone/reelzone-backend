﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    /**
   {
        "accountBalance":9230,
        "externalTransactionId":46,
        "statusCode":0,
        "statusMessage":""
    }
     */

    public class BetResponseApiModel
    {
        [JsonPropertyName("accountBalance")]
        public Int64 AccountBalance { get; set; }

        [JsonPropertyName("externalTransactionId")]
        public int ExternalTransactionId { get; set; }

        [JsonPropertyName("statusCode")]
        public StatusCodes StatusCode { get; set; }

        [JsonPropertyName("statusMessage")]
        public string StatusMessage { get; set; }
    }
}