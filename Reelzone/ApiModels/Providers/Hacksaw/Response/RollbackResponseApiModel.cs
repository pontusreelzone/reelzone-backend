﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    /**
    {
        "accountBalance":11230,
        "externalTransactionId":47,
        "statusCode":0,
        "statusMessage":""
    }
     */

    public class RollbackResponseApiModel
    {
        [JsonPropertyName("accountBalance")]
        public Int64 AccountBalance { get; set; }

        [JsonPropertyName("externalTransactionId")]
        public string ExternalTransactionId { get; set; }

        [JsonPropertyName("statusCode")]
        public StatusCodes StatusCode { get; set; }

        [JsonPropertyName("statusMessage")]
        public string StatusMessage { get; set; }
    }
}