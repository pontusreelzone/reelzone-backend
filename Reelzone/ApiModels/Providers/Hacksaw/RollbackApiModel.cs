﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
 {
    "action":"Rollback",
    "secret":"your_secret",
    "externalPlayerId":"3",
    "amount":2000,
    "currency":"EUR",
    "roundId":13,
    "gameId":1,
    "gameSessionId":12,
    "externalSessionId":"",
    "transactionId":211134774,
    "rolledBackTransactionId":211134775
}
 */

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class RollbackApiModel : BaseApiModel
    {
        [JsonPropertyName("amount")]
        public int Amount { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("roundId")]
        public string RoundId { get; set; }

        [JsonPropertyName("gameSessionId")]
        public int GameSessionId { get; set; }

        [JsonPropertyName("transactionId")]
        public int TransactionId { get; set; }

        [JsonPropertyName("rolledBackTransactionId")]
        public int RolledBackTransactionId { get; set; }
    }
}