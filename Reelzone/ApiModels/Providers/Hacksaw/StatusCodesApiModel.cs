﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
0 	Success 	No
1 	General/Server error 	Yes
2 	Invalid user / token expired 	No
3 	Invalid currency for user 	No
4 	Invalid partner code 	No
5 	Insufficient funds to place bet 	No
6 	Account locked 	No
7 	Account disabled 	No
8 	Gambling limit exceeded (Loss limit or betting limit) 	No
9 	Time limit exceeded 	No
10 	Session timeout or invalid session id 	No
 */

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public enum StatusCodes
    {
        Success,
        General,
        InvalidUser,
        InvalidCurrency,
        InvalidPartner,
        InsufficientFunds,
        AccountLocked,
        AccountDisabled,
        GamblingLimitExceeded,
        TimeLimitExceeded,
        SessionTimeout
    }
}