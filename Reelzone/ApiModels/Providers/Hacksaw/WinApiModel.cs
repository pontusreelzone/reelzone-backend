﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
 {
    "action":"Win",
    "secret":"your_secret",
    "externalPlayerId":"3",
    "amount":2000,
    "currency":"EUR",
    "roundId":13,
    "gameId":1,
    "gameSessionId":12,
    "externalSessionId":"",
    "transactionId":211134775,
    "type":"free",
    "freeRoundData":{
        "freeRoundActivationId":197,
        "campaignId":null,
        "offerId":"bb6dc603-2ba9-c128-4a12-2e02deb16f52",
        "freeRoundsRemaining": 2
    },
    "jackpotAmount": 0
}
 */

namespace Reelzone.API.ApiModels.Providers.Hacksaw
{
    public class WinApiModel : BaseApiModel
    {
        [JsonPropertyName("amount")]
        public Int64 Amount { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("roundId")]
        public string RoundId { get; set; }

        [JsonPropertyName("gameSessionId")]
        public int GameSessionId { get; set; }

        [JsonPropertyName("transactionId")]
        public int TransactionId { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("jackpotAmount")]
        public int JackpotAmount { get; set; }
    }
}