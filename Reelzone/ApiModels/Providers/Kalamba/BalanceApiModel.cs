﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Kalamba
{
    public class BalanceApiModel
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("user_id")]
        public string UserId { get; set; }

        [JsonPropertyName("game_id")]
        public string GameId { get; set; }
    }
}