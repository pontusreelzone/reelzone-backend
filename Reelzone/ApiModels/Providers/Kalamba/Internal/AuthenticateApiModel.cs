﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Kalamba.Internal
{
    public class _AuthenticateApiModel
    {
        [JsonPropertyName("GameId")]
        public string GameId { get; set; }

        [AllowNull]
        [JsonPropertyName("TournamentId")]
        public Guid TournamentId { get; set; }

        [AllowNull]
        [JsonPropertyName("Platform")]
        public string Platform { get; set; }
    }
}