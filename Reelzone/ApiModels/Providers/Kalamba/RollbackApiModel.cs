﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Kalamba
{
    /*
     {
      "transaction_id": "16d2dcfe-b89e-11e7-854a-58404eea6d16",
      "user_id": "john12345",
      "token": "55b7518e-b89e-11e7-81be-58404eea6d16",
      "reference_transaction_id": "16d2dcfe-b89e-11e7-854a-58404eea6d16",
      "game_id": "hongbao",
      "free_round_id": "rNEMwgzJAOZ6eR3V",
      "round_id": "rNEMwgzJAOZ6eR3V",
      "is_in_free_round": false
    }
    */

    public class RollbackApiModel
    {
        [JsonPropertyName("transaction_id")]
        public string TransactionId { get; set; }

        [JsonPropertyName("user_id")]
        public string UserId { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("reference_transaction_id")]
        public string ReferenceTranscationId { get; set; }

        [JsonPropertyName("game_id")]
        public string GameId { get; set; }

        [JsonPropertyName("free_round_id")]
        public string FreeRoundId { get; set; }

        [JsonPropertyName("round_id")]
        public string RoundId { get; set; }

        [JsonPropertyName("is_in_free_round")]
        public string isInFreeRound { get; set; }
    }
}