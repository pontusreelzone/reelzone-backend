﻿using Reelzone.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Kalamba
{
    /*
     {
      "free_round_id": "rNEMwgzJAOZ6eR3V",
      "transaction_id": "16d2dcfe-b89e-11e7-854a-58404eea6d16",
      "round_id": "rNEMwgzJAOZ6eR3V",
      "token": "55b7518e-b89e-11e7-81be-58404eea6d16",
      "user_id": "john12345",
      "reference_transaction_id": "16d2dcfe-b89e-11e7-854a-58404eea6d16",
      "is_in_free_round": false,
      "is_round_ended": true,
      "game_id": "hongbao",
      "currency": "USD",
      "amount": 15.8,
      "free_round_ended": false
     }
    */

    public class WinApiModel
    {
        [JsonPropertyName("free_round_id")]
        public string FreeRoundId { get; set; }

        [JsonPropertyName("transaction_id")]
        public string TransactionId { get; set; }

        [JsonPropertyName("round_id")]
        public string RoundId { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("user_id")]
        public string UserId { get; set; }

        [JsonPropertyName("reference_transaction_id")]
        public string ReferenceTranscationId { get; set; }

        [JsonPropertyName("is_in_free_round")]
        public bool isInFreeRound { get; set; }

        [JsonPropertyName("is_round_ended")]
        public bool isRoundEnded { get; set; }

        [JsonPropertyName("game_id")]
        public string GameId { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }

        [JsonPropertyName("amount")]
        public string Amount { get; set; }

        [JsonPropertyName("free_round_ended")]
        public bool FreeRoundEnded { get; set; }
    }
}