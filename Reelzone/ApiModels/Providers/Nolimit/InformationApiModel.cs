﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class InformationApiModel
    {
        [JsonPropertyName("uniqueReference")]
        public string UniqueReference { get; set; }

        [JsonPropertyName("gameRoundId")]
        public string GameRoundId { get; set; }

        [JsonPropertyName("game")]
        public string Game { get; set; }

        [JsonPropertyName("time")]
        public string Time { get; set; }
    }
}