﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

/**
 *
{
    "action": "Balance",
    "secret": "your_secret"
}
 */

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class InitialApiModel
    {
        [JsonPropertyName("jsonrpc")]
        public string JsonRPC { get; set; }

        [JsonPropertyName("method")]
        public string Method { get; set; }

        [JsonPropertyName("params")]
        public ParamsApiModel Params { get; set; }

        [JsonPropertyName("id")]
        public string ID { get; set; }
    }
}