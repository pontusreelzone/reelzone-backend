﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class ParamsApiModel
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("game")]
        public string Game { get; set; }

        [JsonPropertyName("userId")]
        public string UserId { get; set; }

        [JsonPropertyName("deposit")]
        public DepositApiModel Deposit { get; set; }

        [JsonPropertyName("withdraw")]
        public DepositApiModel Withdraw { get; set; }

        [JsonPropertyName("identification")]
        public IdentificationApiModel Identification { get; set; }

        [JsonPropertyName("information")]
        public InformationApiModel Information { get; set; }
    }
}