﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class BalanceResponseApiModel
    {
        [JsonPropertyName("amount")]
        public Int64 Amount { get; set; }

        [JsonPropertyName("currency")]
        public string Currency { get; set; }
    }
}