﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class ResultResponseApiModel
    {
        [JsonPropertyName("userId")]
        public string UserId { get; set; }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("balance")]
        public BalanceResponseApiModel Balance { get; set; }

        [JsonPropertyName("transactionId")]
        public string TransactionId { get; set; }
    }
}