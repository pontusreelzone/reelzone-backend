﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels.Providers.Nolimit
{
    public class ValidateTokenResponseModel
    {
        [JsonPropertyName("jsonrpc")]
        public string JsonRPC { get; set; }

        [JsonPropertyName("result")]
        public ResultResponseApiModel Result { get; set; }

        [JsonPropertyName("id")]
        public string ID { get; set; }
    }
}