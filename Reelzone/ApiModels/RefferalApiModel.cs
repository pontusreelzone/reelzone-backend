﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class RefferalApiModel
    {
        public string ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long Amount { get; set; }
    }
}
