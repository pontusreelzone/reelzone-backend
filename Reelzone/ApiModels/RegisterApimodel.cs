﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Reelzone.ApiModels
{
    public class RegisterApimodel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        //Regex, Min 8 char, 1 digit, 1 special, 1 uppercase
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{4,15}$", ErrorMessage = "Not a valid password")]
        public string Password { get; set; }

        [Required]
        //Regex, Min 8 char, 1 digit, 1 special, 1 uppercase
        [RegularExpression(@"^(?=[a-zA-Z0-9._]{4,20}$)(?!.*[_.]{2})[^_.].*[^_.]$", ErrorMessage = "Not a valid username")]
        public string Username { get; set; }

        [Required]
        public string Identifier { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public Guid Country { get; set; }

        public string Refferal { get; set; }
    }
}