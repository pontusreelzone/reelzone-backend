﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Reelzone.ApiModels
{
    public class RegisterOauthApiModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Identifier { get; set; }

        [Required]
        public Guid Country { get; set; }
    }
}