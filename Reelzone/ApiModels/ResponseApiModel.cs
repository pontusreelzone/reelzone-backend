﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class ResponseApiModel
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
        public List<Object> Response {get; set;}
    }
}
