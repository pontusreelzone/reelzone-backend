﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class TournamentApiModel
    {
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Retryable { get; set; }
        public string HeaderImage { get; set; }
        [Required]        
        public TournamentFormat Format { get; set; }
        [Required]
        public int StartSpins { get; set; }
        [Required]
        public DateTime StartDOT { get; set; }
        [Required]
        public DateTime EndDOT { get; set; }
        [Required]
        public List<TournamentPrizepoolApiModel> Prizepool { get; set; }
        [Required]
        public string Game { get; set; }
        public bool Exclusive { get; set; }


    }
}
