﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class TournamentPrizepoolApiModel
    {
        public string ID { get; set; }

        [Required]
        public int Prize { get; set; }

        [Required]
        public string Position { get; set; }
        [Required]

        public bool CoinPrize { get; set; }

    }
}
