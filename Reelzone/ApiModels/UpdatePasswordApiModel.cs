﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Reelzone.ApiModels
{
    public class UpdatePasswordApiModel
    {
        //Regex, Min 8 char, 1 digit, 1 special, 1 uppercase
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{4,15}$", ErrorMessage = "Not a valid password")]
        public string Password { get; set; }

        [Required]
        public string Identifier { get; set; }
    }
}