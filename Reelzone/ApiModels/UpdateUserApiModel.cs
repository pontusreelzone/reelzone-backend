﻿using Reelzone.API.ApiModels.ApiEnums;
using Reelzone.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reelzone.API.ApiModels
{
    public class UpdateUserApiModel
    {
        [Required]
        [EmailAddress]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",
         ErrorMessage = "Not an email")]
        public string Email { get; set; }

        public bool Admin { get; set; }
        public Status Status { get; set; }
        public string Iban { get; set; }
        public string Swift { get; set; }
        public string Identifier { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public PayOutApi PayOutOptions { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string AvatarImage { get; set; }

        [Required]
        public string HeaderImage { get; set; }

        //Regex, Min 8 char, 1 digit, 1 special, 1 uppercase
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{4,15}$", ErrorMessage = "Not a valid password")]
        public string Password { get; set; }

        public string Country { get; set; }
    }
}