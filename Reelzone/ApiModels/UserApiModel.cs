﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace Reelzone.ApiModels
{
    public class UserApiModel
    {
        public Guid ID { get; set; }
        public IEnumerable<Claim> Claims { get; set; }
        public string Name { get; set; }
        public string AvatarImage { get; set; }
        public string HeaderImage { get; set; }
        [EmailAddress]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",
         ErrorMessage = "Not an email")]
        public string Email { get; set; }
        public string Identifier { get; set; }
        public string BirthDay { get; set; }

        public ProviderType Provider { get; set; }
    }
}