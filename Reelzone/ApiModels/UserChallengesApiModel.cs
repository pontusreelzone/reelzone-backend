﻿using Reelzone.Core.Dtos;
using System.Collections.Generic;

namespace Reelzone.ApiModels
{
    public class UserChallengesApiModel
    {
        public List<UserAchievementDto> Current { get; set; }
        public List<UserAchievementDto> Completed { get; set; }
    }
}