﻿namespace Reelzone.ApiModels
{
    public class UserOauthProviderApiModel
    {
        public int ID { get; set; }
        public string Identifier { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}