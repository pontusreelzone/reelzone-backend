﻿using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace Reelzone.ApiModels
{
    public class WalletTokenApiModel
    {
        public Guid ID { get; set; }
        public string Token { get; set; }
        public string TournamentId { get; set; }
        public DateTime Created { get; set; }
    }
}