﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.API.ApiModels.Providers.Hacksaw.Dashboard;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using RestSharp;

namespace Reelzone.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Policy = "Admin")]
    public class AdminController : ControllerBase
    {
        public IUserService UserService;
        public IUserActivityService UserActivityService;
        public IUserExperienceService UserExperienceService;
        public ITournamentService TournamentService;
        public IGameService GameService;
        public IGameProviderService GameProvicerService;
        public IAchievementService AchievementService;
        public IGameTransactionService GameTransactionService;
        public IRefferalService RefferalService;
        public ICountryService CountryService;
        public IUserNotificationService UserNotificationService;



        public AdminController(ITournamentService tournamentService, IUserService userService,
                               IUserActivityService userActivityService, IUserExperienceService userExperienceService,
                               IGameService gameService, IGameProviderService gameProvicerService,
                               IAchievementService achievementService, IGameTransactionService gameTransactionService,
                               IRefferalService refferalService, ICountryService countryService,
                               IUserNotificationService userNotificationService)
        {
            GameProvicerService = gameProvicerService;
            GameService = gameService;
            UserService = userService;
            UserActivityService = userActivityService;
            UserExperienceService = userExperienceService;
            TournamentService = tournamentService;
            AchievementService = achievementService;
            GameTransactionService = gameTransactionService;
            RefferalService = refferalService;
            CountryService = countryService;
            UserNotificationService = userNotificationService;
        }

        #region DASHBOARD
        [Route("dashboard/{LastMonth}")]
        [HttpGet]
        public ActionResult Dashboard(bool LastMonth)
        {
            Dashboard atsp = new Dashboard()
            {
                Attendees = TournamentService.CountAttendees(LastMonth),
                Spins = TournamentService.CountSpins(LastMonth),
                Users = UserService.CountUsers(LastMonth),
                Tournaments = TournamentService.DashBoardTournaments()
            };

            return Ok(JsonConvert.SerializeObject(atsp, Formatting.Indented));
        }
        [Route("dashboard/countries")]
        [HttpGet]
        public ActionResult DashboardCountries()
        {
            var data = CountryService.GetDashboardCoutryData(); 

            return Ok(JsonConvert.SerializeObject(data, Formatting.Indented));
        }
        [AllowAnonymous]
        [Authorize(Policy = "Provider")]
        [Route("dashboard/provider")]
        [HttpGet]
        public ActionResult DashboardProvider()
        {
            var userDto = UserService
                                    .GetUser(
                                    User
                                    .Claims
                                    .FirstOrDefault
                                     (c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                                    .Value);



            Dashboard atsp = new Dashboard()
            {
                Attendees = TournamentService.FilteredByProviderCountAttendees(userDto.GameProvider.ID.ToString(), null, null),
                Spins = TournamentService.FilteredByProviderCountSpins(userDto.GameProvider.ID.ToString(), null, null),
                Tournaments = TournamentService.DashBoardProviderTournaments(userDto.GameProvider.ID)
            };

            return Ok(JsonConvert.SerializeObject(atsp, Formatting.Indented));
        }


        [Route("dashboard")]
        [HttpPost]
        public ActionResult FilteredAttendeesCountAndSpins([FromBody] FilterDashboardApiModel dashboard)
        {
            //nothing is selected
            if (dashboard.Game == "" && dashboard.Start == null && dashboard.End == null && dashboard.Provider == "")
            {
                var backend = ConfigurationManager.AppSettings.Get("Reelzone.Backend.Url");
                return RedirectPermanent(backend + "/admin/dashboard");
            }
            var atsp = new Dashboard();
            //Filter by provider
            if (dashboard.Provider != "" && dashboard.Game == "")
            {
                try
                {
                    atsp.Attendees = TournamentService.FilteredByProviderCountAttendees(dashboard.Provider, dashboard.Start, dashboard.End);
                    atsp.Spins = TournamentService.FilteredByProviderCountSpins(dashboard.Provider, dashboard.Start, dashboard.End);
                    atsp.Users = UserService.FillterCountUsers(dashboard.Start, dashboard.End);
                }
                catch (Exception e)
                {
                    return BadRequest(e);
                }
            }
            //filter by game
            if (dashboard.Game != "")
            {
                try
                {
                    atsp.Attendees = TournamentService.FilteredByGameCountAttendees(dashboard.Game, dashboard.Start, dashboard.End);
                    atsp.Spins = TournamentService.FilteredByGameCountSpins(dashboard.Game, dashboard.Start, dashboard.End);
                    atsp.Users = UserService.FillterCountUsers(dashboard.Start, dashboard.End);
                }
                catch (Exception e)
                {
                    return BadRequest(e);
                }
            }
            //filter without selected game or provider
            else
            {
                try
                {
                    atsp.Attendees = TournamentService.FilteredCountAttendees(dashboard.Start, dashboard.End);
                    atsp.Spins = TournamentService.FilteredCountSpins(dashboard.Start, dashboard.End);
                    atsp.Users = UserService.FillterCountUsers(dashboard.Start, dashboard.End);
                }
                catch (Exception e)
                {
                    return BadRequest(e);
                }
            }


            return Ok(JsonConvert.SerializeObject(atsp, Formatting.Indented));
        }
        [Route("affiliate")]
        [HttpPost]
        public ActionResult AffiliateStats()
        {

            RestClient client;

            var currentMonth = DateTime.UtcNow.Month;
            var year = 2020;
            var currentYear = DateTime.UtcNow.Year - year;
            var month = 8;
            if (currentYear >= 0)
            {
                if (currentMonth - month >= 0)
                {
                    currentMonth = currentMonth + currentYear * 12;
                }
                else
                {
                    currentYear = currentYear - 1;
                    currentMonth = currentMonth + 12 - month + currentYear * 12;
                }
            }

            List<MathcingVisionsDashboardApiModel> data = new List<MathcingVisionsDashboardApiModel>();

            for (int i = 7; i < currentMonth; i++)
            {
                if (month == 13)
                {
                    month = 1;
                    year++;
                }

                client = new RestClient("https://matchingvisions.com/publisher/export/feed_json.php?token=ec884aa6067e61a25d9c6137059d53b1&month="
                                        + month.ToString().PadLeft(2, '0') + "&year="
                                        + year + "");
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("cache-control", "no-cache");

                IRestResponse response = client.Execute(request);

                var item = (JsonConvert.
                                    DeserializeObject
                                    <MathcingVisionsDashboardApiModel>
                                    (response.Content));
                item.Month = month;
                item.Year = year;
                data.Add(item);
                month++;
            }

            return Ok(JsonConvert.SerializeObject(data, Formatting.Indented));
        }
        #endregion
        #region USER
        [Route("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Request.HttpContext.SignOutAsync();
            return Ok();
        }

        [HttpPost]
        [Route("signin")]
        [AllowAnonymous]
        public async Task<IActionResult> SignIn(LoginApiModel model)
        {
            var response = new ResponseApiModel();

            if (ModelState.IsValid)
            {
                try
                {
                    var result = await UserService.SignInAsync(model.Email, model.Password, HttpContext);
                    if (result == true)
                    {
                        response.Successful = true;
                        return Ok(response);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception e)
                {
                    var responce = new ResponseApiModel()
                    {
                        Message = e.Message,
                        Successful = false
                    };
                    return BadRequest(responce);
                }
            }
            ModelState.AddModelError("", "Invalid login attempt");

            response.Successful = false;

            return Ok(response);
        }
        [Route("users/{howMany}/{page}")]
        [HttpGet]
        public ActionResult GetLimitedUsers(int howMany, int page)
        {
            var users = UserService.GetLimitedUsers(howMany, page);
            if (users == null)
                return BadRequest();
            return Ok(JsonConvert.SerializeObject(users, Formatting.Indented));
        }

        [Route("users/filter/{pageSize}/{currentPage}")]
        [HttpPost]
        public ActionResult GetSearchUsers([FromBody] FilterUserDto filters, int pageSize, int currentPage)
        {
            var users = UserService.SearchUsers(pageSize, currentPage, filters);

            return Ok(JsonConvert.SerializeObject(users, Formatting.Indented));
        }

        [Route("user/{id}")]
        [HttpDelete]
        public ActionResult DeleteUser(string id)
        {

            try
            {
                var user = UserService.DeleteUser(new Guid(id));
                if (user == false)
                {
                    return BadRequest();
                }
                return Ok();
            }
            catch (FormatException e)
            {
                return BadRequest(e);
            }


        }


        [Route("usernote/{id}")]
        [HttpPost]
        public ActionResult UpdateUserNote(string id, [FromBody] ApiModelNote note)
        {
            try
            {
                var user = UserService.UpdateNote(new Guid(id), note.Note);

                return Ok(JsonConvert.SerializeObject(user, Formatting.Indented));

            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("user/{id}")]
        [HttpPut]
        public ActionResult UpdateUser([FromBody] AdminUpdateUserApiModel user, string id)
        {
            try
            {
                var guid = new Guid(id);
                var userDto = new UserDto()
                {
                    ID = guid,
                    BirthDate = user.BirthDate,
                    Coins = user.Coins,
                    Email = user.Email,
                    Status = user.Status,
                    PerformKYCProcess = user.PerformKYC,
                    KycVerification = user.KycVerification,
                    AvatarImage = user.AvatarImage,
                    Username = user.Username,
                    Role = user.Role,
                    GameProvider = null

                };
                if (user.GameProvider!="")
                {
                    var provider = GameProvicerService.GetProvider(new Guid(user.GameProvider));

                    userDto.GameProvider = provider;
                }
                var updatedUser = UserService.UpdateUser(userDto, user.Country);
                if (updatedUser != null)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [Route("user/gametransactions")]
        [HttpPost]
        public ActionResult UserGameTransactions([FromBody] FillterUserGameTransactionsApiModel apiModel)
        {
            try
            {
                var response = GameTransactionService.FilteredGameTransaction(apiModel.Start, apiModel.End, apiModel.User);
                return Ok(JsonConvert.SerializeObject(response, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }
        #endregion
        #region User Notifications
        [Route("notifications/{user}/{viewed}")]
        [HttpGet]
        public ActionResult AllUserNotifications(string user,string viewed)
        {
            try
            {
                if (user == "")
                {
                    return BadRequest("Please select user");
                }
                if (viewed=="all")
                {
                    var notificationsNoFilter = UserNotificationService.GetUserNotifications(new Guid(user));
                    return Ok(JsonConvert.SerializeObject(notificationsNoFilter, Formatting.Indented));
                }
                var viewedBool = false;
                if (viewed=="true")
                {
                    viewedBool = true;
                }
               
                var notifications = UserNotificationService.GetUserNotifications(new Guid(user), viewedBool);
                return Ok(JsonConvert.SerializeObject(notifications, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
            
        }

        [Route("notification")]
        [HttpPost]
        public ActionResult AddCountry([FromBody] UserNotificationApiModel notification)
        {
            var userNotificationDto = new UserNotificationDto()
            {
                Title = notification.Title,
                Description = notification.Description,
                User = notification.UserID,
                Link = notification.Link,                
            };
            try
            {
                var createdNotification = UserNotificationService.AddUserNotification(userNotificationDto);
                return Created("", JsonConvert.SerializeObject(createdNotification, Formatting.Indented));

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [Route("notification/{id}")]
        [HttpPut]
        public ActionResult UpdateNotification([FromBody] UserNotificationApiModel notification, string id)
        {
            var userNotificationDto = new UserNotificationDto()
            {
                Title = notification.Title,
                Description = notification.Description,
                User = notification.UserID,
                Link = notification.Link,
                Viewed = notification.Viewed,
            };
            try
            {
                userNotificationDto.ID = new Guid(id);
                var createdNotification = UserNotificationService.UpdatUserNotification(userNotificationDto);
                return Ok(JsonConvert.SerializeObject(createdNotification, Formatting.Indented));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }

        [Route("notification/{id}")]
        [HttpDelete]
        public ActionResult DeleteNotification(string id)
        {
            try
            {
                UserNotificationService.DeleteUserNotification(new Guid(id));
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        #endregion
        #region TOURNAMENTS
        [Route("tournaments/all")]
        [HttpGet]
        public ActionResult All()
        {
            var tournaments = TournamentService.GetAllTournaments();
            if (tournaments == null)
            {
                return BadRequest("No tournaments");
            }
            return Ok(JsonConvert.SerializeObject(tournaments, Formatting.Indented));
        }

        [Route("/tournaments/{id}")]
        [HttpGet]
        public ActionResult GetTournament(Guid ID)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournament = TournamentService.GetTournament(ID, identifier);
            if (tournament == null)
            {
                return BadRequest();
            }
            return Ok(JsonConvert.SerializeObject(tournament, Formatting.Indented));
        }
        [Route("tournaments")]
        [HttpPost]
        public ActionResult CreateTornament([FromBody] TournamentApiModel tournament)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            List<TournamentPrizepoolDto> prizepools = new List<TournamentPrizepoolDto>();
            foreach (var prizepool in tournament.Prizepool)
            {
                prizepools.Add(new TournamentPrizepoolDto()
                {
                    Position = prizepool.Position,
                    Prize = prizepool.Prize,
                    CoinPrize = prizepool.CoinPrize
                });
            }

            var tournamentDto = new TournamentDto()
            {
                Name = tournament.Name,
                Retryable = tournament.Retryable,
                Prizepool = prizepools,
                HeaderImage = tournament.HeaderImage,
                Description = tournament.Description,
                EndDOT = tournament.EndDOT,
                StartDOT = tournament.StartDOT,
                StartSpins = tournament.StartSpins,
                Format = tournament.Format,
                Exclusive = tournament.Exclusive

            };

            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            try
            {
                var createdTournament = TournamentService.CreateTournament(tournamentDto, identifier, tournament.Game);

                return Created("", JsonConvert.SerializeObject(createdTournament, Formatting.Indented));
            }
            catch (Exception e)
            {
                var responce = new ResponseApiModel()
                {
                    Message = e.Message,
                    Successful = false
                };
                return BadRequest(responce);
            }
        }

        [Route("/tournaments/{id}")]
        [HttpPut]
        public ActionResult UpdateTornament([FromBody] TournamentApiModel tournament, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var guid = Guid.NewGuid();
            List<TournamentPrizepoolDto> prizepools = new List<TournamentPrizepoolDto>();
            foreach (var prizepool in tournament.Prizepool)
            {
                if (prizepool.ID != null)
                {
                    try
                    {
                        guid = Guid.Parse(prizepool.ID);
                    }
                    catch (FormatException)
                    {
                        return BadRequest();
                    }
                }
                prizepools.Add(new TournamentPrizepoolDto()
                {
                    ID = guid,
                    Position = prizepool.Position,
                    Prize = prizepool.Prize,
                    CoinPrize = prizepool.CoinPrize
                });
            }
            try
            {
                guid = Guid.Parse(id);
            }
            catch (FormatException)
            {
                var responce = new ResponseApiModel()
                {
                    Message = "If you just created the turnament and you want to update it please refresh the page first ",
                    Successful = false
                };
                return BadRequest(responce);
            }

            var tournamentDto = new TournamentDto()
            {
                ID = guid,
                Name = tournament.Name,
                Prizepool = prizepools,
                Retryable = tournament.Retryable,
                HeaderImage = tournament.HeaderImage,
                Description = tournament.Description,
                EndDOT = tournament.EndDOT,
                StartDOT = tournament.StartDOT,
                StartSpins = tournament.StartSpins,
                Format = tournament.Format,
                Exclusive = tournament.Exclusive

            };

            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;

            try
            {
                var createdTournament = TournamentService.Update(tournamentDto, identifier, tournament.Game);

                return Ok();
            }
            catch (Exception e)
            {
                var responce = new ResponseApiModel()
                {
                    Message = e.Message,
                    Successful = false
                };
                return BadRequest(responce);
            }
        }

        [Route("/tournaments/{id}")]
        [HttpDelete]
        public ActionResult DeleteTournament(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var guid = Guid.NewGuid();

            try
            {
                guid = Guid.Parse(id);
            }
            catch (FormatException)
            {
                return BadRequest();
            }

            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournament = TournamentService.DeleteTournament(guid, identifier);
            if (tournament == false)
            {
                return BadRequest();
            }
            return Ok();
        }
        #endregion
        #region GAMES
        [Route("games")]
        [HttpGet]
        public ActionResult AllGames()
        {
            var games = GameService.AllGames();
            if (games == null)
            {
                return BadRequest("No games");
            }
            return Ok(JsonConvert.SerializeObject(games, Formatting.Indented));
        }

        [Route("games")]
        [HttpPost]
        public ActionResult AddGame([FromBody] GameApiModel game)
        {
            var gameDto = new GameDto()
            {
                Name = game.Name,
                BackgroundImage = game.BackgroundImage,
                CardBackgroundImage = game.CardBackgroundImage,
                Description = game.Description,
                ExternalGameId = game.ExternalGameId,
                HeaderImage = game.HeaderImage,
                LogoImage = game.LogoImage,
                URL = game.URL,
                Visible = game.Visible
            };
            var GameProvider = new GameProviderDto();
            try
            {
                GameProvider.ID = new Guid(game.GameProviderID);
            }
            catch (Exception)
            {
                return BadRequest("Failed to create the game");
            }
            gameDto.GameProvider = GameProvider;
            var createdGame = GameService.AddGame(gameDto);
            if (createdGame == null)
            {
                return BadRequest("Failed to create the game");
            }
            return Created("", JsonConvert.SerializeObject(createdGame, Formatting.Indented));
        }

        [Route("games/{id}")]
        [HttpPut]
        public ActionResult UpdateGame([FromBody] GameApiModel game, string id)
        {
            var gameDto = new GameDto()
            {
                Name = game.Name,
                BackgroundImage = game.BackgroundImage,
                CardBackgroundImage = game.CardBackgroundImage,
                Description = game.Description,
                ExternalGameId = game.ExternalGameId,
                HeaderImage = game.HeaderImage,
                LogoImage = game.LogoImage,
                URL = game.URL,
                Visible = game.Visible
            };
            var GameProvider = new GameProviderDto();
            try
            {
                GameProvider.ID = new Guid(game.GameProviderID);
                gameDto.ID = new Guid(id);
            }
            catch (Exception)
            {
                return BadRequest("Failed to create the game");
            }
            gameDto.GameProvider = GameProvider;
            var updatedGame = GameService.UpdateGame(gameDto);
            if (updatedGame == null)
            {
                return BadRequest("Failed to update the game");
            }
            return Ok(JsonConvert.SerializeObject(updatedGame, Formatting.Indented));
        }

        [Route("games/{id}")]
        [HttpDelete]
        public ActionResult DeleteGame(string id)
        {
            try
            {
                var games = GameService.DeleteGame(new Guid(id));
                if (games == false)
                {
                    return BadRequest("Failed");
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("games/{id}")]
        [HttpGet]
        public ActionResult GetGame(string id)
        {
            try
            {
                var games = GameService.GetGame(new Guid(id));
                if (games == null)
                {
                    return BadRequest("Failed");
                }
                return Ok(JsonConvert.SerializeObject(games, Formatting.Indented));
            }
            catch (Exception)
            {
                return BadRequest("Failed");
            }
        }
        #endregion
        #region PROVIDERS
        [Route("providers")]
        [HttpGet]
        public ActionResult AllProviders()
        {
            var providers = GameProvicerService.GetProviders();
            if (providers == null)
            {
                return BadRequest("No providers");
            }
            return Ok(JsonConvert.SerializeObject(providers, Formatting.Indented));
        }

        [Route("providers")]
        [HttpPost]
        public ActionResult AddProvider([FromBody] GameProviderApiModel provider)
        {
            var gameProviderDto = new GameProviderDto()
            {
                Image = provider.Image,
                Description = provider.Description,
                IntegrationInitScript = provider.IntegrationInitScript,
                IntegrationConfig = provider.IntegrationConfig,
                IntegrationType = provider.IntegrationType,
                IntegrationUrl = provider.IntegrationUrl,
                Name = provider.Name,
            };

            var createdProvider = GameProvicerService.AddGameProvider(gameProviderDto);
            if (createdProvider == null)
            {
                return BadRequest("Failed to create the game");
            }
            return Created("", JsonConvert.SerializeObject(createdProvider, Formatting.Indented));
        }

        [Route("providers/{id}")]
        [HttpPut]
        public ActionResult UpdateProvider([FromBody] GameProviderApiModel provider, string id)
        {
            var gameProviderDto = new GameProviderDto()
            {
                Image = provider.Image,
                Description = provider.Description,
                IntegrationInitScript = provider.IntegrationInitScript,
                IntegrationConfig = provider.IntegrationConfig,
                IntegrationType = provider.IntegrationType,
                IntegrationUrl = provider.IntegrationUrl,
                Name = provider.Name,
            };
            try
            {
                gameProviderDto.ID = new Guid(id);
            }
            catch (Exception)
            {
                return BadRequest("Failed to create the provider");
            }
            var createdProvider = GameProvicerService.UpdateGameProvider(gameProviderDto);
            if (createdProvider == null)
            {
                return BadRequest("Failed to update the provider");
            }
            return Ok(JsonConvert.SerializeObject(createdProvider, Formatting.Indented));
        }

        [Route("providers/{id}")]
        [HttpDelete]
        public ActionResult DeleteProvider(string id)
        {
            try
            {
                var provider = GameProvicerService.DeleteProvider(new Guid(id));
                if (provider == false)
                {
                    return BadRequest("Failed");
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("providers/{id}")]
        [HttpGet]
        public ActionResult GetProvider(string id)
        {
            try
            {
                var provider = GameProvicerService.GetProvider(new Guid(id));
                if (provider == null)
                {
                    return BadRequest("Failed");
                }
                return Ok(JsonConvert.SerializeObject(provider, Formatting.Indented));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion
        #region ACHIEVEMENTS
        [Route("achievements")]
        [HttpGet]
        public ActionResult AllAchievements()
        {
            try
            {
                var achievements = AchievementService.GetAllAchievements();
                if (achievements == null)
                {
                    return BadRequest("No achivements");
                }
                return Ok(JsonConvert.SerializeObject(achievements, Formatting.Indented));

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
           
        }

        [Route("achievements")]
        [HttpPost]
        public ActionResult AddAchivements([FromBody] AchivementsApiModel achievement)
        {
            var hours = Int32.Parse(achievement.TimeThreshold.Split(":")[0]);
            var minutes = Int32.Parse(achievement.TimeThreshold.Split(":")[1]);

            var seconds = Int32.Parse(achievement.TimeThreshold.Split(":")[2]);

            var ts = new TimeSpan(hours, minutes, seconds);
            var achievementDto = new AchievementDto()
            {
                Name = achievement.Name,
                Amount = achievement.Amount,
                BadgeImage = achievement.BadgeImage,
                Description = achievement.Description,
                LevelThreshold = achievement.LevelThreshold,
                RewardAmount = achievement.RewardAmount,
                Enabled = achievement.Enabled,
                Period = achievement.Period,
                TimeThreshold = ts,
                RewardType = achievement.RewardType,
                Visible = achievement.Visible,
            };
            achievementDto.Activity = achievement.Activity;
            try
            {
                var createdProvider = AchievementService.CreateAchievement(achievementDto);

                return Created("", JsonConvert.SerializeObject(createdProvider, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }





        [Route("achievements/{id}")]
        [HttpPut]
        public ActionResult UpdateAchivement([FromBody] AchivementsApiModel achievement, Guid id)
        {
            var hours = Int32.Parse(achievement.TimeThreshold.Split(":")[0]);
            var minutes = Int32.Parse(achievement.TimeThreshold.Split(":")[1]);

            var seconds = Int32.Parse(achievement.TimeThreshold.Split(":")[2]);

            var ts = new TimeSpan(hours, minutes, seconds);


            var achievementDto = new AchievementDto()
            {
                ID = id,
                Name = achievement.Name,
                Amount = achievement.Amount,
                BadgeImage = achievement.BadgeImage,
                Description = achievement.Description,
                LevelThreshold = achievement.LevelThreshold,
                RewardAmount = achievement.RewardAmount,
                Enabled = achievement.Enabled,
                Period = achievement.Period,
                TimeThreshold = ts,
                RewardType = achievement.RewardType,
                Visible = achievement.Visible,
            };
            achievementDto.Activity = achievement.Activity;
            try
            {
                var createdProvider = AchievementService.UpdateAchievement(achievementDto);

                return Created("", JsonConvert.SerializeObject(createdProvider, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }





        [Route("achievements/{id}")]
        [HttpDelete]
        public ActionResult DeleteAchievement(Guid id)
        {
            try
            {
                var achievement = AchievementService.DeleteAchievement(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("achievements/{id}")]
        [HttpGet]
        public ActionResult GetAchievement(string id)
        {
            try
            {
                var achievement = AchievementService.GetAchievement(new Guid(id));

                return Ok(JsonConvert.SerializeObject(achievement, Formatting.Indented));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("activities")]
        [HttpGet]
        public ActionResult AllActivities()
        {
            var achievements = AchievementService.GetAllActivities();
            if (achievements == null)
            {
                return BadRequest("No activities");
            }
            return Ok(JsonConvert.SerializeObject(achievements, Formatting.Indented));
        }

        #endregion
        #region REFERALS
        [Route("refferals/{page}")]
        [HttpGet]
        public ActionResult AllRefferals(int page)
        {
            var refferals = RefferalService.GetRefferals(page);

            return Ok(JsonConvert.SerializeObject(refferals, Formatting.Indented));
        }
        [Route("refferals/filter/{code}")]
        [HttpGet]
        public ActionResult GetFillteredRefferals(string code)
        {
            var refferals = RefferalService.GetFillteredRefferals(code);

            return Ok(JsonConvert.SerializeObject(refferals, Formatting.Indented));
        }

        [Route("refferals")]
        [HttpPost]
        public ActionResult AddRefferal([FromBody] RefferalApiModel refferal)
        {

            var refferalDto = new RefferalDto()
            {
                Amount = refferal.Amount,
                Code = refferal.Code,
                Description = refferal.Description,
            };
            try
            {
                var createdRefferal = RefferalService.CreateRefferal(refferalDto);

                return Created("", JsonConvert.SerializeObject(createdRefferal, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
        [Route("refferals")]
        [HttpPut]
        public ActionResult UpdateRefferal([FromBody] RefferalApiModel refferal)
        {
            try
            {
                var refferalDto = new RefferalDto()
                {
                    ID = new Guid(refferal.ID),
                    Amount = refferal.Amount,
                    Code = refferal.Code,
                    Description = refferal.Description,
                };
                var createdRefferal = RefferalService.UpdateReferal(refferalDto);

                return Created("", JsonConvert.SerializeObject(createdRefferal, Formatting.Indented));
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
        [Route("refferals/{id}")]
        [HttpDelete]
        public ActionResult DeleteRefferal(Guid id)
        {
            try
            {
                RefferalService.DeleteRefferal(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        #endregion
        #region Country
        [Route("Country")]
        [HttpGet]
        public ActionResult AllCountries()
        {
            var countries = CountryService.AllCountries();
            return Ok(JsonConvert.SerializeObject(countries, Formatting.Indented));
        }

        [Route("Country")]
        [HttpPost]
        public ActionResult AddCountry([FromBody] CountryApiModel country)
        {
            var countryDto = new CountryDto()
            {
                Name = country.Name,
                CountryFlag = country.CountryFlag,
                CountryIso = country.CountryIso
            };
            try
            {
                var createdCountry = CountryService.AddCountry(countryDto);
                return Created("", JsonConvert.SerializeObject(createdCountry, Formatting.Indented));

            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

        [Route("Country/{id}")]
        [HttpPut]
        public ActionResult UpdateCountry([FromBody] CountryApiModel country, string id)
        {
            var countryDto = new CountryDto()
            {
                Name = country.Name,
                CountryFlag = country.CountryFlag,
                CountryIso = country.CountryIso
            };
            try
            {
                countryDto.ID = new Guid(id);

                var updatedCountry = CountryService.UpdateCountry(countryDto);
                return Ok(JsonConvert.SerializeObject(updatedCountry, Formatting.Indented));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }

        [Route("Country/{id}")]
        [HttpDelete]
        public ActionResult DeleteCountry(string id)
        {
            try
            {
                var country = CountryService.DeleteCountry(new Guid(id));
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("Country/{id}")]
        [HttpGet]
        public ActionResult GetCountry(string id)
        {
            try
            {
                var country = CountryService.GetCountry(new Guid(id));
                return Ok(JsonConvert.SerializeObject(country, Formatting.Indented));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        #endregion
    }
}