﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reelzone.API.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Reelzone.Core.Entities.Users;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthenticationController : ControllerBase
    {
        public IUserService UserService;

        public AuthenticationController(IUserService userService)
        {
            UserService = userService;
        }

        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> SignIn(LoginApiModel model)
        {
            var response = new ResponseApiModel();

            if (ModelState.IsValid)
            {
                var result = await UserService.SignInAsync(model.Email, model.Password, HttpContext);

                if (result == true)
                {
                    response.Successful = true;

                    return Ok(response);
                }
            }
            ModelState.AddModelError("", "Invalid login attempt");

            response.Successful = false;

            return Ok(response);
        }

        [HttpGet]
        [Route("signin/{provider}")]
        public IActionResult SignInProvider(string provider, string returnUrl = null)
        {
            var challenge = Challenge(new AuthenticationProperties { RedirectUri = returnUrl ?? ConfigurationManager.AppSettings.Get("Reelzone.Backend.Url") + "/authentication/callback/" }, provider);
            return challenge;
        }

        [Route("signout")]
        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }

        [Route("callback")]
        public void Callback()
        {
            if (User.Identity.IsAuthenticated)
            {
                var profile = new UserDto();

                switch (User.Identity.AuthenticationType)
                {
                    case "Twitch":
                        profile = new UserDto
                        {
                            Email = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value,
                            AvatarImage = User.Claims.FirstOrDefault(c => c.Type == "urn:twitch:profileimageurl").Value,
                            Username = User.Identity.Name,
                            Identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value,
                            Provider = ProviderType.Twitch
                        };
                        break;

                    case "Facebook":
                        profile = new UserDto
                        {
                            Email = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value,
                            AvatarImage = "https://graph.facebook.com/" + User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value + "/picture?type=large",
                            Username = User.Identity.Name,
                            Identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value,
                            Provider = ProviderType.Facebook
                        };
                        break;
                }

                if (UserService.UserOauthProviderConnected(profile.Identifier) == true)
                {
                    var createdUser = UserService.CreateUser(profile);
                    Response.Redirect(ConfigurationManager.AppSettings.Get("Reelzone.Frontend.Url") + "/home");
                }
                else
                {
                    var createdUser = UserService.CreateOauthProvider(profile);
                    Response.Redirect("https://play.reelzone.app/register/" + profile.Identifier);
                }
            }
            else
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("Reelzone.Frontend.Url") + "/login");
            }
        }
    }
}