﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.ApiModels;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChallengeController : ControllerBase
    {
        public ITournamentService TournamentService;
        public IUserService UserService;
        private IUserAchievmentService UserAchievementService;

        public ChallengeController(ITournamentService tournamentService, IUserService userService, IUserAchievmentService userAchievementService)
        {
            TournamentService = tournamentService;
            UserService = userService;
            UserAchievementService = userAchievementService;
        }

        [Route("current/{period}")]
        [Authorize]
        public ActionResult Current(AchievementPeriod period)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);

            var UserChallangesApiModel = new UserChallengesApiModel();

            var completed = UserAchievementService.GetUserAchievements(user.ID, period);

            var current = UserAchievementService.GetUserAchievements(user.ID, period);
            if (current.Count() > 0)
            {
                UserChallangesApiModel.Current = current;
            }

            if (completed.Count() > 0)
            {
                UserChallangesApiModel.Completed = completed;
            }

            return Ok(JsonConvert.SerializeObject(UserChallangesApiModel, Formatting.Indented));
        }

        [Route("completed/{userId}")]
        [Authorize]
        public ActionResult Completed(Guid userId)
        {
            var user = UserService.GetUser(userId);

            var completed = UserAchievementService.GetUserAchievements(user.ID);

            return Ok(JsonConvert.SerializeObject(completed, Formatting.Indented));
        }
    }
}