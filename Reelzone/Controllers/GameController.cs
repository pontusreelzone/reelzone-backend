﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;

namespace Reelzone.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        public IUserService UserService;
        public IUserActivityService UserActivityService;
        public IUserExperienceService UserExperienceService;
        public ITournamentService TournamentService;
        public IGameService GameService;
        private IMemoryCache _cache;

        public GameController(IGameService gameService, IMemoryCache memoryCache)
        {
            GameService = gameService;
            _cache = memoryCache;
        }

        [Route("all")]
        [HttpGet]
        public ActionResult AllGames()
        {
            List<Game> games;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.Games, out games))
            {
                // Key not in cache, so get data.
                games = GameService.AllGames();

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromDays(30));

                // Save data in cache.
                _cache.Set(CacheKeys.Winners, games, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(games, Formatting.Indented));
        }

        [Route("providers")]
        [HttpGet]
        public ActionResult AllProviders()
        {
            GameListingApiModel model;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.Providers, out model))
            {
                model = new GameListingApiModel();
                // Key not in cache, so get data.
                model.Providers = GameService.GetGameProviders();
                model.Featured = GameService.GetFeaturedGames();

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromDays(30));

                // Save data in cache.
                _cache.Set(CacheKeys.Providers, model, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(model, Formatting.Indented));
        }

        [Route("leaderboard")]
        [HttpGet]
        public ActionResult Leadeboard()
        {
            List<GameLeaderBoardModel> leadboard;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.GameLeaderboard, out leadboard))
            {
                // Key not in cache, so get data.
                leadboard = GameService.GetLeaderboard();

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(10));

                // Save data in cache.
                _cache.Set(CacheKeys.GameLeaderboard, leadboard, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(leadboard, Formatting.Indented));
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult AllGames(Guid id)
        {
            Game game;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.Game + "_" + id, out game))
            {
                // Key not in cache, so get data.
                game = GameService.GetGame(id);

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromDays(30));

                // Save data in cache.
                _cache.Set(CacheKeys.Game + "_" + id, game, cacheEntryOptions);
            }

            if (game == null)
            {
                return BadRequest();
            }
            return Ok(JsonConvert.SerializeObject(game, Formatting.Indented));
        }
    }
}