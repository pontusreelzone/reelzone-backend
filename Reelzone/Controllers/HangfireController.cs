﻿using Microsoft.AspNetCore.Mvc;
using Reelzone.Core.Interfaces;

namespace Reelzone.Controllers
{
    [Route("/Api/Hangfire")]
    public class HangfireController : ControllerBase
    {
        public IHangfireService HangfireService;
        public IUserAchievmentService UserAchievementService;

        public HangfireController(IHangfireService hangfireService, IUserAchievmentService userAchievementService)
        {
            HangfireService = hangfireService;
            UserAchievementService = userAchievementService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            //Declare jobs
            HangfireService.DeclareJobs();

            return Ok();
        }
    }
}