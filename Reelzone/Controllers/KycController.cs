﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Interfaces;

namespace Reelzone.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KycController : ControllerBase
    {
        public IUserService UserService;
        public IMailService MailService;

        public KycController(IUserService userService, IMailService mailService)
        {
            UserService = userService;
            MailService = mailService;
        }

        [Route("callback/{secret}")]
        [HttpPost]
        public ActionResult Callback([FromBody] MatiCallbackModel payload, string secret)
        {
            if (secret == "BVHzoECIMV")
            {
                if (payload.eventName == "verification_completed")
                {
                    if (payload.details.age.data >= 18)
                    {
                        UserService.VerifyKyc(new Guid(payload.metadata.ID));
                        var user = UserService.GetUser(new Guid(payload.metadata.ID));
                        MailService.SendVerifiedEmail(user.Email);
                        return Ok(payload);
                    }
                    else
                    {
                        return Ok();
                    }
                }

                /*if (payload.eventName == "step_completed")
                {
                    var user = UserService.GetUser(new Guid(payload.metadata.ID));

                    if (payload.step.id == "document-reading")
                    {
                        if (payload.step.data.dateOfBirth != null)
                        {
                            user.BirthDate = DateTime.ParseExact(payload.step.data.dateOfBirth.value, "yyyy-MM-dd", null);
                        }
                    }

                    UserService.UpdateBirthDate(user);
                }*/

                return Ok();
            }
            else
            {
                return Ok();
            }
        }
    }
}