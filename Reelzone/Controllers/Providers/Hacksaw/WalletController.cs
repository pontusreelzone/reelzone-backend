﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using Reelzone.API.ApiModels.Providers.Hacksaw;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Services;
using Reelzone.Core.Dtos;
using System.Text;
using System.Threading.Tasks;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WalletController : ControllerBase
    {
        private IUserService UserService;
        private ITournamentService TournamentService;
        private LoggerService LoggerService;
        private IGameTransactionService GameTransactionService;
        private IUserExperienceService UserExperienceService;
        private IWalletService WalletService;
        private ICoinService CoinService;
        private IUserActivityService UserActivityService;
        private IPushService PushService;

        public WalletController(IUserService userService, ITournamentService tournamentService, IGameTransactionService gameTransactionService, IUserExperienceService userExperienceService, IWalletService walletService, ICoinService coinService, IUserActivityService userActivityService, IPushService pushService)
        {
            UserService = userService;
            TournamentService = tournamentService;
            LoggerService = new LoggerService();
            GameTransactionService = gameTransactionService;
            UserExperienceService = userExperienceService;
            WalletService = walletService;
            CoinService = coinService;
            UserActivityService = userActivityService;
            PushService = pushService;
        }

        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        [Route("hacksaw")]
        [HttpPost]
        public async Task<ActionResult> Authenticate([FromBody] InitialApiModel initialApiModel)
        {
            try
            {
                LoggerService.Log("Got new wallet action '" + initialApiModel.Action + "' with data");
                LoggerService.Log(JsonConvert.SerializeObject(initialApiModel));
                //D>jUP4Hn%aB&  yw{b6*8azqPX
                if (initialApiModel.Secret != "yw{b6*8azqPX")
                {
                    var wrongSecretModel = new BetResponseApiModel()
                    {
                        StatusCode = StatusCodes.General,
                        StatusMessage = "Wrong secret."
                    };

                    return Ok(wrongSecretModel);
                }

                switch (initialApiModel.Action)
                {
                    case "Authenticate":

                        var token = WalletService.GetToken(initialApiModel.Token);
                        var user = UserService.GetUserByToken(initialApiModel.Token);

                        if (token.Type == TokenType.Tournament)
                        {
                            var attende = TournamentService.GetAttenderByToken(initialApiModel.Token);

                            var authenticateResponseApiModel = new AuthenticateResponseApiModel()
                            {
                                ExternalPlayerId = user.ID.ToString(),
                                Name = user.Username,
                                AccountBalance = attende.Balance,
                                AccountCurrency = "EUR",
                                ExternalSessionId = attende.ID.ToString(),
                                LanguageId = "EN",
                                CountryId = "MT",
                                BirthDate = new DateTime(1995, 2, 22).ToString("yyyy-MM-dd"),
                                RegistrationDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                BrandId = "reelzone_stg",
                                Gender = "m",
                                StatusCode = StatusCodes.Success,
                                StatusMessage = ""
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(authenticateResponseApiModel));
                            return Ok(authenticateResponseApiModel);
                        }
                        else
                        {
                            var authenticateResponseApiModel = new AuthenticateResponseApiModel()
                            {
                                ExternalPlayerId = user.ID.ToString(),
                                Name = user.Username,
                                AccountBalance = user.Coins * 100,
                                AccountCurrency = "EUR",
                                ExternalSessionId = CreateMD5(user.ID.ToString()),
                                LanguageId = "EN",
                                CountryId = "MT",
                                BirthDate = new DateTime(1995, 2, 22).ToString("yyyy-MM-dd"),
                                RegistrationDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                BrandId = "reelzone_stg",
                                Gender = "m",
                                StatusCode = StatusCodes.Success,
                                StatusMessage = ""
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(authenticateResponseApiModel));
                            return Ok(authenticateResponseApiModel);
                        }
                    case "Balance":
                        var balanceAttende = TournamentService.GetAttender(new Guid(initialApiModel.ExternalSessionId));

                        if (balanceAttende != null)
                        {
                            var balanceResponseApiModel = new BalanceResponseApiModel()
                            {
                                AccountBalance = balanceAttende.Balance,
                                AccountCurrency = "EUR",
                                StatusCode = StatusCodes.Success,
                                StatusMessage = ""
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(balanceResponseApiModel));
                            return Ok(balanceResponseApiModel);
                        }
                        else
                        {
                            var balanceUser = UserService.GetUser(new Guid(initialApiModel.ExternalPlayerId));

                            var balanceResponseApiModel = new BalanceResponseApiModel()
                            {
                                AccountBalance = balanceUser.Coins * 100,
                                AccountCurrency = "EUR",
                                StatusCode = StatusCodes.Success,
                                StatusMessage = ""
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(balanceResponseApiModel));
                            return Ok(balanceResponseApiModel);
                        }

                    case "Bet":
                        var betTransaction = GameTransactionService.Find(initialApiModel.TransactionId);
                        if (betTransaction == null)
                        {
                            var betAttende = TournamentService.GetAttender(new Guid(initialApiModel.ExternalSessionId));

                            if (betAttende != null)
                            {
                                if (initialApiModel.Amount > 100)
                                {
                                    var betErrorResponseApiModel = new BetResponseApiModel()
                                    {
                                        StatusCode = StatusCodes.InsufficientFunds,
                                        StatusMessage = ""
                                    };
                                    return Ok(betErrorResponseApiModel);
                                }
                                else
                                {
                                    var updatedAttende = TournamentService.DeductSpins(betAttende, initialApiModel.Amount);

                                    var betResponseApiModel = new BetResponseApiModel()
                                    {
                                        AccountBalance = updatedAttende.Balance,
                                        StatusCode = StatusCodes.Success,
                                        StatusMessage = ""
                                    };

                                    LoggerService.Log("Responding with the following object");
                                    LoggerService.Log(JsonConvert.SerializeObject(betResponseApiModel));

                                    UserExperienceService.CreateUserExperience(1, betAttende.User.ID);

                                    GameTransactionService.Create(new GameTranscationDto()
                                    {
                                        Amount = initialApiModel.Amount,
                                        Description = "Bet on tournament",
                                        ExternalTransactionId = initialApiModel.TransactionId,
                                        Tournament = TournamentService.GetTournament(betAttende.Tournament.ID),
                                        User = UserService.GetUser(betAttende.User.ID),
                                    });

                                    //UserActivityService.CreateUserActivity(betAttende.User.ID, "UserSpinnedTournament");

                                    return Ok(betResponseApiModel);
                                }
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.ExternalPlayerId));

                                if (betUser.Coins >= (initialApiModel.Amount / 100))
                                {
                                    var amount = CoinService.CreateCoin(new CoinDto()
                                    {
                                        Amount = (initialApiModel.Amount / 100) * -1,
                                        Created = DateTime.Now,
                                        Description = "Betting on game",
                                        User = betUser
                                    });

                                    await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");

                                    var betResponseApiModel = new BetResponseApiModel()
                                    {
                                        AccountBalance = amount * 100,
                                        StatusCode = StatusCodes.Success,
                                        StatusMessage = ""
                                    };

                                    LoggerService.Log("Responding with the following object");
                                    LoggerService.Log(JsonConvert.SerializeObject(betResponseApiModel));

                                    UserExperienceService.CreateUserExperience(1, betUser.ID);

                                    GameTransactionService.Create(new GameTranscationDto()
                                    {
                                        Amount = initialApiModel.Amount,
                                        Description = "Bet on tournament",
                                        ExternalTransactionId = initialApiModel.TransactionId,
                                        Tournament = null,
                                        User = UserService.GetUser(betUser.ID),
                                    });

                                    UserActivityService.CreateUserActivity(betUser.ID, "UserSpinnedCoinLeague");
                                    return Ok(betResponseApiModel);
                                }
                                else
                                {
                                    var betResponseApiModel = new BetResponseApiModel()
                                    {
                                        AccountBalance = betUser.Coins * 100,
                                        StatusCode = StatusCodes.InsufficientFunds,
                                        StatusMessage = ""
                                    };
                                    return Ok(betResponseApiModel);
                                }
                            }
                        }
                        else
                        {
                            LoggerService.Log("Got the duplicated transactionId.");
                            LoggerService.Log(JsonConvert.SerializeObject(initialApiModel));

                            var errorResponse = new WinResponseApiModel()
                            {
                                StatusCode = StatusCodes.General,
                                StatusMessage = "Duplicated transactionId"
                            };
                            return Ok(errorResponse);
                        }

                    case "Win":
                        var winTransaction = GameTransactionService.Find(initialApiModel.TransactionId);
                        if (winTransaction == null)
                        {
                            var winAttende = TournamentService.GetAttender(new Guid(initialApiModel.ExternalSessionId));

                            if (winAttende != null)
                            {
                                var updatedScore = TournamentService.UpdateScore(winAttende, initialApiModel.Amount);

                                var winResponseApiModel = new WinResponseApiModel()
                                {
                                    AccountBalance = updatedScore.Balance,
                                    StatusCode = StatusCodes.Success,
                                    StatusMessage = ""
                                };
                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(winResponseApiModel));

                                GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = initialApiModel.Amount,
                                    Description = "Win on tournament",
                                    ExternalTransactionId = initialApiModel.TransactionId,
                                    Tournament = TournamentService.GetTournament(winAttende.Tournament.ID),
                                    User = UserService.GetUser(winAttende.User.ID),
                                });

                                return Ok(winResponseApiModel);
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.ExternalPlayerId));

                                var amount = CoinService.CreateCoin(new CoinDto()
                                {
                                    Amount = initialApiModel.Amount / 100,
                                    Created = DateTime.Now,
                                    Description = "Winning on game",
                                    User = betUser
                                });

                                await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");

                                var winResponseApiModel = new WinResponseApiModel()
                                {
                                    AccountBalance = (amount * 100),
                                    StatusCode = StatusCodes.Success,
                                    StatusMessage = ""
                                };
                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(winResponseApiModel));

                                GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = initialApiModel.Amount,
                                    Description = "Win on tournament",
                                    ExternalTransactionId = initialApiModel.TransactionId,
                                    Tournament = null,
                                    User = UserService.GetUser(betUser.ID),
                                });

                                return Ok(winResponseApiModel);
                            }
                        }
                        else
                        {
                            LoggerService.Log("Got the duplicated transactionId.");
                            LoggerService.Log(JsonConvert.SerializeObject(initialApiModel));

                            var errorResponse = new WinResponseApiModel()
                            {
                                StatusCode = StatusCodes.General,
                                StatusMessage = "Duplicated transactionId"
                            };
                            return Ok(errorResponse);
                        }

                    case "Rollback":

                        var rollbackAttende = TournamentService.GetAttender(new Guid(initialApiModel.ExternalSessionId));
                        var rollbackTransaction = GameTransactionService.Find(initialApiModel.TransactionId);

                        if (rollbackTransaction != null)
                        {
                            if (rollbackAttende != null)
                            {
                                var updatedScoreRollback = TournamentService.UpdateScore(rollbackAttende, initialApiModel.Amount * -1);

                                var rollbackResponseApiModel = new RollbackResponseApiModel()
                                {
                                    AccountBalance = updatedScoreRollback.Spins,
                                    ExternalTransactionId = rollbackTransaction.ID.ToString(),
                                    StatusCode = StatusCodes.Success,
                                    StatusMessage = ""
                                };
                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(rollbackResponseApiModel));

                                GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = initialApiModel.Amount,
                                    Description = "Rollback on tournament bet",
                                    ExternalTransactionId = initialApiModel.TransactionId,
                                    Tournament = TournamentService.GetTournament(rollbackAttende.Tournament.ID),
                                    User = UserService.GetUser(rollbackAttende.User.ID)
                                });

                                return Ok(rollbackResponseApiModel);
                            }
                            else
                            {
                                var rollbackUser = UserService.GetUser(new Guid(initialApiModel.ExternalPlayerId));

                                var amount = CoinService.CreateCoin(new CoinDto()
                                {
                                    Amount = (initialApiModel.Amount / 100) * -1,
                                    Created = DateTime.Now,
                                    Description = "Betting on game",
                                    User = rollbackUser
                                });

                                var rollbackResponseApiModel = new RollbackResponseApiModel()
                                {
                                    AccountBalance = amount,
                                    ExternalTransactionId = rollbackTransaction.ID.ToString(),
                                    StatusCode = StatusCodes.Success,
                                    StatusMessage = ""
                                };
                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(rollbackResponseApiModel));

                                GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = initialApiModel.Amount,
                                    Description = "Rollback on tournament bet",
                                    ExternalTransactionId = initialApiModel.TransactionId,
                                    User = UserService.GetUser(rollbackUser.ID)
                                });

                                return Ok(rollbackResponseApiModel);
                            }
                        }
                        else
                        {
                            var rollbackResponseApiModel = new RollbackResponseApiModel()
                            {
                                AccountBalance = 0,
                                ExternalTransactionId = "1",
                                StatusCode = StatusCodes.Success,
                                StatusMessage = ""
                            };
                            return Ok(rollbackResponseApiModel);
                        }

                    default:
                        var error = new RollbackResponseApiModel()
                        {
                            StatusCode = StatusCodes.General,
                            StatusMessage = "Not a valid action."
                        };
                        LoggerService.Log("Action did not exist '" + initialApiModel.Action + "'");
                        LoggerService.Log(JsonConvert.SerializeObject(error));
                        return Ok(error);
                }
            }
            catch (Exception e)
            {
                var error = new RollbackResponseApiModel()
                {
                    StatusCode = StatusCodes.General,
                    StatusMessage = "Not a valid action."
                };
                LoggerService.Log("Action did not exist '" + initialApiModel.Action + "'");
                LoggerService.Log(JsonConvert.SerializeObject(error));
                return Ok(error);
            }
        }
    }
}