﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using Reelzone.API.ApiModels.Providers.Kalamba;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Services;
using Reelzone.Core.Dtos;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Threading.Tasks;
using Nancy.Json;
using Microsoft.AspNetCore.Http;
using Reelzone.API.ApiModels.Providers.Kalamba.Enum;
using Reelzone.API.ApiModels.Providers.Kalamba.Internal;
using System.Globalization;

namespace Reelzone.Controllers.Kalamba
{
    [ApiController]
    [Route("[controller]")]
    public class WalletController : ControllerBase
    {
        private IUserService UserService;
        private ITournamentService TournamentService;
        private LoggerService LoggerService;
        private IGameTransactionService GameTransactionService;
        private IUserExperienceService UserExperienceService;
        private IWalletService WalletService;
        private ICoinService CoinService;
        private IUserActivityService UserActivityService;
        private HashHelper HashHelper;
        private CurlHelper CurlHelper;
        private string apiUrl = "https://reelzone.integration.kalambagames.com";
        private JavaScriptSerializer JavaScriptSerializer = new JavaScriptSerializer();
        private IPushService PushService;

        public WalletController(IUserService userService, ITournamentService tournamentService, IGameTransactionService gameTransactionService, IUserExperienceService userExperienceService, IWalletService walletService, ICoinService coinService, IUserActivityService userActivityService, IPushService pushService)
        {
            UserService = userService;
            TournamentService = tournamentService;
            LoggerService = new LoggerService();
            HashHelper = new HashHelper();
            CurlHelper = new CurlHelper();
            GameTransactionService = gameTransactionService;
            UserExperienceService = userExperienceService;
            WalletService = walletService;
            CoinService = coinService;
            UserActivityService = userActivityService;
            PushService = pushService;
        }

        public bool VerifyAuth(HttpContext httpContext)
        {
            string authHeader = httpContext.Request.Headers["Authorization"];

            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');

                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);

                if (username == "kalamba_prod" && password == "49LWXfWjtrmocEdLa956RvmswCFWWn2o")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        [Route("kalamba/user/balance")]
        public async Task<ActionResult> Balance(BalanceApiModel balanceApiModel)
        {
            /*if (VerifyAuth(HttpContext) == false)
            {
                var errorResponse = new BalanceResponseApiModel()
                {
                    Status = "RS_ERROR_UNKNOWN"
                };
                return Ok(errorResponse);
            }*/

            var response = new BalanceResponseApiModel();

            var balanceAttende = TournamentService.GetAttender(new Guid(balanceApiModel.UserId));

            if (balanceAttende != null)
            {
                response = new BalanceResponseApiModel()
                {
                    Balance = (balanceAttende.Balance / 100).ToString(),
                    Currency = "EUR",
                    Status = "RS_OK",
                    UserId = balanceAttende.ID.ToString()
                };
            }
            else
            {
                var balanceUser = UserService.GetUser(new Guid(balanceApiModel.UserId));

                response = new BalanceResponseApiModel()
                {
                    Balance = balanceUser.Coins.ToString(),
                    Currency = "EUR",
                    Status = "RS_OK",
                    UserId = balanceUser.ID.ToString()
                };
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("kalamba/transaction/win")]
        public async Task<ActionResult> Win([FromBody]WinApiModel winApiModel)
        {
            /*if (VerifyAuth(HttpContext) == false)
             {
                 var errorResponse = new BalanceResponseApiModel()
                 {
                     Status = "RS_ERROR_UNKNOWN"
                 };
                 return Ok(errorResponse);
             }*/

            /*var winTransaction = GameTransactionService.Find(GetHashCode(winApiModel.TransactionId));
            if (winTransaction == null)
            {*/
            var winAttende = TournamentService.GetAttender(new Guid(winApiModel.UserId));
            var winAmount = Convert.ToInt64(Convert.ToDouble(winApiModel.Amount));
            var winAmountD = Double.Parse(winApiModel.Amount);
            if (winAttende != null)
            {
                var updatedScore = TournamentService.UpdateScore(winAttende, Convert.ToInt64(winAmountD * 100));

                var winResponseApiModel = new WinResponseApiModel()
                {
                    Balance = (updatedScore.Balance / 100),
                    Currency = "EUR",
                    Status = "RS_OK",
                    UserId = winAttende.ID.ToString()
                };
                LoggerService.Log("Responding with the following object");
                LoggerService.Log(JsonConvert.SerializeObject(winResponseApiModel));

                GameTransactionService.Create(new GameTranscationDto()
                {
                    Amount = winAmount * 100,
                    Description = "Win on tournament",
                    ExternalTransactionId = GetHashCode(winApiModel.TransactionId),
                    Tournament = TournamentService.GetTournament(winAttende.Tournament.ID),
                    User = UserService.GetUser(winAttende.User.ID),
                });

                return Ok(winResponseApiModel);
            }
            else
            {
                var betUser = UserService.GetUser(new Guid(winApiModel.UserId));

                var amount = CoinService.CreateCoin(new CoinDto()
                {
                    Amount = winAmount,
                    Created = DateTime.Now,
                    Description = "Winning on game",
                    User = betUser
                });

                await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");

                var winResponseApiModel = new WinResponseApiModel()
                {
                    Balance = amount,
                    Currency = "EUR",
                    Status = "RS_OK",
                    UserId = betUser.ID.ToString()
                };
                LoggerService.Log("Responding with the following object");
                LoggerService.Log(JsonConvert.SerializeObject(winResponseApiModel));

                GameTransactionService.Create(new GameTranscationDto()
                {
                    Amount = winAmount * 100,
                    Description = "Win on game",
                    ExternalTransactionId = GetHashCode(winApiModel.TransactionId),
                    Tournament = null,
                    User = UserService.GetUser(betUser.ID),
                });

                return Ok(winResponseApiModel);
            }
            /*}
            else
            {
                var winAttende = TournamentService.GetAttender(new Guid(winApiModel.UserId));
                if (winAttende != null)
                {
                    LoggerService.Log("Got the duplicated transactionId.");
                    LoggerService.Log(JsonConvert.SerializeObject(winApiModel));

                    var errorResponse = new WinResponseApiModel()
                    {
                        Status = "RS_OK",
                        Currency = "EUR",
                        UserId = winApiModel.UserId,
                        Balance = winAttende.Balance / 100
                    };
                    return Ok(errorResponse);
                }
                else
                {
                    var betUser = UserService.GetUser(new Guid(winApiModel.UserId));
                    var errorResponse = new WinResponseApiModel()
                    {
                        Status = "RS_OK",
                        Currency = "EUR",
                        UserId = winApiModel.UserId,
                        Balance = betUser.Coins
                    };
                    return Ok(errorResponse);
                }
            }*/
        }

        [HttpPost]
        [Route("kalamba/transaction/rollback")]
        public async Task<ActionResult> Rollback([FromBody]RollbackApiModel rollbackApiModel)
        {
            /*if (VerifyAuth(HttpContext) == false)
            {
                var errorResponse = new BalanceResponseApiModel()
                {
                    Status = "RS_ERROR_UNKNOWN"
                };
                return Ok(errorResponse);
            }*/

            var rollbackResponseApiModel = new RollbackResponseApiModel();
            var winTransaction = GameTransactionService.Find(GetHashCode(rollbackApiModel.ReferenceTranscationId));
            if (winTransaction == null)
            {
                var rollbackAttende = TournamentService.GetAttender(new Guid(rollbackApiModel.UserId));
                var rollbackAmount = winTransaction.Amount;
                if (rollbackAttende != null)
                {
                    var updatedScoreRollback = TournamentService.UpdateScore(rollbackAttende, rollbackAmount * -1);

                    GameTransactionService.Create(new GameTranscationDto()
                    {
                        Amount = rollbackAmount,
                        Description = "Rollback on tournament bet",
                        ExternalTransactionId = GetHashCode(rollbackApiModel.TransactionId),
                        Tournament = TournamentService.GetTournament(rollbackAttende.Tournament.ID),
                        User = UserService.GetUser(rollbackAttende.User.ID)
                    });

                    rollbackResponseApiModel = new RollbackResponseApiModel()
                    {
                        Currency = "EUR",
                        Status = "RS_OK",
                        Balance = updatedScoreRollback.Balance / 100,
                        UserId = rollbackAttende.ID.ToString()
                    };
                }
                else
                {
                    var betUser = UserService.GetUser(new Guid(rollbackApiModel.UserId));

                    var amount = CoinService.CreateCoin(new CoinDto()
                    {
                        Amount = rollbackAmount * -1,
                        Created = DateTime.Now,
                        Description = "Rollback on game",
                        User = betUser
                    });

                    GameTransactionService.Create(new GameTranscationDto()
                    {
                        Amount = rollbackAmount,
                        Description = "Rollback on coin league bet",
                        ExternalTransactionId = GetHashCode(rollbackApiModel.TransactionId),
                        Tournament = TournamentService.GetTournament(rollbackAttende.Tournament.ID),
                        User = UserService.GetUser(rollbackAttende.User.ID)
                    });

                    rollbackResponseApiModel = new RollbackResponseApiModel()
                    {
                        Currency = "EUR",
                        Status = "RS_OK",
                        Balance = amount,
                        UserId = rollbackAttende.ID.ToString()
                    };
                }
            }

            return Ok(rollbackResponseApiModel);
        }

        public static int GetHashCode(string value)
        {
            int h = 0;
            for (int i = 0; i < value.Length; i++)
                h += value[i] * 31 ^ value.Length - (i + 1);
            return h;
        }

        public long StringToDouble(string input)
        {
            var last = input.LastIndexOfAny(new[] { ',', '.' });
            var separator = last >= 0 ? input[last] : '.';
            var clone = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = separator.ToString(CultureInfo.InvariantCulture);
            return long.Parse(input, clone);
        }

        [HttpPost]
        [Route("kalamba/transaction/bet")]
        public async Task<ActionResult> Bet([FromBody]BetApiModel betApiModel)
        {
            /*if (VerifyAuth(HttpContext) == false)
            {
                var errorResponse = new BalanceResponseApiModel()
                {
                    Status = "RS_ERROR_UNKNOWN"
                };
                return Ok(errorResponse);
            }*/

            /*var betTransaction = GameTransactionService.Find(GetHashCode(betApiModel.TransactionId));

            if (betTransaction == null)
            {*/
            var betAttende = TournamentService.GetAttender(new Guid(betApiModel.UserId));
            var betAmount = Convert.ToInt64(Convert.ToDouble(betApiModel.Amount));

            if (betAttende != null)
            {
                if (betAmount == 1)
                {
                    var updatedAttende = TournamentService.DeductSpins(betAttende, betAmount);

                    var betResponseApiModel = new BetResponseApiModel()
                    {
                        Balance = (updatedAttende.Balance / 100),
                        Currency = "EUR",
                        Status = "RS_OK",
                        UserId = betAttende.ID.ToString()
                    };

                    LoggerService.Log("Responding with the following object");
                    LoggerService.Log(JsonConvert.SerializeObject(betResponseApiModel));

                    UserExperienceService.CreateUserExperience(1, betAttende.User.ID);

                    GameTransactionService.Create(new GameTranscationDto()
                    {
                        Amount = betAmount * 100,
                        Description = "Bet on tournament",
                        ExternalTransactionId = GetHashCode(betApiModel.TransactionId),
                        Tournament = TournamentService.GetTournament(betAttende.Tournament.ID),
                        User = UserService.GetUser(betAttende.User.ID),
                    });

                    //UserActivityService.CreateUserActivity(betAttende.User.ID, "UserSpinnedTournament");

                    return Ok(betResponseApiModel);
                }
                else
                {
                    var betResponseApiModel = new BetResponseApiModel()
                    {
                        Currency = "EUR",
                        Status = "RS_ERROR_NOT_ENOUGH_MONEY",
                        UserId = betAttende.ID.ToString()
                    };
                    return Ok(betResponseApiModel);
                }
            }
            else
            {
                var betUser = UserService.GetUser(new Guid(betApiModel.UserId));

                if (betUser.Coins >= betAmount)
                {
                    var amount = CoinService.CreateCoin(new CoinDto()
                    {
                        Amount = betAmount * -1,
                        Created = DateTime.Now,
                        Description = "Betting on game",
                        User = betUser
                    });

                    await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");

                    var betResponseApiModel = new BetResponseApiModel()
                    {
                        Balance = amount,
                        Currency = "EUR",
                        Status = "RS_OK",
                        UserId = betUser.ID.ToString()
                    };

                    LoggerService.Log("Responding with the following object");
                    LoggerService.Log(JsonConvert.SerializeObject(betResponseApiModel));

                    UserExperienceService.CreateUserExperience(1, betUser.ID);

                    GameTransactionService.Create(new GameTranscationDto()
                    {
                        Amount = betAmount * 100,
                        Description = "Bet on game",
                        ExternalTransactionId = GetHashCode(betApiModel.TransactionId),
                        Tournament = null,
                        User = UserService.GetUser(betUser.ID),
                    });

                    UserActivityService.CreateUserActivity(betUser.ID, "UserSpinnedCoinLeague");
                    return Ok(betResponseApiModel);
                }
                else
                {
                    var betResponseApiModel = new BetResponseApiModel()
                    {
                        Balance = betUser.Coins,
                        Currency = "EUR",
                        Status = "RS_ERROR_NOT_ENOUGH_MONEY",
                        UserId = betUser.ID.ToString()
                    };
                    return Ok(betResponseApiModel);
                }
            }
            /*}
            else
            {
                LoggerService.Log("Got the duplicated transactionId.");
                LoggerService.Log(JsonConvert.SerializeObject(betApiModel));

                var errorResponse = new WinResponseApiModel()
                {
                    Status = "RS_ERROR_DUPLICATE_TRANSACTION",
                    Currency = "EUR"
                };
                return Ok(errorResponse);
            }*/
        }

        [HttpPost]
        [Route("kalamba/authenticate")]
        [Authorize]
        public async Task<ActionResult> Authenticate([FromBody]_AuthenticateApiModel authenticateApiModel)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);

            var model = new AuthenticateApiModel();
            var tournament = TournamentService.GetTournament(authenticateApiModel.TournamentId);

            if (tournament != null)
            {
                var walletTokenDto = new WalletTokenDto()
                {
                    Tournament = tournament,
                    User = UserService.GetUser(user.ID)
                };

                if (authenticateApiModel.Platform == null)
                {
                    authenticateApiModel.Platform = "GPL_DESKTOP";
                }

                var walletToken = WalletService.CreateWalletToken(walletTokenDto);

                var attende = TournamentService.GetAttenderByToken(walletToken.Token);

                model = new AuthenticateApiModel()
                {
                    Mode = "real",
                    UserId = attende.ID.ToString(),
                    Token = walletToken.Token,
                    GameId = authenticateApiModel.GameId,
                    BrandId = "reelzone",
                    Platform = authenticateApiModel.Platform,
                    Lang = "eng",
                    Currency = "EUR",
                    Country = "MT",
                    MaxBet = "1.20",
                    MinBet = "0.80"
                };
            }
            else
            {
                var walletTokenDto = new WalletTokenDto()
                {
                    Tournament = null,
                    User = UserService.GetUser(user.ID)
                };

                var walletToken = WalletService.CreatePlayWalletToken(walletTokenDto);

                model = new AuthenticateApiModel()
                {
                    Mode = "real",
                    UserId = user.ID.ToString(),
                    Token = walletToken.Token,
                    GameId = authenticateApiModel.GameId,
                    BrandId = "reelzone",
                    Platform = authenticateApiModel.Platform,
                    Lang = "eng",
                    Currency = "EUR",
                    Country = "MT"
                };
            }

            var body = JsonConvert.SerializeObject(model, Formatting.None);
            var request = await CurlHelper.Curl(apiUrl + "/games/url", body);

            return Ok(request);
        }
    }
}