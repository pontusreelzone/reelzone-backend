﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using Reelzone.API.ApiModels.Providers.Nolimit;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Services;
using Reelzone.Core.Dtos;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Reelzone.Controllers.Nolimit
{
    [ApiController]
    [Route("[controller]")]
    public class WalletController : ControllerBase
    {
        private IUserService UserService;
        private ITournamentService TournamentService;
        private LoggerService LoggerService;
        private IGameTransactionService GameTransactionService;
        private IUserExperienceService UserExperienceService;
        private IWalletService WalletService;
        private ICoinService CoinService;
        private IUserActivityService UserActivityService;
        private IPushService PushService;

        public WalletController(IUserService userService, ITournamentService tournamentService, IGameTransactionService gameTransactionService, IUserExperienceService userExperienceService, IWalletService walletService, ICoinService coinService, IUserActivityService userActivityService, IPushService pushService)
        {
            UserService = userService;
            TournamentService = tournamentService;
            LoggerService = new LoggerService();
            GameTransactionService = gameTransactionService;
            UserExperienceService = userExperienceService;
            WalletService = walletService;
            CoinService = coinService;
            UserActivityService = userActivityService;
            PushService = pushService;
        }

        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static int GetHashCode(string value)
        {
            int h = 0;
            for (int i = 0; i < value.Length; i++)
                h += value[i] * 31 ^ value.Length - (i + 1);
            return h;
        }

        [Route("nolimit")]
        [HttpPost]
        public async Task<ActionResult> Authenticate([FromBody] InitialApiModel initialApiModel)
        {
            try
            {
                LoggerService.Log("Got new wallet action '" + initialApiModel.Method + "' with data");
                LoggerService.Log(JsonConvert.SerializeObject(initialApiModel));

                if (initialApiModel.Params.Identification.Name != "REELZONE" || initialApiModel.Params.Identification.Key != "9dMTToZAP2l5")
                {
                    return BadRequest();
                }

                switch (initialApiModel.Method)
                {
                    case "wallet.validate-token":

                        var token = WalletService.GetToken(initialApiModel.Params.Token);
                        var user = UserService.GetUserByToken(initialApiModel.Params.Token);

                        if (token.Type == TokenType.Tournament)
                        {
                            var attende = TournamentService.GetAttenderByToken(initialApiModel.Params.Token);

                            var authenticateResponseApiModel = new ValidateTokenResponseModel()
                            {
                                JsonRPC = "2.0",
                                Result = new ResultResponseApiModel()
                                {
                                    UserId = user.ID.ToString(),
                                    Username = user.Username,
                                    Balance = new BalanceResponseApiModel() { Amount = attende.Balance, Currency = "EUR" }
                                },
                                ID = initialApiModel.ID
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(authenticateResponseApiModel));
                            return Ok(authenticateResponseApiModel);
                        }
                        else
                        {
                            var authenticateResponseApiModel = new ValidateTokenResponseModel()
                            {
                                JsonRPC = "2.0",
                                Result = new ResultResponseApiModel()
                                {
                                    UserId = user.ID.ToString(),
                                    Username = user.Username,
                                    Balance = new BalanceResponseApiModel() { Amount = user.Coins, Currency = "EUR" }
                                },
                                ID = initialApiModel.ID
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(authenticateResponseApiModel));
                            return Ok(authenticateResponseApiModel);
                        }
                    case "wallet.balance":
                        var balanceAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));

                        if (balanceAttende != null)
                        {
                            var balanceApiResponseModel = new ValidateTokenResponseModel()
                            {
                                JsonRPC = "2.0",
                                Result = new ResultResponseApiModel()
                                {
                                    UserId = balanceAttende.User.ID.ToString(),
                                    Username = balanceAttende.User.Username,
                                    Balance = new BalanceResponseApiModel() { Amount = balanceAttende.Balance, Currency = "EUR" }
                                },
                                ID = initialApiModel.ID
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(balanceApiResponseModel));
                            return Ok(balanceApiResponseModel);
                        }
                        else
                        {
                            var balanceUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));

                            var balanceApiResponseModel = new ValidateTokenResponseModel()
                            {
                                JsonRPC = "2.0",
                                Result = new ResultResponseApiModel()
                                {
                                    UserId = balanceUser.ID.ToString(),
                                    Username = balanceUser.Username,
                                    Balance = new BalanceResponseApiModel() { Amount = balanceUser.Coins, Currency = "EUR" }
                                },
                                ID = initialApiModel.ID
                            };
                            LoggerService.Log("Responding with the following object");
                            LoggerService.Log(JsonConvert.SerializeObject(balanceApiResponseModel));
                            return Ok(balanceApiResponseModel);
                        }

                    case "wallet.withdraw":
                        var betTransaction = GameTransactionService.Find(GetHashCode(initialApiModel.Params.Information.GameRoundId));

                        if (betTransaction == null)
                        {
                            var betAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));

                            if (betAttende != null)
                            {
                                if (initialApiModel.Params.Withdraw.Amount > 100)
                                {
                                    return BadRequest();
                                }
                                else
                                {
                                    var updatedAttende = TournamentService.DeductSpins(betAttende, Convert.ToInt64(initialApiModel.Params.Withdraw.Amount));

                                    UserExperienceService.CreateUserExperience(1, betAttende.User.ID);

                                    var transaction = GameTransactionService.Create(new GameTranscationDto()
                                    {
                                        Amount = Convert.ToInt64(initialApiModel.Params.Withdraw.Amount),
                                        Description = "Bet on tournament",
                                        ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                        Tournament = TournamentService.GetTournament(betAttende.Tournament.ID),
                                        User = UserService.GetUser(betAttende.User.ID),
                                    });

                                    var betApiResponseModel = new ValidateTokenResponseModel()
                                    {
                                        JsonRPC = "2.0",
                                        Result = new ResultResponseApiModel()
                                        {
                                            UserId = betAttende.ID.ToString(),
                                            Username = betAttende.User.Username,
                                            Balance = new BalanceResponseApiModel() { Amount = betAttende.Balance, Currency = "EUR" },
                                            TransactionId = transaction.ID.ToString()
                                        },
                                        ID = initialApiModel.ID
                                    };

                                    LoggerService.Log("Responding with the following object");
                                    LoggerService.Log(JsonConvert.SerializeObject(betApiResponseModel));

                                    return Ok(betApiResponseModel);
                                }
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));

                                if (betUser.Coins >= initialApiModel.Params.Withdraw.Amount)
                                {
                                    var amount = CoinService.CreateCoin(new CoinDto()
                                    {
                                        Amount = (Convert.ToInt64(initialApiModel.Params.Withdraw.Amount)) * -1,
                                        Created = DateTime.Now,
                                        Description = "Betting on game",
                                        User = betUser
                                    });

                                    await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");
                                    UserExperienceService.CreateUserExperience(1, betUser.ID);

                                    var transaction = GameTransactionService.Create(new GameTranscationDto()
                                    {
                                        Amount = amount,
                                        Description = "Bet on tournament",
                                        ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                        Tournament = null,
                                        User = UserService.GetUser(betUser.ID),
                                    });

                                    var betApiResponseModel = new ValidateTokenResponseModel()
                                    {
                                        JsonRPC = "2.0",
                                        Result = new ResultResponseApiModel()
                                        {
                                            UserId = betUser.ID.ToString(),
                                            Username = betUser.Username,
                                            Balance = new BalanceResponseApiModel() { Amount = amount, Currency = "EUR" },
                                            TransactionId = transaction.ID.ToString()
                                        },
                                        ID = initialApiModel.ID
                                    };

                                    LoggerService.Log("Responding with the following object");
                                    LoggerService.Log(JsonConvert.SerializeObject(betApiResponseModel));

                                    UserActivityService.CreateUserActivity(betUser.ID, "UserSpinnedCoinLeague");
                                    return Ok(betApiResponseModel);
                                }
                                else
                                {
                                    return BadRequest();
                                }
                            }
                        }
                        else
                        {
                            var winAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));

                            if (winAttende != null)
                            {
                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = winAttende.ID.ToString(),
                                        Username = winAttende.User.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = winAttende.Balance, Currency = "EUR" },
                                        TransactionId = betTransaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                return Ok(winResponseApiModel);
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));
                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = betUser.ID.ToString(),
                                        Username = betUser.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = betUser.Coins, Currency = "EUR" },
                                        TransactionId = betTransaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };
                                return Ok(winResponseApiModel);
                            }
                        }

                    case "wallet.deposit":
                        var winTransaction = GameTransactionService.Find(GetHashCode(initialApiModel.Params.Information.GameRoundId));

                        if (winTransaction == null)
                        {
                            var winAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));

                            if (winAttende != null)
                            {
                                var updatedScore = TournamentService.UpdateScore(winAttende, Convert.ToInt64(initialApiModel.Params.Deposit.Amount) * 100);

                                var transaction = GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = Convert.ToInt64(initialApiModel.Params.Deposit.Amount),
                                    Description = "Win on tournament",
                                    ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                    Tournament = TournamentService.GetTournament(winAttende.Tournament.ID),
                                    User = UserService.GetUser(winAttende.User.ID),
                                });

                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = winAttende.ID.ToString(),
                                        Username = winAttende.User.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = (updatedScore.Balance / 100), Currency = "EUR" },
                                        TransactionId = transaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                return Ok(winResponseApiModel);
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));

                                var amount = CoinService.CreateCoin(new CoinDto()
                                {
                                    Amount = Convert.ToInt64(initialApiModel.Params.Deposit.Amount),
                                    Created = DateTime.Now,
                                    Description = "Winning on game",
                                    User = betUser
                                });

                                await PushService.Send(betUser.ID, "balance", amount.ToString(), "", "success");

                                var transaction = GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = Convert.ToInt64(initialApiModel.Params.Deposit.Amount),
                                    Description = "Win on game",
                                    ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                    Tournament = null,
                                    User = UserService.GetUser(betUser.ID),
                                });
                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = betUser.ID.ToString(),
                                        Username = betUser.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = amount, Currency = "EUR" },
                                        TransactionId = transaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(winResponseApiModel));

                                return Ok(winResponseApiModel);
                            }
                        }
                        else
                        {
                            var winAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));

                            if (winAttende != null)
                            {
                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = winAttende.ID.ToString(),
                                        Username = winAttende.User.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = winAttende.Balance, Currency = "EUR" },
                                        TransactionId = winTransaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                return Ok(winResponseApiModel);
                            }
                            else
                            {
                                var betUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));
                                var winResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        UserId = betUser.ID.ToString(),
                                        Username = betUser.Username,
                                        Balance = new BalanceResponseApiModel() { Amount = betUser.Coins, Currency = "EUR" },
                                        TransactionId = winTransaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };
                                return Ok(winResponseApiModel);
                            }
                        }

                    case "wallet.rollback":

                        var rollbackAttende = TournamentService.GetAttender(new Guid(initialApiModel.Params.UserId));
                        var rollbackTransaction = GameTransactionService.Find(GetHashCode(initialApiModel.Params.Information.GameRoundId));

                        if (rollbackTransaction != null)
                        {
                            if (rollbackAttende != null)
                            {
                                var updatedScoreRollback = TournamentService.UpdateScore(rollbackAttende, rollbackTransaction.Amount * -1);

                                var transaction = GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = rollbackTransaction.Amount * -1,
                                    Description = "Rollback on tournament bet",
                                    ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                    Tournament = TournamentService.GetTournament(rollbackAttende.Tournament.ID),
                                    User = UserService.GetUser(rollbackAttende.User.ID)
                                });

                                var rollbackResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        Balance = new BalanceResponseApiModel() { Amount = updatedScoreRollback.Spins, Currency = "EUR" },
                                        TransactionId = transaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(rollbackResponseApiModel));

                                return Ok(rollbackResponseApiModel);
                            }
                            else
                            {
                                var rollbackUser = UserService.GetUser(new Guid(initialApiModel.Params.UserId));

                                var amount = CoinService.CreateCoin(new CoinDto()
                                {
                                    Amount = rollbackTransaction.Amount * -1,
                                    Created = DateTime.Now,
                                    Description = "Betting on game",
                                    User = rollbackUser
                                });

                                var transaction = GameTransactionService.Create(new GameTranscationDto()
                                {
                                    Amount = rollbackTransaction.Amount * -1,
                                    Description = "Rollback on tournament bet",
                                    ExternalTransactionId = GetHashCode(initialApiModel.Params.Information.GameRoundId),
                                    User = UserService.GetUser(rollbackUser.ID)
                                });

                                var rollbackResponseApiModel = new ValidateTokenResponseModel()
                                {
                                    JsonRPC = "2.0",
                                    Result = new ResultResponseApiModel()
                                    {
                                        Balance = new BalanceResponseApiModel() { Amount = amount, Currency = "EUR" },
                                        TransactionId = transaction.ID.ToString()
                                    },
                                    ID = initialApiModel.ID
                                };

                                LoggerService.Log("Responding with the following object");
                                LoggerService.Log(JsonConvert.SerializeObject(rollbackResponseApiModel));

                                return Ok(rollbackResponseApiModel);
                            }
                        }
                        else
                        {
                            return BadRequest();
                        }

                    default:
                        return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}