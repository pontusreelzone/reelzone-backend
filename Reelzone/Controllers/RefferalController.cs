﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;

namespace Reelzone.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RefferalController : ControllerBase
    {
        public IUserRefferalService UserRefferalService;
        public IUserService UserService;

        public RefferalController(IUserRefferalService userRefferalService, IUserService userService)
        {
            UserRefferalService = userRefferalService;
            UserService = userService;
        }

        [Route("leaderboard")]
        [HttpGet]
        public ActionResult Leaderboard()
        {
            var leaderboard = UserRefferalService.GetReferralLeaderboard();

            return Ok(leaderboard);
        }

        [Route("access/{code}")]
        [HttpPost]
        [Authorize]
        public ActionResult Access(string code)
        {
            var response = new ResponseApiModel();

            if (code == "WildzCaxino2020")
            {
                response.Successful = true;
            }
            else
            {
                response.Successful = false;
            }

            return Ok(response);
        }

        [Route("access2/{code}")]
        [HttpPost]
        [Authorize]
        public ActionResult AccessSecond(string code)
        {
            var response = new ResponseApiModel();

            if (code == "2020slotwolf")
            {
                response.Successful = true;
            }
            else
            {
                response.Successful = false;
            }

            return Ok(response);
        }

        [Route("redeem/{code}")]
        [HttpPost]
        [Authorize]
        public ActionResult Redeem(string code)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);
            var redeem = UserRefferalService.RedeemRefferal(code, user.ID);

            var response = new ResponseApiModel();

            if (redeem == true)
            {
                response.Successful = true;
            }
            else
            {
                response.Successful = false;
            }

            return Ok(response);
        }
    }
}