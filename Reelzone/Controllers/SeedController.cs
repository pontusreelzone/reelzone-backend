﻿using Microsoft.AspNetCore.Mvc;
using Reelzone.Core.Contexts;
using Reelzone.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Storage;
using Reelzone.Core.Entities.Users;
using Reelzone.Core.Helpers;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SeedController : ControllerBase
    {
        public DatabaseContext Context;
        public static DateHelper DateHelper;

        public SeedController(DatabaseContext dbContext)
        {
            Context = dbContext;
            DateHelper = new DateHelper();
        }

        [Route("tournaments")]
        public ActionResult Tournaments()
        {
            using (var db = new DatabaseContext())
            {
                var stickEmGame = db.Games.FirstOrDefault(x => x.Name == "Stick´Em");
                var omNomGame = db.Games.FirstOrDefault(x => x.Name == "OmNom");
                var hauntedHouseGame = db.Games.FirstOrDefault(x => x.Name == "The Haunted Circus");
                var cubesGame = db.Games.FirstOrDefault(x => x.Name == "Cubes");

                var currentDate = DateTime.UtcNow;

                db.Tournaments.AddRange(new List<Tournament>()
                {
                new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 0,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 1, 0, 0, 0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },

                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 1,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 2,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },

                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 2,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 3,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },

                 new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 3,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 4,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 4,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 5,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 6,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 7,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                 new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 7,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 8,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 8,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 9,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 9,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 10,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                 new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 10,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 11,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 11,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 12,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 12,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 13,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                 new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 13,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 14,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                   new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 14,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 15,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 15,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 16,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 16,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 17,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                  new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 17,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 18,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "OmNom €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = omNomGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 18,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 19,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                 new Tournament
                {
                    Name = "Cubes €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = cubesGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 19,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 20,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 20,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 21,0,0,0)),
                    Retryable = true,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 21,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 22,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                },
                new Tournament
                {
                    Name = "Stick'Em €200",
                    Format = TournamentFormat.Score,
                    StartSpins = 100,
                    Game = stickEmGame,
                    StartDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 22,0,0,0)),
                    EndDOT = DateHelper.ParseTimeZone(DateHelper.ChangeTime(currentDate, 23,0,0,0)),
                    Retryable = false,
                    Prizepool = new List<TournamentPrizepool>()
                    {
                        new TournamentPrizepool()
                        {
                            Position = "1",
                            Prize = 100
                        },
                        new TournamentPrizepool()
                        {
                            Position = "2",
                            Prize = 50
                        },
                        new TournamentPrizepool()
                        {
                            Position = "3",
                            Prize = 25
                        },
                        new TournamentPrizepool()
                        {
                            Position = "4",
                            Prize = 25
                        },
                    }
                }
            });

                db.SaveChanges();
            }
            return Ok();
        }

        [Route("run")]
        public ActionResult Seed()
        {
            //TODO: Truncate all tables

            var provider = Context.GameProviders.Add(new GameProvider()
            {
                Name = "Hacksaw Gaming",
                Description = "We are a gambling software provider with the operator in focus and the mobile slots on our minds."
            }).Entity;

            Context.Activities.AddRange(new List<Activity>()
                {
                new Activity
                {
                    Name = "Login",
                    Description = "User has logged in",
                    LevelThreshold = 0,
                    TimeThreshold = TimeSpan.Zero,
                    Handle= "UserLoggedIn"
                },
                new Activity
                {
                    Name = "JoinTournament",
                    Description = "User has joined a tournament",
                    LevelThreshold = 0,
                    TimeThreshold = TimeSpan.Zero,
                    Handle= "UserJoinedTournament"
                },
                 new Activity
                {
                    Name = "Spin tournament",
                    Description = "User spinned tournament",
                    LevelThreshold = 0,
                    TimeThreshold = TimeSpan.Zero,
                    Handle= "UserSpinnedTournament"
                },
                new Activity
                {
                    Name = "Spin coin league",
                    Description = "User spinned in coin league",
                    LevelThreshold = 0,
                    TimeThreshold = TimeSpan.Zero,
                    Handle= "UserSpinnedCoinLeague"
                },
                new Activity
                {
                    Name = "LevelUp",
                    Description = "User has leveled up",
                    LevelThreshold = 0,
                    TimeThreshold = TimeSpan.Zero,
                    Handle= "UserLeveledUp"
                }
            });

            Context.SaveChanges();

            Context.Achievements.AddRange(new List<Achievement>()
            {
            new Achievement
            {
                Name = "Amount of Spins",
                Description = "Spin 1000 spins in coin league",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/SpinsBronzeResurs_42_kr80pk.png",
                Period = AchievementPeriod.Day,
                Amount = 1000,
                RewardAmount = 5,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserSpinnedCoinLeague"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
             new Achievement
            {
                Name = "Amount of Spins",
                Description = "Spin 2000 spins in coin league",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884499/badges/SpinsSilverResurs_43_k2f6qu.png",
                Period = AchievementPeriod.Day,
                Amount = 2000,
                RewardAmount = 10,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserSpinnedCoinLeague"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
              new Achievement
            {
                Name = "Amount of Spins",
                Description = "Spin 5000 spins in coin league",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/SpinsBronzeResurs_42_kr80pk.png",
                Period = AchievementPeriod.Week,
                Amount = 5000,
                RewardAmount = 15,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserSpinnedCoinLeague"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
               new Achievement
            {
                Name = "Amount of Spins",
                Description = "Spin 10000 spins in coin league",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/SpinsBronzeResurs_42_kr80pk.png",
                Period = AchievementPeriod.Month,
                Amount = 10000,
                RewardAmount = 30,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserSpinnedCoinLeague"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
            new Achievement
            {
                Name = "Amount of Tournaments Played",
                Description = "Play 10 tournaments",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/TournamentsBronzeResurs_29_fbzltw.png",
                Period = AchievementPeriod.Week,
                Amount = 10,
                RewardAmount = 5,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserJoinedTournament"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
            new Achievement
            {
                Name = "Amount of Tournaments Played",
                Description = "Play 50 tournaments",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/TournamentsSilverResurs_30_ogddxx.png",
                Period = AchievementPeriod.Month,
                Amount = 50,
                RewardAmount = 5,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserJoinedTournament"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            }
            ,
            new Achievement
            {
                Name = "Amount of Tournaments Played",
                Description = "Play 100 tournaments",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/TournamentsGoldResurs_31_so2f8d.png",
                Period = AchievementPeriod.Lifetime,
                Amount = 100,
                RewardAmount = 5,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserJoinedTournament"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            },
            new Achievement
            {
                Name = "Amount of Tournaments Played",
                Description = "Play 1000 tournaments",
                BadgeImage = "https://res.cloudinary.com/reelzone/image/upload/v1596884498/badges/TournamentsPremiumResurs_32_crsk12.png",
                Period = AchievementPeriod.Lifetime,
                Amount = 1000,
                RewardAmount = 5,
                RewardType = 0,
                Activity = Context.Activities.FirstOrDefault(x => x.Handle == "UserJoinedTournament"),
                Visible = true,
                Enabled = true,
                LevelThreshold = 0
            }
            });

            Context.SaveChanges();
            Context.Games.AddRange(new List<Game>()
            {
                new Game
                {
                    GameProvider = provider,
                    Name = "Stick´Em",
                    HeaderImage = "https://i.imgur.com/TRl9VJ5.png",
                    Description = null,
                    ExternalGameId = 1042,
                    BackgroundImage = "https://i.imgur.com/Nxg3Fpc.png",
                    CardBackgroundImage = "https://i.imgur.com/ZJ3PoLI.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "OmNom",
                    HeaderImage = "https://i.imgur.com/D8I8CfE.png",
                    Description = null,
                    ExternalGameId = 1043,
                    BackgroundImage = "https://i.imgur.com/my2LKrE.png",
                    CardBackgroundImage = "https://i.imgur.com/AqODcxQ.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "The Haunted Circus",
                    HeaderImage = "https://i.imgur.com/L17mk1b.png",
                    Description = null,
                    ExternalGameId = 1039,
                    BackgroundImage = "https://i.imgur.com/mUfwDi5.png",
                    CardBackgroundImage = "https://i.imgur.com/ion4Wbh.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "Cubes",
                    HeaderImage = "https://i.imgur.com/eJE948N.png",
                    Description = null,
                    ExternalGameId = 1048,
                    BackgroundImage = "https://i.imgur.com/OoQl6ta.png",
                    CardBackgroundImage = "https://i.imgur.com/N1nqdQt.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "Cash Compass",
                    HeaderImage = "https://i.imgur.com/HSizgRX.png",
                    Description = null,
                    ExternalGameId = 1055,
                    BackgroundImage = "",
                    CardBackgroundImage = "https://i.imgur.com/KmblyL6.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "Lucky Number",
                    HeaderImage = "https://res.cloudinary.com/reelzone/image/upload/v1596795411/UserImages/my_lucky_number_600x250_rjtrwc.png",
                    Description = null,
                    ExternalGameId = 1038,
                    BackgroundImage = "",
                    CardBackgroundImage = "https://res.cloudinary.com/reelzone/image/upload/v1596795411/UserImages/my_lucky_number_600x250_rjtrwc.png"
                },
                new Game
                {
                    GameProvider = provider,
                    Name = "Miami Multiplier",
                    HeaderImage = "https://res.cloudinary.com/reelzone/image/upload/v1596824145/UserImages/miami_600x250_ygpi0t.pngL",
                    Description = null,
                    ExternalGameId = 1049,
                    BackgroundImage = "",
                    CardBackgroundImage = "https://res.cloudinary.com/reelzone/image/upload/v1596824145/UserImages/miami_600x250_ygpi0t.pngL"
                }
            });

            Context.SaveChanges();

            return Ok();
        }
    }
}