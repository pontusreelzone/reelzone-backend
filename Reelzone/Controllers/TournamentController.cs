﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Reelzone.Core.Interfaces;
using System;
using System.Linq;
using Reelzone.ApiModels;
using Reelzone.Core.Dtos;
using Reelzone.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using Reelzone.Core.Helpers;
using System.Threading.Tasks;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TournamentController : ControllerBase
    {
        public ITournamentService TournamentService;
        public IUserService UserService;
        public IWalletService WalletService;
        private IMemoryCache _cache;
        private IUserActivityService UserActivityService;
        private IMailService MailService;

        public TournamentController(ITournamentService tournamentService, IUserService userService, IWalletService walletService, IMemoryCache memoryCache, IUserActivityService userActivityService, IMailService mailService)
        {
            TournamentService = tournamentService;
            UserService = userService;
            WalletService = walletService;
            _cache = memoryCache;
            UserActivityService = userActivityService;
            MailService = mailService;
        }

        [Route("live")]
        [Authorize]
        public ActionResult Live()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournaments = TournamentService.GetLiveTournaments(identifier);
            return Ok(JsonConvert.SerializeObject(tournaments, Formatting.Indented));
        }

        [Route("exclusive")]
        [Authorize]
        public ActionResult Exclusive()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournaments = TournamentService.GetExclusiveTournaments(identifier);

            if (tournaments != null)
            {
                return Ok(JsonConvert.SerializeObject(tournaments, Formatting.Indented));
            }
            else
            {
                return BadRequest();
            }
        }

        [Route("history")]
        [Authorize]
        public ActionResult History()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournaments = TournamentService.GetHistoryTournaments(identifier);
            return Ok(JsonConvert.SerializeObject(tournaments, Formatting.Indented));
        }

        [Route("winners")]
        public ActionResult Winners()
        {
            List<TournamentAttendeDto> cacheEntry;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.Winners, out cacheEntry))
            {
                // Key not in cache, so get data.
                cacheEntry = TournamentService.GetTournamentWinners(4);

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(30));

                // Save data in cache.
                _cache.Set(CacheKeys.Winners, cacheEntry, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(cacheEntry, Formatting.Indented));
        }

        [Route("all")]
        public ActionResult All()
        {
            var tournaments = TournamentService.GetTournaments(4);
            return Ok(JsonConvert.SerializeObject(tournaments, Formatting.Indented));
        }

        [Route("token")]
        [HttpPost]
        [Authorize]
        public ActionResult CreateWalletToken([FromBody] WalletTokenApiModel tokenApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;

            var walletTokenDto = new WalletTokenDto()
            {
                Tournament = TournamentService.GetTournament(new Guid(tokenApiModel.TournamentId)),
                User = UserService.GetUser(identifier)
            };

            var walletToken = WalletService.CreateWalletToken(walletTokenDto);
            return Ok(JsonConvert.SerializeObject(walletToken, Formatting.Indented));
        }

        [Route("token/play")]
        [HttpPost]
        [Authorize]
        public ActionResult CreateWalletPlayToken([FromBody] WalletTokenApiModel tokenApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;

            var walletTokenDto = new WalletTokenDto()
            {
                Tournament = null,
                User = UserService.GetUser(identifier)
            };

            var walletToken = WalletService.CreatePlayWalletToken(walletTokenDto);
            return Ok(JsonConvert.SerializeObject(walletToken, Formatting.Indented));
        }

        [Route("{id}")]
        public ActionResult Tournament(Guid ID)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var tournament = TournamentService.GetTournament(ID, identifier);
            return Ok(JsonConvert.SerializeObject(tournament, Formatting.Indented));
        }

        [Route("notify/{id}")]
        [HttpGet]
        public async Task<ActionResult> Notify(Guid ID)
        {
            var attendees = TournamentService.GetAttenders(ID);
            var tournament = TournamentService.GetTournament(ID);

            foreach (var attende in attendees)
            {
                if (attende.CurrentPrize > 0)
                {
                    var user = UserService.GetUser(attende.User.ID);
                    UserActivityService.CreateUserActivity(attende.User.ID, "wonTournament");
                    await MailService.SendWinnerEmail(user.Email, user, tournament, attende);
                }
            }

            return Ok(JsonConvert.SerializeObject(tournament, Formatting.Indented));
        }

        [Route("{id}/join")]
        [Authorize]
        public ActionResult Join(Guid ID)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);
            var tournamentAttende = TournamentService.JoinTournament(user.ID, ID);
            if (tournamentAttende != null && user.Banned == false)
            {
                UserActivityService.CreateUserActivity(user.ID, "UserJoinedTournament");
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [Route("{tournamentID}/attenders")]
        [Authorize]
        public ActionResult GetAttenders(Guid tournamentID)
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetCleanUser(identifier);
            var attenders = TournamentService.GetAttendersModel(tournamentID);
            var attende = TournamentService.UserAttending(user.ID, tournamentID);
            if (attende != null)
            {
                attenders.Attende = attende;
            }
            return Ok(attenders);
        }

        [Route("{tournamentID}/attenders/gui")]
        public ActionResult GetAttendersGui(Guid tournamentID)
        {
            var attenders = TournamentService.GetAttendersModel(tournamentID);
            return Ok(attenders);
        }

        [Route("{id}/attende/{attenderID}")]
        public ActionResult Attende(Guid ID, Guid attenderID)
        {
            var attender = TournamentService.GetAttender(attenderID);
            if (attender != null)
            {
                return Ok(JsonConvert.SerializeObject(attender, Formatting.Indented));
            }
            else
            {
                return BadRequest();
            }
        }
    }
}