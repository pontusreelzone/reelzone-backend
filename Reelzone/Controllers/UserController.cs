﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.ApiModels;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Enums;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using Reelzone.Core.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        public IUserService UserService;
        public IUserActivityService UserActivityService;
        public IUserExperienceService UserExperienceService;
        public ICoinService CoinService;
        public ITournamentService TournamentService;
        public IMemoryCache _cache;
        public IMailService MailService;
        public IUserRefferalService UserRefferalService;
        public IPushService PushService;

        public UserController(IUserService userService, IUserActivityService userActivityService, IUserExperienceService userExperienceService, ICoinService coinService, IMemoryCache memoryCache, IMailService mailService, ITournamentService tournamentService, IUserRefferalService userRefferalService, IPushService pushService)
        {
            UserService = userService;
            UserActivityService = userActivityService;
            UserExperienceService = userExperienceService;
            CoinService = coinService;
            _cache = memoryCache;
            MailService = mailService;
            TournamentService = tournamentService;
            UserRefferalService = userRefferalService;
            PushService = pushService;
        }

        [Route("cheaters")]
        public ActionResult Cheaters()
        {
            var cheaters = UserService.GetCheaters();
            return Ok(cheaters);
        }

        [Route("give")]
        public ActionResult GiveCoins()
        {
            var cheaters = UserService.GiveCoins();
            return Ok(cheaters);
        }

        [Route("forgot")]
        [HttpPost]
        public async Task<ActionResult> Email([FromBody]ForgotPasswordApiModel model)
        {
            var user = UserService.GetUserByEmail(model.Email);
            if (user != null)
            {
                var token = UserService.GenerateUserTokenAsync(user.ID, "PasswordReset");
                var email = await MailService.SendForgotPasswordEmail(model.Email, "https://play.reelzone.app/forgot/" + token);
                return Ok();
            }
            return Ok();
        }

        [Route("forgot/update")]
        [HttpPost]
        public async Task<ActionResult> UpdatePassword([FromBody]UpdatePasswordApiModel model)
        {
            var user = UserService.GetUserByVerifytoken(model.Identifier);

            if (user != null)
            {
                user.Password = model.Password;
                UserService.UpdatePassword(user);
                await UserService.SignInEmailAsync(user, HttpContext);
            }
            else
            {
                return BadRequest();
            }
            return Ok();
        }

        [Route("me")]
        [Authorize]
        public ActionResult Index()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);
            return Ok(JsonConvert.SerializeObject(user, Formatting.Indented));
        }

        [Route("verify/{page}")]
        public async Task<ActionResult> Verify(int page)
        {
            var users = UserService.GetLimitedUsers(500, page);

            foreach (var user in users)
            {
                //Create verification token
                if (user.Verified == false)
                {
                    var token = UserService.GenerateUserTokenAsync(user.ID, "Email");
                    var verificationLink = "https://play.reelzone.app/verify/" + token;

                    var send = await MailService.SendVerificationEmail(user.Email, verificationLink);
                }
            }

            return Ok("Sent to " + users.Count());
        }

        [Route("profile/{userId}")]
        public ActionResult Index(Guid userId)
        {
            UserProfileModel user = new UserProfileModel();

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.Profile + "-" + user.ID, out user))
            {
                // Key not in cache, so get data.
                user = UserService.GetUserProfile(userId);

                user.TournamentsHistory = TournamentService.GetUsersAttenders(user.ID);

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(30));

                // Save data in cache.
                _cache.Set(CacheKeys.Profile + "-" + user.ID, user, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(user, Formatting.Indented));
        }

        [Route("activities/{userId}")]
        public ActionResult Activities(Guid userId)
        {
            List<UserActivity> user;

            // Look for cache key.
            if (!_cache.TryGetValue(CacheKeys.UserActivities + "-" + userId, out user))
            {
                // Key not in cache, so get data.
                user = UserService.GetUserActivities(userId);

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(30));

                // Save data in cache.
                _cache.Set(CacheKeys.UserActivities + "-" + userId, user, cacheEntryOptions);
            }

            return Ok(JsonConvert.SerializeObject(user, Formatting.Indented));
        }

        [Route("register/oauth")]
        [HttpPost]
        public ActionResult RegisterWithOAuth([FromBody] RegisterOauthApiModel registerApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (User.Identity.IsAuthenticated)
            {
                var userDto = new UserDto();

                var country = UserService.GetCountries().FirstOrDefault(x => x.ID == registerApiModel.Country);

                switch (User.Identity.AuthenticationType)
                {
                    case "Twitch":

                        userDto = new UserDto
                        {
                            Email = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value,
                            AvatarImage = User.Claims.FirstOrDefault(c => c.Type == "urn:twitch:profileimageurl").Value,
                            Username = User.Identity.Name,
                            Identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value,
                            Provider = ProviderType.Twitch,
                            Country = country
                        };
                        break;

                    case "Facebook":
                        userDto = new UserDto
                        {
                            Email = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value,
                            AvatarImage = "https://graph.facebook.com/" + User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value + "/picture?type=large",
                            Username = User.Identity.Name,
                            Identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value,
                            Provider = ProviderType.Facebook,
                            Country = country
                        };
                        break;
                }

                if (UserService.UserOauthProviderConnected(userDto.Identifier) == false)
                {
                    var user = UserService.CreateUser(userDto);
                    var response = new ResponseApiModel();
                    if (user != null)
                    {
                        response.Successful = true;
                        return Ok(response);
                    }
                    else
                    {
                        response.Successful = false;
                        return Ok(response);
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        public bool IsLegalAge(string inputDate)
        {
            var bday = Convert.ToDateTime(inputDate);
            var ts = DateTime.Today - bday;
            var year = DateTime.MinValue.Add(ts).Year - 1;
            return year >= 18;
        }

        [Route("register/email")]
        [HttpPost]
        public ActionResult RegisterWithEmail([FromBody] RegisterApimodel registerApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (IsLegalAge(registerApiModel.BirthDate.ToString()) == false)
            {
                return BadRequest();
            }

            var country = UserService.GetCountries().FirstOrDefault(x => x.ID == registerApiModel.Country);

            var userDto = new UserDto
            {
                Email = registerApiModel.Email,
                AvatarImage = "https://res.cloudinary.com/reelzone/image/upload/v1595588889/d8835a090d49a988171ecbf24c107f5c_uf2j0x.png",
                Username = registerApiModel.Username,
                Identifier = registerApiModel.Email,
                Provider = ProviderType.Email,
                Country = country,
                Password = registerApiModel.Password,
                BirthDate = registerApiModel.BirthDate
            };

            var response = new ResponseApiModel();

            if (UserService.UserOauthProviderConnected(userDto.Identifier) == false)
            {
                //Get user
                var user = UserService.CreateUser(userDto);

                if (registerApiModel.Refferal != null)
                {
                    UserRefferalService.RedeemRefferal(registerApiModel.Refferal, user.ID);
                }

                //Create verification token
                //var token = UserService.GenerateUserTokenAsync(user.ID, "Email");

                //var verificationLink = "https://api.reelzone.app/verify/" + token;

                //Send verification email to user
                //MailService.SendVerificationEmail(user.Email, verificationLink);

                //Set cookie
                UserService.SignInEmailAsync(user, HttpContext.Request.HttpContext);

                response.Successful = true;

                return Ok(response);
            }
            else
            {
                response.Successful = false;
                return Ok(response);
            }
        }

        [Route("verify/{token}")]
        [HttpPost]
        public ActionResult VerifyEmail(string token)
        {
            var response = new ResponseApiModel();
            if (User.Identity.IsAuthenticated)
            {
                //get userDto
                var userDto = UserService.GetUserByEmailToken(token);

                //Update Verified
                if (userDto.Verified != true)
                {
                    UserService.UpdateVerified(userDto);
                }
                response.Successful = true;

                return Ok(response);
            }
            else
            {
                response.Successful = false;
                return Ok(response);
            }
        }

        [Route("verify/newsletter/{token}")]
        [HttpPost]
        public async Task<ActionResult> VerifyEmailNewsletter(string token)
        {
            var response = new ResponseApiModel();

            //get userDto
            var userDto = UserService.GetUserByEmailToken(token);

            //var user = await MailService.AddSubscriberToCampaignList(userDto.Email, "2837110d95");

            //Update Verified
            if (userDto.Verified != true)
            {
                UserService.UpdateVerified(userDto);
                UserService.UpdateNewsletter(userDto);
            }
            response.Successful = true;

            return Ok(response);
        }

        [Route("call")]
        public ActionResult Call()
        {
            UserService.SendVerifySMS("");
            return Ok();
        }

        [Route("refferal/stats")]
        [HttpGet]
        [Authorize]
        public ActionResult ReferallStats()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUserEntity(identifier);
            if (user != null)
            {
                var stats = UserService.GetReferralStats(user.ID);
                return Ok(stats);
            }
            return BadRequest();
        }

        [Route("refferal/all/{page}")]
        [HttpGet]
        public ActionResult ReferallAll(int page)
        {
            var users = UserService.GetLimitedUsers(500, page);
            var list = new List<UserReferralStatsModel>();
            foreach (var user in users)
            {
                var stats = UserService.GetReferralStats(user.ID);
                if (stats != null)
                {
                    list.Add(stats);
                }
            }

            return Ok(list);
        }

        [Route("verify/resend/{email}")]
        [HttpPost]
        [Authorize]
        public ActionResult ResendVerifyEmail(string email)
        {
            if (User.Identity.IsAuthenticated)
            {
                //get userDto
                var user = UserService.GetUser(email);
                //If user already verified dont send email verification
                if (user.Verified != true)
                {
                    //Create verification token
                    var token = UserService.GenerateUserTokenAsync(user.ID, "Email");

                    var verificationLink = "https://api.reelzone.app/verify/" + token;

                    //Send verification email to user
                    MailService.SendVerificationEmail(user.Email, verificationLink);

                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [Route("forgot/{email}")]
        [HttpPost]
        public ActionResult ResendPasswordEmail(string email)
        {
            //get userDto
            var user = UserService.GetUser(email);

            //Create verification token
            var token = UserService.GenerateUserTokenAsync(user.ID, "PasswordReset");

            var verificationLink = "https://api.reelzone.app/verify/" + token;

            //Send verification email to user
            MailService.SendForgotPasswordEmail(user.Email, verificationLink);

            return Ok();
        }

        [Route("logout")]
        [HttpGet]
        public void Logout()
        {
            HttpContext.Request.HttpContext.SignOutAsync();
            Response.Redirect(ConfigurationManager.AppSettings.Get("Reelzone.Frontend.Url") + "/login");
        }

        [Route("coins")]
        [HttpGet]
        [Authorize]
        public ActionResult Coins()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUserEntity(identifier);
            if (user != null)
            {
                var coins = CoinService.GetAmount(user.ID);

                var response = new ResponseApiModel()
                {
                    Successful = true,
                    Message = coins.ToString()
                };
                return Ok(JsonConvert.SerializeObject(response, Formatting.Indented));
            }
            else
            {
                var response = new ResponseApiModel()
                {
                    Successful = true,
                    Message = "0"
                };
                return Ok(JsonConvert.SerializeObject(response, Formatting.Indented));
            }
        }

        [Route("provider")]
        [Authorize]
        public ActionResult Provider()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var email = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value;
            var name = User.Identity.Name;

            var UserOauthProviderApiModel = new UserOauthProviderApiModel()
            {
                Identifier = identifier,
                Name = name,
                Email = email
            };

            return Ok(JsonConvert.SerializeObject(UserOauthProviderApiModel, Formatting.Indented));
        }

        [Route("settings")]
        [HttpPatch]
        [Authorize]
        public ActionResult UpdateGeneral([FromBody] UpdateUserApiModel updateUserApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var userDto = UserService
                                    .GetUser(
                                    User
                                    .Claims
                                    .FirstOrDefault
                                     (c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                                    .Value);

            UserDto validInfo = new UserDto();

            //Updates Username
            if (userDto.Username != updateUserApiModel.Username)
            {
                userDto.Username = updateUserApiModel.Username;
                validInfo = UserService.UpdateUsername(userDto);
            }

            //Updates avatar
            if (userDto.AvatarImage != updateUserApiModel.AvatarImage)
            {
                userDto.AvatarImage = updateUserApiModel.AvatarImage;
                validInfo = UserService.UpdateProfileImage(userDto);
            }

            //Updates header
            if (userDto.HeaderImage != updateUserApiModel.HeaderImage)
            {
                userDto.HeaderImage = updateUserApiModel.HeaderImage;
                validInfo = UserService.UpdateHeaderImage(userDto);
            }

            //Updates Birthday

            if (userDto.BirthDate != updateUserApiModel.BirthDate)
            {
                userDto.BirthDate = updateUserApiModel.BirthDate;
                userDto = UserService.UpdateBirthDate(userDto);
            }

            //Updates e mail
            if (userDto.Email != updateUserApiModel.Email)
            {
                userDto.Email = updateUserApiModel.Email;
                validInfo = UserService.UpdateEmail(userDto);
            }

            //Updates Country
            if (userDto.Country != null)
            {
                if (userDto.Country.ID != new Guid(updateUserApiModel.Country))
                {
                    var country = updateUserApiModel.Country;
                    validInfo = UserService.UpdateCountry(userDto, new Guid(updateUserApiModel.Country));
                }
            }

            //Updates PayOut
            try
            {
                if (userDto.PayOutOption != (PayOut)updateUserApiModel.PayOutOptions)
                {
                    userDto.PayOutOption = (PayOut)updateUserApiModel.PayOutOptions;
                    validInfo = UserService.UpdatePayOut(userDto);
                }
            }
            catch (Exception)
            {
                return BadRequest("Not a pay option");
            }
            if (validInfo == null)
            {
                return BadRequest();
            }
            return Ok();
        }

        [Route("settings/password")]
        [HttpPatch]
        [Authorize]
        public ActionResult UpdatePassword([FromBody] UpdateUserApiModel updateUserApiModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var userDto = UserService
                                    .GetUser(
                                    User
                                    .Claims
                                    .FirstOrDefault
                                     (c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                                    .Value);

            if (userDto.Password != updateUserApiModel.Password)
            {
                userDto.Password = updateUserApiModel.Password;
                UserService.UpdatePassword(userDto);
                return Ok();
            }
            return BadRequest();
        }

        //[Route("User/settings/Profile-Image")]
        //[HttpPatch]
        ////public ActionResult UpdateProfileImage([FromBody] UpdateUserApiModel updateUserApiModel)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var userDto = UserService.GetUser(updateUserApiModel.Identifier);
        //    }
        //}
        [Route("countries")]
        [HttpGet]
        public ActionResult GetCountries()
        {
            var countries = UserService.GetCountries();
            if (countries == null)
            {
                return BadRequest();
            }
            return Ok(countries);
        }
    }
}