﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Reelzone.API.ApiModels;
using Reelzone.ApiModels;
using Reelzone.Core.Contexts;
using Reelzone.Core.Dtos;
using Reelzone.Core.Entities;
using Reelzone.Core.Enums;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Reelzone.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WheelController : ControllerBase
    {
        public IUserService UserService;
        public IUserActivityService UserActivityService;
        public IUserExperienceService UserExperienceService;
        public ICoinService CoinService;
        public IWheelService WheelService;
        public DateHelper DateHelper;

        public WheelController(IUserService userService, IUserActivityService userActivityService, IUserExperienceService userExperienceService, ICoinService coinService, IWheelService wheelService)
        {
            UserService = userService;
            UserActivityService = userActivityService;
            UserExperienceService = userExperienceService;
            CoinService = coinService;
            WheelService = wheelService;
            DateHelper = new DateHelper();
        }

        [Route("spin")]
        [Authorize]
        public ActionResult Index()
        {
            var identifier = User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" || c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var user = UserService.GetUser(identifier);
            var spin = WheelService.SpinWheel(user.ID);
            if (spin != 0 || spin != 7)
            {
                var amount = WheelService.GetCoinWinnings(spin);

                var coins = CoinService.CreateCoin(new CoinDto()
                {
                    Amount = amount,
                    Description = "Won ony daily fortune spin",
                    Created = DateHelper.ParseTimeZone(DateTime.UtcNow),
                    User = user
                });
                return Ok(spin);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}