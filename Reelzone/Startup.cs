using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Reelzone.Core.Helpers.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Reelzone.Core.Contexts;
using Reelzone.Core.Helpers;
using Reelzone.Core.Interfaces;
using Reelzone.Core.Services;
using System.Configuration;
using Microsoft.AspNetCore.Http;
using System.Net;
using Reelzone.Core.Entities;
using System;
using Microsoft.Extensions.Caching.Memory;

namespace Reelzone
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = (IConfigurationRoot)configuration;
        }

        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public IConfigurationRoot Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                // TODO: Use your User Agent library of choice here.
                if (DisallowsSameSiteNone(userAgent))
                {
                    // For .NET Core < 3.1 set SameSite = (SameSiteMode)(-1)
                    options.SameSite = SameSiteMode.Unspecified;
                }
            }
        }

        //  Read comments in https://docs.microsoft.com/en-us/aspnet/core/security/samesite?view=aspnetcore-3.1
        public bool DisallowsSameSiteNone(string userAgent)
        {
            // Check if a null or empty string has been passed in, since this
            // will cause further interrogation of the useragent to fail.
            if (String.IsNullOrWhiteSpace(userAgent))
                return false;

            // Cover all iOS based browsers here. This includes:
            // - Safari on iOS 12 for iPhone, iPod Touch, iPad
            // - WkWebview on iOS 12 for iPhone, iPod Touch, iPad
            // - Chrome on iOS 12 for iPhone, iPod Touch, iPad
            // All of which are broken by SameSite=None, because they use the iOS networking
            // stack.
            if (userAgent.Contains("CPU iPhone OS 12") ||
                userAgent.Contains("iPad; CPU OS 12"))
            {
                return true;
            }

            // Cover Mac OS X based browsers that use the Mac OS networking stack.
            // This includes:
            // - Safari on Mac OS X.
            // This does not include:
            // - Chrome on Mac OS X
            // Because they do not use the Mac OS networking stack.
            if (userAgent.Contains("Macintosh; Intel Mac OS X 10_14") &&
                userAgent.Contains("Version/") && userAgent.Contains("Safari"))
            {
                return true;
            }

            // Cover Chrome 50-69, because some versions are broken by SameSite=None,
            // and none in this range require it.
            // Note: this covers some pre-Chromium Edge versions,
            // but pre-Chromium Edge does not require SameSite=None.
            if (userAgent.Contains("Chrome/5") || userAgent.Contains("Chrome/6"))
            {
                return true;
            }

            return false;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //var connectionString = @"Server=localhost\SQLEXPRESS;Database=reelzone-dev;Trusted_Connection=True;";
            var connectionString = ConfigurationManager.AppSettings.Get("Microsoft.Sql.ConnectionString");

            //Hangfire
            services.AddHangfire(x => x.UseSqlServerStorage(connectionString));
            services.AddHangfireServer();
            services.AddScoped<IHangfireService, HangfireService>();

            // Register the Swagger services
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Reelzone API";
                    document.Info.Description = "Reelzone API for the React App";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Pontus Espe",
                        Email = "pontus@reelzone.app",
                        Url = "https://twitter.com/spboyer"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Use under Reelzone Ltd",
                        Url = "https://example.com/license"
                    };
                };
            });
            services.AddAuthorizationCore(opt =>
            {
                opt.AddPolicy("Admin", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.Requirements.Add(new RoleRequirement(2));
                });
                opt.AddPolicy("Provider", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.Requirements.Add(new RoleRequirement(1));
                });
            }
            );
            services.AddSingleton<IAuthorizationHandler, RoleHandler>();

            services.AddControllers();

            string TwitchClientID = ConfigurationManager.AppSettings.Get("Reelzone.Oauth.Twitch.ClientID");
            string TwitchSecret = ConfigurationManager.AppSettings.Get("Reelzone.Oauth.Twitch.Secret");
            string AllowedOrigin = ConfigurationManager.AppSettings.Get("Reelzone.AllowedOrigin");
            string CookieDomain = ConfigurationManager.AppSettings.Get("Reelzone.CookieDomain");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddFacebook(options =>
            {
                options.AppId = "386091638744942";
                options.AppSecret = "ec6f63160ee833eaea95a73982f9698a";
                //options.CallbackPath = new PathString("/signin-facebook");
            })
            .AddTwitch(options =>
            {
                options.ClientId = TwitchClientID;
                options.ClientSecret = TwitchSecret;
            })
            .AddCookie(options =>
            {
                options.Cookie.Domain = CookieDomain;
                options.LoginPath = "/authentication/signin";
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
                options.OnAppendCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext =>
                    CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins(AllowedOrigin)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
            });
        }

        // ConfigureContainer is where you can register things directly
        // with Autofac. This runs after ConfigureServices so the things
        // here will override registrations made in ConfigureServices.
        // Don't build the container; that gets done for you by the factory.
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<UserService>()
                .As<IUserService>()
                .UsingConstructor(typeof(IUserExperienceService), typeof(IAutoMapper), typeof(ICoinService), typeof(IUserAchievmentService))
                .SingleInstance();

            builder.RegisterType<WalletService>()
                .As<IWalletService>()
                .UsingConstructor(typeof(DatabaseContext), typeof(IUserService))
                .SingleInstance();
            builder.RegisterType<CountryService>()
               .As<ICountryService>()
               .UsingConstructor(typeof(DatabaseContext), typeof(IUserService))
               .SingleInstance();

            builder.RegisterType<MailService>()
                .As<IMailService>()
                .UsingConstructor(typeof(DatabaseContext), typeof(IUserExperienceService))
                .SingleInstance();

            builder.RegisterType<CoinService>()
              .As<ICoinService>()
              .UsingConstructor(typeof(DatabaseContext))
              .SingleInstance();

            builder.RegisterType<PushService>()
             .As<IPushService>()
             .UsingConstructor()
             .SingleInstance();

            builder.RegisterType<UserActivityService>()
                .As<IUserActivityService>()
                .UsingConstructor(typeof(IUserExperienceService), typeof(IUserAchievmentService), typeof(DatabaseContext))
                .SingleInstance();

            builder.RegisterType<UserRefferalService>()
              .As<IUserRefferalService>()
              .UsingConstructor(typeof(DatabaseContext), typeof(ICoinService), typeof(IUserService))
              .SingleInstance();
            builder.RegisterType<RefferalService>()
             .As<IRefferalService>()
             .UsingConstructor(typeof(DatabaseContext), typeof(IUserRefferalService), typeof(IUserService), typeof(IAutoMapper))
             .SingleInstance();

            builder.RegisterType<BusQueService>()
                .As<IBusQueService>()
                .UsingConstructor(typeof(DatabaseContext))
                .SingleInstance();

            builder.RegisterType<Mapper>()
                .As<IAutoMapper>()
                .SingleInstance();

            builder.RegisterType<WheelService>()
             .As<IWheelService>()
             .UsingConstructor(typeof(DatabaseContext))
             .SingleInstance();

            builder.RegisterType<DatabaseContext>()
                .SingleInstance();

            builder.RegisterType<MemoryCache>()
                .As<IMemoryCache>()
                .SingleInstance();

            builder.RegisterType<TournamentService>()
                .As<ITournamentService>()
                .UsingConstructor(typeof(IUserActivityService), typeof(IBusQueService), typeof(IAutoMapper), typeof(ICoinService))
                .SingleInstance();

            builder.RegisterType<UserAchievementService>()
                .As<IUserAchievmentService>()
                .UsingConstructor(typeof(IBusQueService), typeof(DatabaseContext), typeof(IUserExperienceService), typeof(IAutoMapper))
                .SingleInstance();

            builder.RegisterType<UserExperienceService>()
                .As<IUserExperienceService>()
                .UsingConstructor(typeof(DatabaseContext), typeof(IBusQueService), typeof(IPushService))
                .SingleInstance();

            builder.RegisterType<HangfireService>()
              .As<IHangfireService>()
              .SingleInstance();

            builder.RegisterType<GameTransactionService>()
                .As<IGameTransactionService>()
                .UsingConstructor(typeof(DatabaseContext))
                .SingleInstance();
            builder.RegisterType<GameService>()
               .As<IGameService>()
               .UsingConstructor(typeof(DatabaseContext), typeof(IAutoMapper), typeof(ICoinService), typeof(IUserService))
               .SingleInstance();
            builder.RegisterType<GameProviderService>()
               .As<IGameProviderService>()
               .UsingConstructor(typeof(DatabaseContext), typeof(IAutoMapper))
               .SingleInstance();
            builder.RegisterType<AchievementService>()
              .As<IAchievementService>()
              .UsingConstructor(typeof(DatabaseContext), typeof(IAutoMapper))
              .SingleInstance();
            builder.RegisterType<UserNotificationService>()
             .As<IUserNotificationService>()
             .UsingConstructor(typeof(DatabaseContext), typeof(IAutoMapper), typeof(IUserService))
             .SingleInstance();
            /*
            builder.RegisterType<UserController>().PropertiesAutowired();
            builder.RegisterType<AuthenticationController>().PropertiesAutowired();
            builder.RegisterType<TournamentController>().PropertiesAutowired();
            builder.RegisterType<SeedController>().PropertiesAutowired();
            */
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Hangfire
            app.UseHangfireDashboard("/hangfire", new DashboardOptions());

            //var userAchievmentService = app.ApplicationServices
            //            .GetRequiredService<IUserAchievmentService>();

            //userAchievmentService.GenerateUserPeriodJobs();

            // If, for some reason, you need a reference to the built container, you
            // can use the convenience extension method GetAutofacRoot.
            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}